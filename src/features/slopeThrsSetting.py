# SETUP
import sys
import gc
import glob
import pyarrow.parquet as pq
from sqlalchemy import create_engine
from requests import get

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import date, datetime, timedelta
from multiprocessing import Pool, cpu_count

pd.options.mode.chained_assignment = None
input_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_11ST/raw/'
outut_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_11ST/processed/'



ivt_files_whole = glob.glob(raw_data_path + '/MWS_COLT_ITEM_IVT/ivt_*')
collect_day_list = list(set([int(f.split('ivt_')[1].split(".")[0]) for f in ivt_files_whole]))
collect_day_list.sort()


class slopeThrsSetting():
    def __init__: # Connect DB & GET collect_days list
        pass

    def get_data(table, partition_col, id_part, collected_days):
        files = glob.glob(path + f'{table}/ivt_*/{partition_col}_PART={id_part}')
        files_collected_days = [f for f in ivt_files if f.split('ivt_')[1].split(".")[0] in collected_days]

        df_full = pd.DataFrame()
        for i in files_collected_days:
            table = pq.read_table(i, use_threads=10)
            df_temp = table.to_pandas()
            df_full = df_full.append(df_temp)
            del df_temp
            gc.collect()

        return df_full

    def filterIvtData(self):
        pass
    def setThrs(self):
        def cal_slope_max(down_slopes, z_score_max=3.5):
            '''
            Calculate reasonable max value (per second) of slopes according to mod-z score.

            > Conditions :
            To calculate reasonable_slope_max_ph, MAD should not be zero.

            > Params :
            - down_slopes : pd.Series. dropped STOCK AMOUNT.
            - z_score_max : int. baseline for cutting modified-z socre outliers.

            > Return :
            - reasonable_slopes_max_psec : reasonable slope max value per a second

            > References:
            - z_score_max : https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm
            "These authors recommend that modified Z-scores with an absolute value of greater than 3.5
            be labeled as potential outliers."
            '''

            down_slopes_median = np.median(down_slopes)
            MAD = np.median(np.abs(down_slopes - down_slopes_median))
            condition = (MAD != 0)

            if condition:
                #         print("Set reasonable_slopes_max_psec as max(normal_down_slopes)")
                modified_z_scores = 0.6745 * ((down_slopes - down_slopes_median) / MAD)
                normal_down_slopes = down_slopes[modified_z_scores < z_score_max]
                reasonable_slopes_max_psec = max(normal_down_slopes)
                return reasonable_slopes_max_psec

            else:
                #         print("Set reasonable_slopes_max_psec as None")
                reasonable_slopes_max_psec = None
                return reasonable_slopes_max_psec

        # ----------------------------------------------------

        pass
    def setImp(self):
        pass

    def saveParquet(self):

    def __del__:
        '''
        FINAL VERSION?!
        '''
# --------------------------------------
'''
FINAL VERSION?! 
'''


def cal_slope_max(down_slopes, z_score_max=3.5):
    '''
    Calculate reasonable max value (per second) of slopes according to mod-z score.

    > Conditions :
    To calculate reasonable_slope_max_ph, MAD should not be zero.

    > Params :
    - down_slopes : pd.Series. dropped STOCK AMOUNT.
    - z_score_max : int. baseline for cutting modified-z socre outliers.

    > Return :
    - reasonable_slopes_max_psec : reasonable slope max value per a second

    > References:
    - z_score_max : https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm
    "These authors recommend that modified Z-scores with an absolute value of greater than 3.5
    be labeled as potential outliers."
    '''

    down_slopes_median = np.median(down_slopes)
    MAD = np.median(np.abs(down_slopes - down_slopes_median))
    condition = (MAD != 0)

    if condition:
        #         print("Set reasonable_slopes_max_psec as max(normal_down_slopes)")
        modified_z_scores = 0.6745 * ((down_slopes - down_slopes_median) / MAD)
        normal_down_slopes = down_slopes[modified_z_scores < z_score_max]
        reasonable_slopes_max_psec = max(normal_down_slopes)
        return reasonable_slopes_max_psec

    else:
        #         print("Set reasonable_slopes_max_psec as None")
        reasonable_slopes_max_psec = None
        return reasonable_slopes_max_psec


# ----------------------------------------------------

unit_hour = 24
min_allow_thrs_puh = 9  # (a.k.a. unconditional_allow)
min_allow_imp_puh = 1
replace_zero_diff = 1.2
z_score_max = 3.5
parameters = tuple(unit_hour, min_allow_thrs_puh, min_allow_imp_puh, replace_zero_diff, z_score_max)

class settingThrsImp():
    '''
    Apply groupby dataframe ['ITEM_ID', 'STOCK_ID']
    '''
    def __init__(self, sdf):
        self.sdf = sdf
        self.iid = sdf['ITEM_ID'].unique()[0]
        self.sid = sdf['STOCK_ID'].unique()[0]

    def checkDataCount(self):
        self.sdf

def set_thrs_imp_main(sdf, parameters):
    '''
    newer version ! as of 190515
    '''
    unit_hour, min_allow_thrs_puh, min_allow_imp_puh, replace_zero_diff, z_score_max = *parameters


    # ----------------------------------------------------
    # 1) check if the stock is appropriate for setting threshold
    # 1-1) Is data collection period longer than 48 hours?
    # 1-2) Is there more than 7 data points?
    collection_period = sdf.REG_DT.max() - sdf.REG_DT.min()
    cond1 = collection_period < timedelta(hours=48)
    cond2 = len(sdf) < 7

    if (cond1 | cond2):
        reasonable_slope_max = None

    else:
        # -----------------------------------------------------
        # 2) calculate mod_z applied threshold for the slope
        # 2-1) get stock_amount_diff
        stock_amount_diff = (sdf['STOCK_AMOUNT'].shift() - sdf['STOCK_AMOUNT']).fillna(0)
        stock_amount_diff = stock_amount_diff.replace(0, replace_zero)

        # 2-2) get reg_dt_diff
        reg_dt_diff = (sdf['REG_DT'] - sdf['REG_DT'].shift()).apply(lambda x: x.total_seconds())
        reg_dt_diff_median = (reg_dt_diff[(reg_dt_diff > timedelta(hours=1).total_seconds())]).median()
        # if reg_dt_diff is too short(less than reg_dt_diff_median), replace reg_dt_diff with the half of reg_dt_diff_median.
        reg_dt_diff = reg_dt_diff.apply(
            lambda rddiff: reg_dt_diff_median * .5 if rddiff < reg_dt_diff_median else rddiff)

        # 2-3) get slope
        slop_per_sec = stock_amount_diff / reg_dt_diff
        down_slopes = slop_per_sec[slop_per_sec > 0]

        # 2-4) get reasonable max slope **per a second** (could be None)
        # calculate mod_z score for each down_slope value & set stock_diff threshold for outlier dectection
        # 'reasonable_max(ys, threshold=3.5, item_id=None)' part in OY module.
        if (len(down_slopes) != 0):
            reasonable_slope_max = cal_slope_max(down_slopes, z_score_max=z_score_max)
        else:
            reasonable_slope_max = None
    # ----------------------------------------------------
    # 3) set stock diff thrs & impute value (per a unit hour)
    if reasonable_slope_max:
        # set thrs
        reasonable_slope_max_puh = round(reasonable_slope_max * 60 * 60 * unit_hour)
        stock_diff_thrs_puh = max(reasonable_slope_max_puh, min_allow_thrs_puh)

        # set impute value
        normal_slope_median_puh = round(
            (down_slopes[down_slopes <= reasonable_slope_max]).median() * 60 * 60 * unit_hour)
        impute_value_puh = max(normal_slope_median_puh, min_allow_imp_puh)

    else:
        # If reasonable_max_value is None (on step #1 or #3-2)
        # Take min_allow_thrs_puh & min_allow_imp_puh
        stock_diff_thrs_puh = min_allow_thrs_puh
        impute_value_puh = min_allow_imp_puh
    #     print(stock_diff_thrs_puh, impute_value)
    return pd.DataFrame({'ITEM_ID': [iid], 'STOCK_ID': [sid], 'SLOPE_THRS_PUH': [stock_diff_thrs_puh],
                         'IMP_VALUE_PUH': [impute_value_puh], 'UNIT_HOUR': [unit_hour]})


def applyParallel(dfGrouped, func):
    with Pool(cpu_count()) as p:
        df_list = p.map(func, [group for name, group in dfGrouped])
    return pd.concat(df_list)




if __main__ == __init__:
    for iip in item_id_parts:
        s = datetime.now()
        print(iip, "START Setting Thresholds and Impute Values module", str(s))
        # Get ivt data
        ivt_full = get_data(table, partition_col, iip, collected_days)

        # Filter ivt data
        ivt_full = filter_ivt_data(ivt_full, collect_site)

        # set thrs and imp values group by ['ITEM_ID', 'STOCK_ID']
        result = applyParallel(ivt_full.groupby(['ITEM_ID', 'STOCK_ID']), set_thrs_imp_main)
        print(iip, "TIME SPENT : ", (str(datetime.now() - s)))
        break