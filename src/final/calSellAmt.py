class calSellAmt:
    def __init__(self, diff_df, th_imp_df, price_df):
        # self.diff_df = diff_df
        # self.th_imp_df = th_imp_df
        df = pd.merge(diff_df, th_imp_df, on=['ITEM_ID', 'STOCK_ID'], how='left')

        self.df = self.markPriceThrs(df, priceparams)
        self.sellamt_columns = ['ID', 'ITEM_ID', 'STOCK_ID', 'COLLECT_DAY', ' SELL_AMOUNT', 'STOCK_AMOUNT',
                                'REVISE_STOCK_AMOUNT', 'REG_ID', 'REG_DT', 'UPT_DT', 'UPT_ID']


    def applyMultiProc(self):
        grouped_df = self.df.groupby(['ITEM_ID', 'STOCK_ID'])
        with Pool(cpu_count()) as p:
            df_list = p.map(self.getResult, [sdf for name, sdf in grouped_df])

        return pd.concat(df_list)

    def getResult(self, sdf):

        sdf = self.stockIntpr(sdf)
        sdf = self.markOuliers(sdf)
        sdf = self.checkDiscountRate(sdf)

        down_slopes, reasonable_slopes_max_psec = self.getRsnbSlpMx(sdf)
        stock_diff_thrs_puh, impute_value_puh = self.setThrsImp(down_slopes, reasonable_slopes_max_psec)

        # output table columns
        # self.sellamt_columns

    def stockIntpr(self, sdf):
        pass

    def checkDiscountRate(self, sdf):
        ''' sdf should already have discount rate of the point'''
        pass

    def markOuliers(self, sdf):
        sdf['FINAL_THRS'] = max()
        sdf['SLOPE'] <=
        pass


