from multiprocessing import Pool, cpu_count
from datetime import datetime
import sys
import os
import pandas as pd
import numpy as np

class ModifyScmDiff:
    def __init__(self, ivt):
        self.ivt = ivt

    def actionNormal(self):
        print("--------  Get STOCK & REG_DT diff values -- groupby [item_id x stock_id] --------")
        # 1.stock amount diff values.
        original_len = len(self.ivt)
        s = datetime.now()
        ivt_diff_df = self.ivt.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getSTKDiff)
        ivt_diff_df = ivt_diff_df.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getREGDTDiff)
        print('len(ivt):', original_len, '---> ', len(ivt_diff_df))
        return result

    def action(self):
        print("-------- Modifying SCM STK_DIFF --------")
        # 1.stock amount diff values. 
        original_len = len(self.ivt)
        self.ivt = self.ivt.assign(REG_DT_SLOT=self.ivt['REG_DT'].apply(
            lambda dt: dt.replace(minute=0 if dt.minute < 30 else 30, second=0, microsecond=0)))
        print('1. Get Base diff values -- groupby [item_id x stock_id]')
        s = datetime.now()
        ivt_diff_df = self.ivt.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getSTKDiff)
        print('len(ivt):', original_len, '---> ', len(ivt_diff_df))
        
        print("2. Make diff matrix -- groupby [collect_site x trust_seller x brand_name x collect_day]")
        ctbc_gdfs = ivt_diff_df.groupby(['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'COLLECT_DAY'])
        m_list = self.applyMultiProc([member_df for name, member_df in ctbc_gdfs], self.getDiffMatrix) 
        ivt_style_m_list = self.applyMultiProc(m_list, self.transformMatrix)
        result_df = pd.concat(ivt_style_m_list)
        print(">> # of groups: ", len(ctbc_gdfs),"len(result_df):", len(result_df))
    
        print("3. Merge on original ivt")
        ivt_diff_df = ivt_diff_df.sort_values('REG_DT')
        ivt_diff_df = ivt_diff_df.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT'], keep = 'last')
        merged = pd.merge(ivt_diff_df, result_df, on = ['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT'], how = 'left')

        print(">> len(ivt_drop_duplicated) <--join--> len(result_df):", len(ivt_diff_df), ' + ', len(result_df))
        print(">> ---> len(merged_df):", len(merged))
        
        
        print("4. Make final result df -- groupby [item_id x stock_id]")
        merged['REG_DT'] = merged['REG_DT_SLOT']
        del merged['REGDT_DIFF']
        del merged['STK_DIFF_x']
        
        merged = merged.rename(columns = {'STK_DIFF_y': 'STK_DIFF'})
        result = merged.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getREGDTDiff)
        
        return result

    def mute(self):
        sys.stdout = open(os.devnull, 'w')

    def applyMultiProc(self, input_list, func):
        with Pool(cpu_count(), initializer = self.mute) as p:
            output_list = p.map(func, input_list)
        return output_list
                
    def getSTKDiff(self, gdf):
        gdf['STK_DIFF'] = (gdf['STOCK_AMOUNT'].shift() - gdf['STOCK_AMOUNT']).fillna(0)
        return gdf

    def getREGDTDiff(self, gdf):
        gdf['REGDT_DIFF'] = (gdf['REG_DT'] - gdf['REG_DT'].shift())
        return gdf

    def getDiffMatrix(self, ctb_gdf_cday):
        '''
        collect_site x trust_seller x brand_name x collect_day level process.
        ctb_gdf_cday : ivt grouped by ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'COLLECT_DAY']. 
        '''
        # [step 1. make a pivot table for the item_id and stock id]
        m = pd.pivot_table(ctb_gdf_cday, index=["ITEM_ID","STOCK_ID"],values=["STK_DIFF"], columns=["REG_DT_SLOT"], aggfunc=[np.sum])
        
        # [step2] pass1: multi option item들을 item 단위로 time slot을 drop 하는 모듈
        m, passed_id_list = self.pass1(m)

        # [step3] pass2: single option item들의 time slot을 drop  
        # pass1을 통과시킨 multi option item들을 묶어서 time slot drop
        m = self.pass2(m, passed_id_list)
        
        return m
    
    
    def pass1(self, m):
        pass1_processed_item_id_list = []
        m_multi_pass1_processed = pd.DataFrame()

        # 디버깅의 용의성을 위해서 for loop 사용함. 나중에 performance tuning 필요함.
        for item_id, item_m in m.groupby(level=0): # for each item
            num_stock = len(item_m)
            if num_stock == 1: # skip single option items
                # print(item_id)
                continue
            # plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, item_m, item_id=item_id)
            for col in item_m.columns: # for each time slot
                slot_values = abs(item_m[col])
                val_cnt = slot_values[slot_values > 0].count()
                val_sum = slot_values[slot_values > 0].sum()

                if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
                    print('Drop TS - pass1:', item_id, col, num_stock, val_cnt, val_sum)
                    m.loc[item_m.index, col] = 0 # drop time slot
                    pass1_processed_item_id_list.append(item_id)

            m_multi_pass1_processed = m_multi_pass1_processed.append(m.loc[item_m.index])

        return m, pass1_processed_item_id_list
    
    def pass2(self, m, pass1_processed_item_id_list):
        if m.empty:
            return m

        stock_id_nunique_df = pd.DataFrame(m.reset_index().groupby('ITEM_ID')['STOCK_ID'].nunique()).rename(columns = {'STOCK_ID': 'STOCK_ID_NUNIQUE'})
        multi_stock_item_ids = stock_id_nunique_df[stock_id_nunique_df['STOCK_ID_NUNIQUE']>1].index
        single_stock_item_ids = stock_id_nunique_df[stock_id_nunique_df['STOCK_ID_NUNIQUE']==1].index
        print('multi_stock_item_ids',multi_stock_item_ids)
        print('single_stock_item_ids',single_stock_item_ids)

        m_multi = m.loc[multi_stock_item_ids]
        m_single = m.loc[single_stock_item_ids]

        for col in m_single.columns: # for each time slot
            sum_, diff, ts = col
            slot_values = abs(m_single[col])
            val_cnt = slot_values[slot_values > 0].count()
            val_sum = slot_values[slot_values > 0].sum()

            if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
                print('Drop TS - pass2 single', ts, val_cnt, val_sum)
                m.loc[m_single.index, col] = 0 # drop time slot
                
        # # Multi option 추후 수정 필요 -- 일단은 임시로 off -------- 
        # for col in m_multi.columns: # for each time slot
        #     sum_, diff, ts = col
        #     slot_values = abs(m_multi[col])
        #     val_cnt = slot_values[slot_values > 0].count()
        #     val_sum = slot_values[slot_values > 0].sum()

        #     if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
        #         print('Drop TS - pass2 multi', ts, val_cnt, val_sum)
        #         m.loc[m_multi.index, col] = 0 # drop time slot

        return m
    
    def transformMatrix(self, m):
        # transform matrix to ivt style
        temp = m.stack(2).reset_index()
        temp.columns = ['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT', 'STK_DIFF']
        return temp