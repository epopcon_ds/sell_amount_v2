'''
    SCM TOOL을 사용하는 SERLLER x BRAND 상품들의 IVT를 읽어서 SCM TIME SLOTS을 제거하는 모듈
    version 0.1 : nrchung.2019.05.30
'''



from datetime import datetime, timedelta
import gc
import sys
import pymongo
import pandas as pd
import numpy as np
import tqdm
from multiprocessing import Pool, cpu_count
from datetime import datetime

path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.preprocess.pqReader import pqReader
from src.preprocess.ivtFilter import ivtBaseFilter
from src.preprocess.ModifyScmDiff import ModifyScmDiff
# from src.preprocess.CheckRefData import CheckRefData


def mark_scm_filter_type(item, scm_ref_database = 'WSPIDER_LBRAND'):
    # Read scm filter data  tables
    host = '133.186.168.8'
    port = 50000
    collection = 'SELL_AMT_SCM_FILTER'
    client = pymongo.MongoClient(host, port)
    mongodb = client[scm_ref_database.upper()]
    cursor = mongodb[collection].find({}, {'_id': False})
    scm_filter_df = pd.DataFrame(list(cursor))
    columns = ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'BRAND_PRIORITY', 'SCM_PRIORITY', 'REG_DT']
    scm_filter_df = scm_filter_df[columns]
    item_scm = pd.merge(item, scm_filter_df[
        ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'SCM_PRIORITY', 'BRAND_PRIORITY']],
                        on=['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME'], how='left')

    if len(item) == len(item_scm):
        scm_filter_t1_cond = (item_scm['SCM_PRIORITY'] == 0) & (item_scm['BRAND_PRIORITY'] >= 0) & (
                    item_scm['BRAND_PRIORITY'] <= 6)
        scm_filter_t2_cond = (item_scm['SCM_PRIORITY'] == 0) & (item_scm['BRAND_PRIORITY'] == -1)
        item_scm.loc[scm_filter_t1_cond, 'SCM_FILTER'] = 'T1_FILTER_SLOT'
        item_scm.loc[scm_filter_t2_cond, 'SCM_FILTER'] = 'T2_MAX1'
        item_scm['SCM_FILTER'] = item_scm['SCM_FILTER'].fillna('T3_NORMAL')
        print("-------- SCM_FILTER TYPES ---------")
        print(item_scm['SCM_FILTER'].value_counts())
        return item_scm
    else:
        print('duplicated values in SELL_AMT_SCM_FILTER. return original item df. ')
        return item

def filter_t1_item_ivts(ivt, item, reverse = False):

    ivt_items = pd.merge(ivt, item[
        ['ITEM_ID', 'COLLECT_SITE', 'ITEM_NUM', 'TRUST_SELLER', 'BRAND_NAME', 'SCM_FILTER', 'PRICE', 'SITE_PRICE']],
                         on='ITEM_ID', how='left')

    #     print(rev_ivt_f_items['SCM_FILTER'].value_counts())
    if not reverse:
        # Left t1
        print("-------- Return T1: SCM TIME SLOT related ivt --------")
        item_ids_to_left = item.loc[item['SCM_FILTER'] == 'T1_FILTER_SLOT', 'ITEM_ID'].unique()

    else:
        # Left t2 and t3
        print("-------- Return T2 & T3: Non SCM ivt --------")
        item_ids_to_left = item.loc[item['SCM_FILTER'] != 'T1_FILTER_SLOT', 'ITEM_ID'].unique()

    ivt_items_to_left = ivt_items[ivt_items['ITEM_ID'].isin(item_ids_to_left)]
    print(len(ivt), '--->', len(ivt_items_to_left))

    return ivt_items_to_left


def get_price_ref(phis, dc, ivt):
    '''output will be unique on ['ITEM_ID', 'COLLECT_DAY'] ( not reg_dt level)'''
    # 1. preprocess collect_day format, drop duplicates, and merge both.
    ivt_org_index = ivt.index
    phis = phis.assign(COLLECT_DAY=phis['COLLECT_DAY'].apply(lambda x: ''.join(str(x).split('-'))))
    phis = phis.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
    dc = dc.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
    price = pd.merge(phis[['ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE']],
                         dc[['ITEM_ID', 'COLLECT_DAY', 'DISCOUNT_PRICE']], how='outer', on=['ITEM_ID', 'COLLECT_DAY'])

    # 2. fill 'ADD_PRICE_REF' with DISCOUNT_PRICE >> SITE_PRICE >> PRICE
    price.at[~price.PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.PRICE.isnull()].PRICE
    price.at[~price.SITE_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.SITE_PRICE.isnull()].SITE_PRICE
    price.at[~price.DISCOUNT_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.DISCOUNT_PRICE.isnull()].DISCOUNT_PRICE

    # 3. merge price with ivt
    ivt = ivt.merge(price[['ITEM_ID', 'COLLECT_DAY', 'ADD_PRICE_REF']], how='left', on=['ITEM_ID', 'COLLECT_DAY'])
    ivt['ADD_PRICE_REF'].fillna(method='ffill', inplace=True)  # for interpolated day
    ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('str')
    ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].apply(lambda price: ''.join([c for c in price if c.isdigit()]))
    # ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('int')
    ivt.index = ivt_org_index

    return ivt


def diffProcesserMainREV(params):
    item_id_part, rev_item_scm = params
    print(f"Make diff of stocks using SCM tool.--------ID PART = {item_id_part}---------")
    # 0. READ REV_HISTORY DATA - history types.
    rev_ivt = pqR.readPqFromRDB('MWS_COLT_ITEM_IVT', 'ITEM_ID_PART', item_id_part)
    rev_phis = pqR.readPqFromRDB('MWS_COLT_ITEM_PRICE_HIS', 'ITEM_ID_PART', item_id_part)
    rev_dc = pqR.readPqFromRDB('MWS_COLT_ITEM_DISCOUNT', 'ITEM_ID_PART', item_id_part)

    # 1. READY DATA
    # >> [IVT] filter bad ivt data
    rev_ivt_f = ivtBaseFilter(rev_ivt, collect_site=collect_site).forSCM()

    # >> [IVT] left T1 SCM TIME SLOT related ivts
    rev_ivt_f_t1 = filter_t1_item_ivts(rev_ivt_f, rev_item_scm)

    # 2. GET DIFF for SCM using seller & brand items.
    s = datetime.now()
    rev_ivt_f_t1_diff = ModifyScmDiff(rev_ivt_f_t1).action()
    print('TIME SPENT for modifying SCM STK_DIFF valeus : ', str(datetime.now() - s))

    # 3. Mark price_ref
    result = get_price_ref(rev_phis, rev_dc, rev_ivt_f_t1_diff)

    # 4. Delete Records not to save
    result_collect_day_list = result['COLLECT_DAY'].unique()
    result_collect_day_list.sort()
    output_collect_day_list = result_collect_day_list[1:]

    # 1) 읽어온 데이터의 첫 번째 날의 데이터를 지운다 --> 대상 collect_day 의 첫날 diff를 계산하기 위해 하루 전 데이터까지 가져왔던 것.
    # 2) REGDT_DIFF가 NaT인 데이터를 지운다 --> 대상 기간 중 수집이 시작된 첫 번째 슬롯은 STK_DIFF == 0이고, 시간 DIFF의 대상도 없기 때문에 지운다.
    output = result[result['COLLECT_DAY'].isin(output_collect_day_list)]
    output = output[~output['REGDT_DIFF'].isna()]

    # 5. Save Result
    for ocday in output_collect_day_list:
        filename = f'/processed/t1_diff_result_{ocday}_{item_id_part}.pickle'
        print("Save ", ocday, "data as", filename)
        output[output['COLLECT_DAY'] == ocday].to_pickle(base_path + filename)

    del rev_ivt
    del rev_phis
    del rev_dc
    del rev_ivt_f
    del rev_ivt_f_t1
    del rev_ivt_f_t1_diff
    del result
    del output
    gc.collect()

def diffProcesserMainNormal(params):
    item_id_part, rev_item_scm = params
    print(f"Make diff of stocks NOT using SCM tool.--------ID PART = {item_id_part}---------")
    # 0. READ REV_HISTORY DATA - history types.

    ivt = pqR.read(collection='MWS_COLT_ITEM_IVT', partition_col='ITEM_ID_PART', id_part=item_id_part)
    phis = pqR.read(collection='MWS_COLT_ITEM_PRICE_HIS', partition_col='ITEM_ID_PART', id_part=item_id_part)
    dc = pqR.read(collection='MWS_COLT_ITEM_DISCOUNT', partition_col='ITEM_ID_PART', id_part=item_id_part)

    # 1. READY DATA
    # >> [IVT] filter bad ivt data
    rev_ivt_f = ivtBaseFilter(ivt, collect_site=collect_site).forNonSCM()

    # >> [IVT] left T1 SCM TIME SLOT related ivts
    rev_ivt_f_t2_t3 = filter_t1_item_ivts(rev_ivt_f, rev_item_scm)

    # 2. GET DIFF for SCM using seller & brand items.
    s = datetime.now()
    rev_ivt_f_t2_t3_diff = ModifyScmDiff(rev_ivt_f_t2_t3).actionNormal()
    print('TIME SPENT for modifying SCM STK_DIFF valeus : ', str(datetime.now() - s))

    # 3. Mark price_ref
    result = get_price_ref(rev_phis, rev_dc, rev_ivt_f_t2_t3_diff)

    # 4. Delete Records not to save
    result_collect_day_list = result['COLLECT_DAY'].unique()
    result_collect_day_list.sort()
    output_collect_day_list = result_collect_day_list[1:]

    # 1) 읽어온 데이터의 첫 번째 날의 데이터를 지운다 --> 대상 collect_day 의 첫날 diff를 계산하기 위해 하루 전 데이터까지 가져왔던 것.
    # 2) REGDT_DIFF가 NaT인 데이터를 지운다 --> 대상 기간 중 수집이 시작된 첫 번째 슬롯은 STK_DIFF == 0이고, 시간 DIFF의 대상도 없기 때문에 지운다.
    output = result[result['COLLECT_DAY'].isin(output_collect_day_list)]
    output = output[~output['REGDT_DIFF'].isna()]

    # 5. Save Result
    for ocday in output_collect_day_list:
        filename = f'/processed/t2_t3_diff_result_{ocday}_{item_id_part}.pickle'
        print("Save ", ocday, "data as", filename)
        output[output['COLLECT_DAY'] == ocday].to_pickle(base_path + filename)

    del rev_ivt
    del rev_phis
    del rev_dc
    del rev_ivt_f
    del rev_ivt_f_t1
    del rev_ivt_f_t1_diff
    del result
    del output
    gc.collect()


###############################################
if __name__=='__main__':
    # the first collect day is just for calculate STK_DIFF of the first slot.
    sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    collect_day_window = 3


    # #########################
    # # For SCM data
    # #########################
    # # settings
    # RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4'
    # database = RDB_BI_REV
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_REV'
    # collect_site = 'not_one'
    # pqR = pqReader(base_path)
    # collect_day_list = pqR.makeCollectDaysList(sellamt_cal_day, collect_day_window)
    # #  1. READ ITEM data from RDB
    # rev_item = pqR.readItem(RDB_BI_REV)
    # # >> [ITEM] mark scm type.
    # rev_item_scm = mark_scm_filter_type(item = rev_item, scm_ref_database = 'WSPIDER_LBRAND')
    # print(rev_item_scm.groupby('COLLECT_SITE').ITEM_ID.count().reset_index(name='NUM_ITEMS').to_string())
    #
    # print('collect_day_list')
    # print(collect_day_list)
    #
    # #  2. Make t1_diff_result
    # split_list = []
    # for i in range(16, 19):
    #     packed_params = (i, rev_item_scm)
    #     diffProcesserMainREV(packed_params)
    #
    #     packed_params = (i, item_scm)
    #
    #
    # # ------------------------ 멀티프로세싱...?!
    # # with Pool(cpu_count()) as p:
    # #     p.map(diffProcesserMain, split_list)
    # print("diffProcesser for SCM is OVER !!!!! YEAY")
    # # -------------------------------------------

    #########################
    # For Normal Data
    #########################
    RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4'
    database = RDB_BI_FASHION2
    base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND'
    collect_site = 'not_one'
    pqR = pqReader(base_path)
    collect_day_list = pqR.makeCollectDaysList(sellamt_cal_day, collect_day_window)


    #  1. READ ITEM data from RDB
    item = pqR.readItem(RDB_BI_FASHION2)
    # >> [ITEM] mark scm type.
    item_scm = mark_scm_filter_type(item = item, scm_ref_database = 'WSPIDER_LBRAND')
    print(item_scm.groupby('COLLECT_SITE').ITEM_ID.count().reset_index(name='NUM_ITEMS').to_string())

    print('collect_day_list')
    print(collect_day_list)


    #  2. Make t1_diff_result
    split_list = []
    for i in range(0, 5):
        packed_params = (i, item_scm)
        diffProcesserMainNormal(packed_params)
