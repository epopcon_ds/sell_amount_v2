
from datetime import datetime, timedelta
import gc
import sys
import pymongo
import pandas as pd
import numpy as np
import tqdm
from multiprocessing import Pool, cpu_count
from datetime import datetime
# import matplotlib.pyplot as plt
# from matplotlib import rc, font_manager
# rc('font', family="NanumGothic")
# plt.rcParams['axes.unicode_minus'] = False
# from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.preprocess.pqReader import pqReader
from src.preprocess.ivtFilter import ivtBaseFilter
from src.preprocess.ModifyScmDiff import ModifyScmDiff

class CheckRefData:
    def __init__(self):
        print("check reference data using CheckRefData module")
    def mark_scm_filter_type(self, item, scm_ref_database):
        # Read scm filter data  tables
        host = '133.186.168.8'
        port = 50000
        # scm_ref_database = 'WSPIDER_LBRAND'
        collection = 'SELL_AMT_SCM_FILTER'
        client = pymongo.MongoClient(host, port)
        mongodb = client[scm_ref_database.upper()]
        cursor = mongodb[collection].find({}, {'_id': False})
        scm_filter_df = pd.DataFrame(list(cursor))
        columns = ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'BRAND_PRIORITY', 'SCM_PRIORITY', 'REG_DT']
        scm_filter_df = scm_filter_df[columns]
        item_scm = pd.merge(item, scm_filter_df[
            ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'SCM_PRIORITY', 'BRAND_PRIORITY']],
                            on=['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME'], how='left')

        if len(item) == len(item_scm):
            scm_filter_t1_cond = (item_scm['SCM_PRIORITY'] == 0) & (item_scm['BRAND_PRIORITY'] >= 0) & (
                    item_scm['BRAND_PRIORITY'] <= 6)
            scm_filter_t2_cond = (item_scm['SCM_PRIORITY'] == 0) & (item_scm['BRAND_PRIORITY'] == -1)
            item_scm.loc[scm_filter_t1_cond, 'SCM_FILTER'] = 'T1_FILTER_SLOT'
            item_scm.loc[scm_filter_t2_cond, 'SCM_FILTER'] = 'T2_MAX1'
            item_scm['SCM_FILTER'] = item_scm['SCM_FILTER'].fillna('T3_NORMAL')
            print("-------- SCM_FILTER TYPES ---------")
            print(item_scm['SCM_FILTER'].value_counts())
            return item_scm
        else:
            print('duplicated values in SELL_AMT_SCM_FILTER. return original item df. ')
            return item

    def filter_t1_item_ivts(self, ivt, item):
        print("-------- Return T1: SCM TIME SLOT related ivt --------")
        ivt_items = pd.merge(ivt, item[
            ['ITEM_ID', 'COLLECT_SITE', 'ITEM_NUM', 'TRUST_SELLER', 'BRAND_NAME', 'SCM_FILTER', 'PRICE', 'SITE_PRICE']],
                             on='ITEM_ID', how='left')
        #     print(rev_ivt_f_items['SCM_FILTER'].value_counts())
        rev_scm_t1_item_ids = item.loc[item['SCM_FILTER'] == 'T1_FILTER_SLOT', 'ITEM_ID'].unique()
        ivt_items_t1 = ivt_items[ivt_items['ITEM_ID'].isin(rev_scm_t1_item_ids)]
        print(len(ivt), '--->', len(ivt_items_t1))
        return ivt_items_t1

    def get_price_ref(self, phis, dc, ivt):
        '''output will be unique on ['ITEM_ID', 'COLLECT_DAY'] ( not reg_dt level)'''
        # 1. preprocess collect_day format, drop duplicates, and merge both.
        ivt_org_index = ivt.index
        phis = phis.assign(COLLECT_DAY=phis['COLLECT_DAY'].apply(lambda x: ''.join(str(x).split('-'))))
        phis = phis.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
        dc = dc.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
        price = pd.merge(phis[['ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE']],
                         dc[['ITEM_ID', 'COLLECT_DAY', 'DISCOUNT_PRICE']], how='outer', on=['ITEM_ID', 'COLLECT_DAY'])

        # 2. fill 'ADD_PRICE_REF' with DISCOUNT_PRICE >> SITE_PRICE >> PRICE
        price.at[~price.PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.PRICE.isnull()].PRICE
        price.at[~price.SITE_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.SITE_PRICE.isnull()].SITE_PRICE
        price.at[~price.DISCOUNT_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.DISCOUNT_PRICE.isnull()].DISCOUNT_PRICE

        # 3. merge price with ivt
        ivt = ivt.merge(price[['ITEM_ID', 'COLLECT_DAY', 'ADD_PRICE_REF']], how='left', on=['ITEM_ID', 'COLLECT_DAY'])
        ivt['ADD_PRICE_REF'].fillna(method='ffill', inplace=True)  # for interpolated day
        ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('str')
        ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].apply(lambda price: ''.join([c for c in price if c.isdigit()]))
        # ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('int') # 계속 오류남.. 이유가 뭐임
        ivt.index = ivt_org_index

        return ivt