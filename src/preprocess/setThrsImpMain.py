from datetime import datetime, timedelta
import gc
import sys
path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.data.pqReader import pqReader
from src.data.features import ThrsImpSetter



base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND'
collect_site = '11st'
sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
collect_day_window = 14



def setThrsImpMain(base_path, collect_site, sellamt_cal_day, collect_day_window):
    pqR = pqReader(base_path)
    collect_day_list = pqR.makeCollectDaysList(sellamt_cal_day, collect_day_window)

    for iip in range(0, 19):
        s = datetime.now()
        print("ITEM_ID_PART: ", iip)
        print("START Setting Thresholds and Impute Values module", str(s))

        # 1. read raw inventory data
        ivt = pqR.read(collection='MWS_COLT_ITEM_IVT', partition_col='ITEM_ID_PART', id_part=iip,
                              selected_collect_day_list=collect_day_list)

        # 2. filter inventory data for setting threshold and interpolating.
        # ivt_filtered = ivtFilter(ivt, collect_site=collect_site).outall()

        # 3. Make SLOPE THRS and IMP values.
        result = ThrsImpSetter(ivt_filtered, slope_params).applyMultiProc()

        # 4. save result into Mongo DB
        result.to_pickle('/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND/processed/thrs_imp_{iip}_{cday}.pickle')
        #result --> mongo db


if __name__ == '__main__':
	setThrsImpMain(base_path, collect_site, sellamt_cal_day, collect_day_window)