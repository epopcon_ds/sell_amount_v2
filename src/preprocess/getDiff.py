from datetime import datetime, timedelta
import gc
import sys
path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.data.pqReader import pqReader
from src.data.features import ThrsImpSetter



base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND'
collect_site = 'not_one'
sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
collect_day_window = 14

#
#
# def get_diff(base_path, collect_site, collect_day_window):
#     pqR = pqReader(base_path)
#     collect_day_list = pqR.makeCollectDaysList(sellamt_cal_day, collect_day_window)
#
#     for iip in range(0, 19):
#         s = datetime.now()
#         print("ITEM_ID_PART: ", iip)
#         print("START Setting Thresholds and Impute Values module", str(s))
#
#         # 1. read raw inventory data
#         ivt = pqR.read(collection='MWS_COLT_ITEM_IVT', partition_col='ITEM_ID_PART', id_part=iip,
#                               selected_collect_day_list=collect_day_list)
#         ivt = pqR.read(collection='MWS_COLT_ITEM_IVT', partition_col='ITEM_ID_PART', id_part=iip,
#                               selected_collect_day_list=collect_day_list)
#
#
#
#
# def get_price(sdf, database):
#     # 0. ivt. add_price
#     # 1. select * from MWS_COLT_ITEM_DISCOUNT limit 10; discount_price
#     # 2-1. select * from MWS_COLT_ITEM_PRICE_HIS limit 10; site_price
#     # 2-2. select * from MWS_COLT_ITEM_PRICE_HIS limit 10; price
#
#     sdf_org_index = sdf.index
#     item_id = sdf.ITEM_ID[:1].item()
#
#     engine = create_engine(database, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
#     collect_day = list(sdf.COLLECT_DAY.unique())
#     collect_day = tuple(collect_day*2 if len(collect_day) == 1 else collect_day)
#     query = 'select ITEM_ID, COLLECT_DAY, DISCOUNT_PRICE from MWS_COLT_ITEM_DISCOUNT where (ITEM_ID = {}) and COLLECT_DAY in {};'.format(item_id, collect_day)
#     pdc = pd.read_sql_query(query, engine)
#     query = 'select ITEM_ID, COLLECT_DAY, SITE_PRICE, PRICE, REG_DT from MWS_COLT_ITEM_PRICE_HIS where (ITEM_ID = {}) and COLLECT_DAY in {};'.format(item_id, collect_day)
#     phis = pd.read_sql_query(query, engine)
#     phis['COLLECT_DAY'] = phis.REG_DT.dt.strftime('%Y%m%d')
#     del phis['REG_DT']
#     engine.dispose()
#
#     price = pdc.merge(phis, how='outer', on=['ITEM_ID', 'COLLECT_DAY'])
#     price.at[~price.PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.PRICE.isnull()].PRICE
#     price.at[~price.SITE_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.SITE_PRICE.isnull()].SITE_PRICE
#     price.at[~price.DISCOUNT_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.DISCOUNT_PRICE.isnull()].DISCOUNT_PRICE
#     sdf = sdf.merge(price[['ITEM_ID', 'COLLECT_DAY', 'ADD_PRICE_REF']], how='left', on=['ITEM_ID', 'COLLECT_DAY'])
#     sdf['ADD_PRICE_REF'].fillna(method='ffill', inplace=True) # for interpolated day
#
#     sdf.index = sdf_org_index
#     sdf['ADD_PRICE']=sdf['ADD_PRICE'].astype(int)
#     sdf['ADD_PRICE_REF']=sdf['ADD_PRICE_REF'].astype(int)
#     #sdf['ADD_PRICE']=sdf['ADD_PRICE']+sdf['ADD_PRICE_REF'] # not consistent
#     sdf['ADD_PRICE']=sdf['ADD_PRICE_REF'] # rough price # not consistent
#     del sdf['ADD_PRICE_REF']
#
#     return sdf
