# DB related
from sqlalchemy import create_engine
import pymysql
pymysql.install_as_MySQLdb()

# Data Wrangling related
from datetime import datetime, timedelta
import pandas as pd
from pyarrow import parquet as pq
import gc
import glob
from tqdm import tqdm

class pqReader():
    def __init__(self, base_path):
        self.base_path = base_path

    def makeCollectDaysList(self, sellamt_cal_day, collect_day_window):
        # make a list of days need for the threshold setting process.
        self.collect_day_start = datetime.strptime(sellamt_cal_day, '%Y-%m-%d') - timedelta(days=collect_day_window)
        self.collect_day_list = pd.date_range(self.collect_day_start, periods=collect_day_window + 1).tolist()
        self.collect_day_list = [datetime.strftime(i, '%Y-%m-%d') for i in self.collect_day_list]
        collect_day_list_two_types = self.collect_day_list + [i.replace('-', '') for i in self.collect_day_list]
        
        collect_day_list_two_types.sort()
        self.collect_day_list = collect_day_list_two_types
        self.today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        return self.collect_day_list

    def read(self, collection, partition_col, id_part, selected_collect_day_list = None):
        # Read history-type data from MongoDB(saved by COLLECT_DAY)
        if selected_collect_day_list:
            self.collect_day_list = selected_collect_day_list
        
        collection_part_path = self.base_path +'/raw/'+ collection + "/*"

        files = glob.glob(collection_part_path + f'/{partition_col}={id_part}')
        # print([ f.split(".")[0].split("_")[-1] for f in files])
        # print(self.collect_day_list)
        files_selected = [f for f in files if f.split(".")[0].split("_")[-1] in self.collect_day_list]
        # print(files_selected)

        df_full = pd.DataFrame()
        for i in files_selected:
            table = pq.read_table(i, use_threads=10)
            df_temp = table.to_pandas()
            df_full = df_full.append(df_temp, sort=False)
            del df_temp
            gc.collect()

        df_full = df_full.drop_duplicates()
        read_collectday_list = df_full['COLLECT_DAY'].unique()
        print(f'unique collect day in the {collection} dataframe: ')
        print(read_collectday_list)

        return df_full

    def readPqFromRDB(self, rdb_table, partition_col, id_part, selected_collect_day_list=None):
        # Read history-type data from RDB (saved by ID column)
        if selected_collect_day_list:
            self.collect_day_list = selected_collect_day_list

        rdb_table_part_path = self.base_path + f'/raw/{rdb_table}/*'
        files = glob.glob(rdb_table_part_path + f'/{partition_col}={id_part}')
        files_selected = [f for f in files if set(f.split(".")[0].split("_")[-4:-2]) & set(self.collect_day_list)]
        # print(f'pq files selected for {rdb_table} and {partition_col}')
        # print(files_selected)

        df_full = pd.DataFrame()
        for i in tqdm(files_selected):
            table = pq.read_table(i, use_threads=10)
            df_temp = table.to_pandas()
            df_full = df_full.append(df_temp, sort=False)
            del df_temp
            gc.collect()

        df_full = df_full.drop_duplicates()
        read_collectday_list = df_full['COLLECT_DAY'].unique()
        print(f'unique collect day in the {rdb_table} dataframe: ')
        print(read_collectday_list)
        return df_full

    def readItem(self, database, selected_collect_day_list = None):
        # Read update-type data
        # ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s
        if selected_collect_day_list:
            self.collect_day_list = selected_collect_day_list
        self.uptdt_from = min(self.collect_day_list)
        # item_path = self.base_path + '/raw/MWS_COLT_ITEM/'
        query = f'select ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, COLLECT_URL, TRUST_SELLER, BRAND_NAME, ' \
                f'ORG_GOODS_NUM, PRICE, SITE_PRICE from MWS_COLT_ITEM where UPT_DT >= "{self.uptdt_from}";'
        engine = create_engine(database, encoding='utf8', pool_size=50, pool_recycle=3600,
                               connect_args={'connect_timeout': 1000000})
        db_rows = pd.read_sql_query(query, engine)
        engine.dispose()
        print("READ MWS_COLT_ITEM table from RDB *DIRECTLY*")
        print(">> uptdt_from : ", self.uptdt_from)
        #
        # selected_item_file = [i for i in files if i.split(f'item_')[1].split(".")[0] == self.uptdt_range_toquery]
        # print('selected_item_file', selected_item_file)
        # if selected_item_file:
        #     table = pq.read_table(selected_item_file[0], use_threads=10)
        #     item_df = table.to_pandas()
        #     return item_df
        # else:
        #     print('no new item data. read from DB first.... ')

        return db_rows




