# input : ivt 
# 
class ThrsImpSetter:
    def __init__(self, ivt, slope_params):
        self.ivt = ivt
        self.slope_params = slope_params
        self.collect_site = slope_params['COLLECT_SITE']
        self.unit_hour = slope_params['UNIT_HOUR']
        self.min_thrs_puh = slope_params['MIN_THRS_PUH']
        self.min_imp_puh = slope_params['MIN_IMP_PUH']
        self.replace_zero_diff = slope_params['REPLACE_ZERO_DIFF']
        self.z_score_max = slope_params['Z_SCORE_MAX']
        
        self.reg_id = 'SELL_AMOUNT_V2'
        
    def applyMultiProc(self):
        groupedIvt = ivt_filtered.groupby(['ITEM_ID', 'STOCK_ID'])
        with Pool(cpu_count()) as p:
            df_list = p.map(self.getResult, [sdf for name, sdf in groupedIvt])
        
        return pd.concat(df_list)

    
    def getResult(self, sdf):
                
        iid = sdf['ITEM_ID'].unique()[0]
        sid = sdf['STOCK_ID'].unique()[0]
        
        down_slopes, reasonable_slopes_max_psec = self.getRsnbSlpMx(sdf)
        stock_diff_thrs_puh , impute_value_puh = self.setThrsImp(down_slopes, reasonable_slopes_max_psec)
        
#         return [self.iid, self.sid, self.stock_diff_thrs_puh, self.impute_value_puh, self.unit_hour]
        
#         print('RESULT ---- ')
#         print({'ITEM_ID': [self.iid], 'STOCK_ID': [self.sid], 'STOCK_DIFF_THRS_PUH': [self.stock_diff_thrs_puh],
#                              'IMP_VALUE_PUH': [self.impute_value_puh], 'UNIT_HOUR': [self.unit_hour]})

        return pd.DataFrame({'ITEM_ID': [iid], 'STOCK_ID': [sid], 'STOCK_DIFF_THRS_PUH': [stock_diff_thrs_puh],
                             'IMP_VALUE_PUH': [impute_value_puh], 'UNIT_HOUR': [self.unit_hour], 'REG_ID':[self.reg_id], 'REG_DT':[datetime.now()]})
        

    def getRsnbSlpMx(self, sdf):
        '''
        newer version ! as of 190515
        '''
        # ----------------------------------------------------
        # 1) check if the stock is appropriate for setting threshold
        # cond1) Is data collection period longer than 48 hours?
        # cond2) Is there more than 7 data points?
        collection_period = sdf.REG_DT.max() - sdf.REG_DT.min()
        cond1 = collection_period < timedelta(hours=48)
        cond2 = len(sdf) < 7
        
        # ----------------------------------------------------
        # 2) calculate mod_z applied threshold for the slope
        if (cond1 | cond2):
            # 2-1) if under cond1 or cond2, set reasonalbe_slope_max as None.  
            down_slopes = None
            reasonable_slopes_max_psec = None
            
        else:
            # 2-2) if not, set reasonable_slope_max using modified-z score.
            # (1) get down slopes
            down_slopes = self.getSlopes(sdf)
            
            # (2) calculate reasonable max slope **per a second** (could be None)
            if (len(down_slopes) != 0):
                reasonable_slopes_max_psec = self.calSlopeMax(down_slopes)
            else:
                reasonable_slopes_max_psec = None
                
        
        return down_slopes, reasonable_slopes_max_psec
                
    def setThrsImp(self, down_slopes, reasonable_slopes_max_psec):
        # ----------------------------------------------------
        # 3) set stock diff thrs & impute value (per a unit hour)
        if reasonable_slopes_max_psec:
            # 3-1) set threshold and impute values according to the reasonable slope max.
            # (1) set thrs
            reasonable_slope_max_puh = round(reasonable_slopes_max_psec * 60 * 60 * self.unit_hour)
            stock_diff_thrs_puh = max(reasonable_slope_max_puh, self.min_thrs_puh)
            # (2) set impute value
            normal_slope_condition = down_slopes <= reasonable_slopes_max_psec
            normal_slope_median_puh = round((down_slopes[normal_slope_condition]).median() * 60 * 60 * self.unit_hour)
            impute_value_puh = max(normal_slope_median_puh, self.min_imp_puh)

        else:
            # If reasonable_slopes_max_psec is None (on step #1 or #3-2)
            # Take min_allow_thrs_puh & min_allow_imp_puh
            stock_diff_thrs_puh = self.min_thrs_puh
            impute_value_puh = self.min_imp_puh
        
        return stock_diff_thrs_puh, impute_value_puh

    
    def getSlopes(self, sdf):
        # (0) filter deal period data
        # sdf['STK_DIFF'] = (sdf['STOCK_AMOUNT'].shift() - sdf['STOCK_AMOUNT'])
        # sdf['REGDT_DIFF'] = (sdf['REG_DT'] - sdf['REG_DT'].shift())
        ivt_ids_except_deal_period = sdf[(~sdf['REGDT_DIFF'].isna()) & (sdf['REGDT_DIFF'] > '04:00:00')].index
        sdf = sdf[sdf.index.isin(ivt_ids_except_deal_period)]
        
        # (1) get stock_amount_diff
        stock_amount_diff = sdf['STK_DIFF'].fillna(0)
        stock_amount_diff = stock_amount_diff.replace(0, self.replace_zero_diff)

        # (2) get reg_dt_diff
        reg_dt_diff = sdf['REGDT_DIFF'].apply(lambda x: x.total_seconds())
        reg_dt_diff_median = (reg_dt_diff[(reg_dt_diff > timedelta(hours=1).total_seconds())]).median()
        # if reg_dt_diff is too short(less than reg_dt_diff_median), replace reg_dt_diff with the half of reg_dt_diff_median.
        reg_dt_diff = reg_dt_diff.apply(lambda rddiff: reg_dt_diff_median * .5 if rddiff < reg_dt_diff_median else rddiff)

        # (3) get slope
        slop_per_sec = stock_amount_diff / reg_dt_diff
        down_slopes = slop_per_sec[slop_per_sec > 0]
        
        return down_slopes


    def calSlopeMax(self, down_slopes):
        # calculate mod_z score for each down_slope value & set stock_diff threshold for outlier dectection
        '''
        Calculate reasonable max value (per second) of slopes according to mod-z score.

        > Conditions :
        To calculate reasonable_slope_max_ph, MAD should not be zero.

        > Params :
        - down_slopes : pd.Series. dropped STOCK AMOUNT.
        - z_score_max : int. baseline for cutting modified-z socre outliers.

        > Return :
        - reasonable_slopes_max_psec : reasonable slope max value per a second

        > References:
        - z_score_max : https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm
        "These authors recommend that modified Z-scores with an absolute value of greater than 3.5
        be labeled as potential outliers."
        '''
        down_slopes_median = np.median(down_slopes)
        MAD = np.median(np.abs(down_slopes - down_slopes_median))
        condition = (MAD != 0)
        if condition:
            #         print("Set reasonable_slopes_max_psec as max(normal_down_slopes)")
            modified_z_scores = 0.6745 * ((down_slopes - down_slopes_median) / MAD)
            normal_down_slopes = down_slopes[modified_z_scores < z_score_max]
            reasonable_slopes_max_psec = max(normal_down_slopes)
        else:
            #         print("Set reasonable_slopes_max_psec as None")
            reasonable_slopes_max_psec = None
        
        return reasonable_slopes_max_psec
