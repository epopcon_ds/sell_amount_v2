import pandas as pd
import pandas as pd


class ivtBaseFilter():
    def __init__(self, ivt_df, collect_site):
        print('-------- Filter Bad IVT data ---')
        self.df = ivt_df
        self.original_df_size = len(ivt_df)
        self.collect_site = collect_site

    def forSCM(self):
        print('>> Filter Out IVT for SqCM setting')
        self.outBugs()
        self.outNochangedItems()
        # [Transformation] Sort values by ID
        self.df.sort_values(['ITEM_ID', 'STOCK_ID', 'REG_DT'], inplace=True)
        self.df = self.df.set_index('ID')
        # self.df = self.df.reset_index(drop=True, inplace=True)
        return self.df

    def forNonSCM(self):
        print('>> Filter Out All Conditions')
        self.outBugs()
        self.outNochangedItems()
        self.outZeroStocks()
        # [Transformation] Sort values by ID
        self.df.sort_values(['ITEM_ID', 'STOCK_ID', 'REG_DT'], inplace=True)
        self.df = self.df.set_index('ID')
        # self.df = self.df.reset_index(drop=True, inplace=True)
        return self.df

    def outBugs(self):
        # [Data collection bug] filter out 11st bug
        if self.collect_site == '11st':
            self.df = self.df[self.df['STOCK_AMOUNT'] >= 0]
            self.df = self.df[self.df['STOCK_ID'] != '-1']
            print(">>>> outBug - 11st - Result :", self.original_df_size, "-->", len(self.df))

        if self.df.empty:  # no ivt left
            self.df = pd.DataFrame()

        # [Data collection bug] filter out collected during mis-collected periods
        if self.collect_site == 'coupang':
            self.df = self.df[
                ~(self.df.REG_DT.between('2019-01-02 13:00:00', '2019-01-04 18:30:00') & (self.df.STOCK_AMOUNT == 0))]
            print(">>>> outBug - coupang - Result :", self.original_df_size, "-->", len(self.df))
        if self.df.empty:  # no ivt left
            self.df = pd.DataFrame()

        # [MongoDB transfer bug] drop duplicates by ['ITEM_ID', 'STOCK_ID', 'REG_DT']
        self.df = self.df.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT'], keep='last')
        if self.df.empty:  # no ivt left
            self.df = pd.DataFrame()
        print(">>>> outBug - mongo transf - Result :", self.original_df_size, "-->", len(self.df))

    def outNochangedItems(self):
        # [No-change items] filter items which have no changes.(only left have been changed ivt data)
        stk = self.df.groupby(['ITEM_ID', 'STOCK_ID']).STOCK_AMOUNT.nunique().reset_index()
        is_stock_changed = (stk.STOCK_AMOUNT > 1)
        stk_changed = stk.loc[is_stock_changed, ['ITEM_ID', 'STOCK_ID']]

        if stk_changed.empty:  # Any items' ivt have been changed
            self.df = pd.DataFrame()
        else:
            self.df = pd.merge(stk_changed, self.df, how='left', on=['ITEM_ID', 'STOCK_ID'])
            print(">>>> outNochanged stock - Result :", self.original_df_size, "-->", len(self.df))

    def outZeroStocks(self):
        # [Dropped to 0 ivts] filter items which have dropped to zero
        self.df = self.df[self.df.STOCK_AMOUNT != 0]
        print(">>>> outZeroStocks :", self.original_df_size, "-->", len(self.df))

    def __del__(self):
        print('Deleted IVT Filter instance.')
#
# class ivtBaseFilter():
#     def __init__(self, ivt_df, collect_site):
#         print('--- Filter Bad IVT data ---')
#         self.df = ivt_df
#         self.original_df_size = len(ivt_df)
#         self.collect_site = collect_site
#
#     def forSCM(self):
#         print('>> Filter Out IVT for SCM setting')
#         self.outBugs()
#         self.outNochangedItems()
#         # [Transformation] Sort values by ID
#         self.df = self.df.set_index('ID')
#         self.df = self.df.sort_values(['ITEM_ID', 'STOCK_ID', 'REG_DT'], inplace=True)
#         # self.df = self.df.reset_index(drop=True, inplace=True)
#         return self.df
#
#
#     def forNotSCM(self):
#         print('>> Filter Out All Conditions')
#         self.outBugs()
#         self.outNochangedItems()
#         self.outZeroStocks()
#         # [Transformation] Sort values by ID
#         self.df = self.df.set_index('ID')
#         self.df = self.df.sort_values(['ITEM_ID', 'STOCK_ID', 'REG_DT'], inplace=True)
#         # self.df = self.df.reset_index(drop=True, inplace=True)
#         return self.df
#
#     def outBugs(self):
#         # [Data collection bug] filter out 11st bug
#         if self.collect_site == '11st':
#             self.df = self.df[self.df['STOCK_AMOUNT'] >= 0]
#             self.df = self.df[self.df['STOCK_ID'] != '-1']
#             print(">>>> outBug - 11st - Result :", self.original_df_size, "-->", len(self.df))
#
#         if self.df.empty: # no ivt left
#             self.df = pd.DataFrame()
#
#         # [Data collection bug] filter out collected during mis-collected periods
#         if self.collect_site == 'coupang':
#             self.df = self.df[~(self.df.REG_DT.between('2019-01-02 13:00:00', '2019-01-04 18:30:00') & (self.df.STOCK_AMOUNT == 0))]
#             print(">>>> outBug - coupang - Result :", self.original_df_size, "-->", len(self.df))
#         if self.df.empty: # no ivt left
#             self.df = pd.DataFrame()
#
#         # [MongoDB transfer bug] drop duplicates by ['ITEM_ID', 'STOCK_ID', 'REG_DT']
#         self.df = self.df.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT'], keep = 'last')
#         if self.df.empty: # no ivt left
#             self.df = pd.DataFrame()
#         print(">>>> outBug - mongo transf - Result :", self.original_df_size, "-->", len(self.df))
#
#     def outNochangedItems(self):
#         # [No-change items] filter items which have no changes.(only left have been changed ivt data)
#         stk = self.df.groupby(['ITEM_ID', 'STOCK_ID']).STOCK_AMOUNT.nunique().reset_index()
#         is_stock_changed = (stk.STOCK_AMOUNT > 1)
#         stk_changed = stk.loc[is_stock_changed, ['ITEM_ID', 'STOCK_ID']]
#
#         if stk_changed.empty: # Any items' ivt have been changed
#             self.df = pd.DataFrame()
#         else:
#             self.df = pd.merge(stk_changed, self.df, how='left', on=['ITEM_ID', 'STOCK_ID'])
#             print(">>>> outNochanged stock - Result :", self.original_df_size, "-->", len(self.df))
#
#     def outZeroStocks(self):
#         # [Dropped to 0 ivts] filter items which have dropped to zero
#         self.df = self.df[self.df.STOCK_AMOUNT != 0]
#         print(">>>> outZeroStocks :", self.original_df_size, "-->", len(self.df))
#
#     def __del__(self):
#         print('Deleted IVT Filter instance.')