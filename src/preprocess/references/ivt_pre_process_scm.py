'''
    SCM TOOL을 사용하는 SERLLER x BRAND 상품들의 IVT를 읽어서 SCM TIME SLOTS을 제거하는 모듈
    version 0 : tkcho.2019.05.24  
'''

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc, font_manager
rc('font', family="NanumGothic")
plt.rcParams['axes.unicode_minus'] = False
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import tqdm
from multiprocessing import Pool
from db_interface import *

# -------------------------------------------    
RDB_BI_GFK_11ST = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_11st?charset=utf8mb4' 
RDB_BI_GFK_COUPANG = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_coupang?charset=utf8mb4' 
RDB_BI_GFK_TMON = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_tmon?charset=utf8mb4' 
RDB_BI_GFK_WEMAKEPRICE = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_wemakeprice?charset=utf8mb4' 
RDB_BI_GFK_HMALL = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_hmall?charset=utf8mb4' 
RDB_BI_FASHION1 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider?charset=utf8mb4'
RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4' 
RDB_BI_AP = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_ap?charset=utf8mb4' 
RDB_BI_OY = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_oliveyoung?charset=utf8mb4' 
RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4' 
RDB_BI_SIVILLAGE = 'mysql://wspider:wspider00!q@13.124.149.212:3306/wspider?charset=utf8mb4' 
#RDB_BI_AP_M = 'mysql://wspidermr:wspidermr00!q@133.186.134.175:3306/ap-bigdata-v2?charset=utf8mb4' 
# -------------------------------------------    

DATE_ITEM_UPT_DT = "2019-05-18"
DATE_IVT_REG_DT = "2019-05-18"

# -------------------------------------------    

def file_dump(df, df_name):
    os.makedirs('./excel/', exist_ok=True) # make cache dir if not exists
    os.makedirs('./pickle/', exist_ok=True) # make cache dir if not exists
    
    df.to_excel('./excel/' + f'{df_name}.xlsx')
    df.to_pickle('./pickle/' + f'{df_name}.pickle')

#-----------------------------------------------------------
def plot_m_scatter(collect_site, trust_seller, brand_name, collect_day, m_org, item_id=None, label=None):
    return

    print('plot_m_scatter', collect_site, trust_seller, brand_name, collect_day, item_id, label)
    m = m_org.copy()    
    if item_id:
        title = f'm scatter {trust_seller} {collect_site} {brand_name} {collect_day} {item_id}'
    else:    
        title = f'm scatter {trust_seller} {collect_site} {brand_name} {collect_day} {label}'

    file_name = './plot/' + title.replace('/', '') + '.png'
    
    if len(m) == 0:
        return 
    
    #fig, ax = plt.subplots(figsize=(10, 10), dpi=400)
    fig, ax = plt.subplots(figsize=(20, 60), dpi=400)
    plt.xticks(color='black', fontsize=6, rotation='vertical')
    plt.yticks(color='black', fontsize=6, rotation='horizontal')
    
    m.fillna(0, inplace=True) # for visualization

    x = []
    y = []
    for col in m.columns:
        sum_, diff, ts = col
        slot_values = abs(m[col])
        val_cnt = slot_values[slot_values > 0].count()
        val_sum = slot_values[slot_values > 0].sum()
        x.append(val_cnt)
        y.append(val_sum)
        # print(ts, val_cnt, val_sum)

    def rand_jitter(arr):
        stdev = .01*(max(arr)-min(arr))
        return arr + np.random.randn(len(arr)) * stdev
        
    #print(x)
    #print(y)
    
    x=rand_jitter(x)
    y=rand_jitter(y)
    
    #print(x)
    #print(y)
    
    #ax.set_xticks(np.arange(len(x)))
    #ax.set_yticks(np.arange(len(y)))

    #ax.set_xticklabels(x)
    #ax.set_yticklabels(y)

    ax.scatter(x, y)
    
    ax.set_title(title)
    
    plt.savefig(f'{file_name}', bbox_inches='tight')
    plt.close()

def plot_m_3d(collect_site, trust_seller, brand_name, collect_day, m_org, item_id=None, label=None):
    return 
    print('m_dist_log', collect_site, trust_seller, brand_name, collect_day, item_id, label)
    m = m_org.copy()

    if item_id:
        title = f'm dist {trust_seller} {collect_site} {brand_name} {collect_day} {item_id}'
    else:    
        title = f'm dist {trust_seller} {collect_site} {brand_name} {collect_day} {label}'

    file_name = './plot/' + title.replace('/', '') + '.png'
    
    if len(m) == 0:
        return 
    
    m.fillna(0, inplace=True) # for visualization
    
    xy = []
    for col in m.columns:
        sum_, diff, ts = col
        slot_values = abs(m[col])
        val_cnt = slot_values[slot_values > 0].count()
        val_sum = slot_values[slot_values > 0].sum().astype(int)
        xy.append((val_cnt, val_sum))

    xy = pd.DataFrame(xy, columns=['x', 'y'])    
    xy = xy.groupby(['x', 'y']).x.count().reset_index(name='z')
    
    
    # 3d plot
    fig = plt.figure(figsize=(10, 20))
    ax1 = fig.add_subplot(111, projection='3d')

    xpos = xy.x
    ypos = xy.y
    dz = xy.z
    width = (1/100) * max(ypos)

    assert len(xpos) == len(ypos) == len(dz)

    num_data_point = len(xpos)
    zpos = [0] * num_data_point
    dx = [width] * num_data_point
    dy = [width] * num_data_point
    
    ax1.margins(x=0)
    ax1.margins(y=0)
    ax1.margins(z=0)

    ax1.set_title(title)
    ax1.set_xlim(0, max(ypos))
    ax1.set_xlabel('diff points count')
    ax1.set_ylabel('diff sum')
    ax1.set_zlabel('num time slots')
    ax1.bar3d(xpos, ypos, zpos, dx, dy, dz, color='#00ceaa')
    ax1.view_init(elev=10., azim=256)
    #plt.show()
    plt.savefig(file_name)
    plt.close()
    

def plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, m_org, item_id=None, label=None):
    return 
    m = m_org.copy()
    print('plot_m_heatmap', collect_site, trust_seller, brand_name, collect_day, item_id, label)

    if len(m) == 0:
        return 
    
    #fig, ax = plt.subplots(figsize=(10, 10), dpi=400)
    fig, ax = plt.subplots(figsize=(20, 60), dpi=400)
    plt.xticks(color='black', fontsize=6, rotation='vertical')
    plt.yticks(color='black', fontsize=6, rotation='horizontal')
    
    m.fillna(0, inplace=True) # for visualization
    
    im = ax.imshow(m.abs().values)
    
    x = [ts for (sum_, diff, ts) in m.columns]
    y = m.index
    ax.set_xticks(np.arange(len(x)))
    ax.set_yticks(np.arange(len(y)))

    ax.set_xticklabels(x)
    ax.set_yticklabels(y)
    
    if item_id:
        title = f'm heatmap {trust_seller} {collect_site} {brand_name} {collect_day} {item_id}'
    else:    
        title = f'm heatmap {trust_seller} {collect_site} {brand_name} {collect_day} {label}'
        
    file_name = './plot/' + title.replace('/', '') + '.png'
    ax.set_title(title)
    
    plt.savefig(file_name, bbox_inches='tight')
    plt.close()
#-----------------------------------------------------------

# diff matrix를 구성함
def get_diff_matrix(database, items, collect_day):
    item_id_list = list(items.ITEM_ID.sort_values())
    
    today = datetime.strptime(collect_day, '%Y-%m-%d')
    yesterday = today - timedelta(days=1)

    ivt = pd.DataFrame()
    
    for item_id in item_id_list:
        ivt_yesterday = get_ivt(database, item_id, yesterday.strftime('%Y%m%d'))
        ivt_today = get_ivt(database, item_id, today.strftime('%Y%m%d'))
        #ivt_tmp = ivt_yesterday.append(ivt_today)
        ivt = ivt.append(ivt_yesterday.append(ivt_today))

    if ivt.empty:
        return pd.DataFrame()
    
    ivt.sort_values(['ITEM_ID', 'STOCK_ID', 'REG_DT'], inplace=True)
    ivt.reset_index(drop=True, inplace=True)
    ivt['REG_DT'] = ivt.REG_DT.apply(lambda dt : dt.replace(minute = 0 if dt.minute < 30 else 30, second=0, microsecond=0))
    ivt['DIFF'] = 0
    
    # code의 가독성 측면과 디버깅 용이성 에서 groupby -> for loop이 groupby -> apply 보다 나은 것 같음. (performace는 나중에 고려하자...)
    for (item_id, stock_id), isivt in ivt.groupby(['ITEM_ID', 'STOCK_ID']):
        diff = (isivt.STOCK_AMOUNT.shift() - isivt.STOCK_AMOUNT).fillna(0)
        ivt.loc[isivt.index, 'DIFF'] = diff

    ivt = ivt[ivt.COLLECT_DAY == today.strftime('%Y%m%d')]
    #print(ivt[['ITEM_ID', 'STOCK_ID', 'DIFF', 'STOCK_AMOUNT', 'REG_DT']].head(10))
    
    #m = pd.pivot_table(ivt, index=["ITEM_ID","STOCK_ID"],values=["DIFF"], columns=["REG_DT"], aggfunc=[np.sum], fill_value=0)
    m = pd.pivot_table(ivt, index=["ITEM_ID","STOCK_ID"],values=["DIFF"], columns=["REG_DT"], aggfunc=[np.sum])
    #matrix.reset_index(inplace=True)
    
    #m.to_pickle(f'matrix_{collect_day}.pickle')
    
    return m

# diff matrix중에서 diff가 모두 0인 stock을 버림
def get_m(task_param):
    database, collect_site, trust_seller, brand_name, collect_day, items_seller_brand = task_param

    m = get_diff_matrix(database, items_seller_brand, collect_day)

    if m.empty:
        return (collect_site, trust_seller, brand_name, collect_day, m)
    
    #seller_name = trust_seller.replace("/", "")
    #m.to_pickle(f'matrix_{seller_name}_{collect_site}_{brand_name}_{collect_day}.pickle')
    #m.to_excel(f'matrix_{seller_name}_{collect_site}_{brand_name}_{collect_day}.xlsx')

    # 전체 diff가 0인 item은 삭제하는 부분.
    # sell_amt의 계산의 효율성 향상을 위함. 그러나 판매량 '0'도 확인해야 하는 상황에서는 skip해야 하는 부분임.
    m_non_zero = pd.DataFrame()
    for item_id, item_m in m.groupby(level=0):
        if any(v != 0 for col in item_m.fillna(0).values for v in col):
            m_non_zero = m_non_zero.append(item_m)
    
    #return (collect_site, trust_seller, brand_name, collect_day, m_non_zero)
    return (collect_site, trust_seller, brand_name, collect_day, m)

# pass1, pass2를 통과시킴
def pre_process_m(task_param):
    collect_site, trust_seller, brand_name, collect_day, m = task_param
    #print('pre_process_m', collect_site, trust_seller, brand_name, collect_day)
    m, pass1_processed_item_id_list = pass1(collect_site, trust_seller, brand_name, collect_day, m)
    m = pass2(collect_site, trust_seller, brand_name, collect_day, m, pass1_processed_item_id_list)
    return m

# multi option item들을 item 단위로 time slot을 drop 하는 모듈
def pass1(collect_site, trust_seller, brand_name, collect_day, m):
    print('pass1', collect_site, trust_seller, brand_name, collect_day)
    
    if m.empty:
        return m, []
    
    title = f'm {trust_seller} {collect_site} {brand_name} {collect_day}'
    file_name = title.replace('/', '')
    
    file_dump(m, f'pass1_{file_name}_before')
    
    pass1_processed_item_id_list = []
    m_multi_pass1_processed = pd.DataFrame()
    
    # 디버깅의 용의성을 위해서 for loop 사용함. 나중에 performance tuning 필요함.
    for item_id, item_m in m.groupby(level=0): # for each item
        plot_m_3d(collect_site, trust_seller, brand_name, collect_day, item_m, item_id=item_id, label=None)
        plot_m_scatter(collect_site, trust_seller, brand_name, collect_day, item_m, item_id=item_id, label=None)
        num_stock = len(item_m)
        
        if num_stock == 1: # skip single option items
            # print(item_id)
            continue

        # plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, item_m, item_id=item_id)
        
        for col in item_m.columns: # for each time slot
            sum_, diff, ts = col
            slot_values = abs(item_m[col])
            val_cnt = slot_values[slot_values > 0].count()
            val_sum = slot_values[slot_values > 0].sum()

            if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
                print('pass1', item_id, ts, num_stock, val_cnt, val_sum)
                m.loc[item_m.index, col] = 0 # drop time slot
                pass1_processed_item_id_list.append(item_id)
                
        m_multi_pass1_processed = m_multi_pass1_processed.append(m.loc[item_m.index])

    plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, m_multi_pass1_processed, label='multi-pass1-processed')
    
    file_dump(m, f'pass1_{file_name}_after')
    
    return m, pass1_processed_item_id_list

# single option item들의 time slot을 drop  
# pass1을 통과시킨 multi option item들을 묶어서 time slot drop
def pass2(collect_site, trust_seller, brand_name, collect_day, m, pass1_processed_item_id_list):
    print('pass2', collect_site, trust_seller, brand_name, collect_day)
    if m.empty:
        return m
    
    title = f'm {trust_seller} {collect_site} {brand_name} {collect_day}'
    file_name = title.replace('/', '')
    
    file_dump(m, f'pass2_{file_name}_before')
    
    m_multi  = pd.DataFrame()
    m_single = pd.DataFrame()
    
    # 디버깅의 용의성을 위해서 for loop 사용함. 나중에 performance tuning 필요함.
    for item_id, item_m in m.groupby(level=0): # for each item
        num_stock = len(item_m)
        
        if num_stock == 1: # skip single option items
            m_single = m_single.append(item_m)
        else:    
            if not item_id in pass1_processed_item_id_list:
                m_multi = m_multi.append(item_m)

    m_single.to_excel('m_single.xlsx')
    m_multi.to_excel('m_multi.xlsx')
    
    file_dump(m_single, f'pass2_m_single_{file_name}_before')            
    file_dump(m_multi, f'pass2_m_multi_{file_name}_before')            
    
    plot_m_3d(collect_site, trust_seller, brand_name, collect_day, m_single, label='single-candidates')
    plot_m_scatter(collect_site, trust_seller, brand_name, collect_day, m_single, label='single-candidates')
    plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, m_single, label='single-candidates')
    plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, m_multi, label='multi-pass2-candidates')
    
    for col in m_single.columns: # for each time slot
        sum_, diff, ts = col
        slot_values = abs(m_single[col])
        val_cnt = slot_values[slot_values > 0].count()
        val_sum = slot_values[slot_values > 0].sum()

        if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
            print('pass2 single', ts, val_cnt, val_sum)
            m.loc[m_single.index, col] = 0 # drop time slot
            
    for col in m_multi.columns: # for each time slot
        sum_, diff, ts = col
        slot_values = abs(m_multi[col])
        val_cnt = slot_values[slot_values > 0].count()
        val_sum = slot_values[slot_values > 0].sum()

        if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
            print('pass2 multi', ts, val_cnt, val_sum)
            m.loc[m_multi.index, col] = 0 # drop time slot

    file_dump(m, f'pass2_{file_name}_after')            
    plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, m, label='all-done')
    return m

# -------------------------------
def get_sell_amt(p):
    if p.empty:
        return pd.DataFrame([], columns = ['ITEM_ID', 'STOCK_ID', 'COLLECT_DAY', 'SELL_AMOUNT'])
    
    collect_day = p.columns[0][2].to_pydatetime().strftime('%Y-%m-%d')
    row = []
    
    for (item_id, stock_id), sells_in_a_day in p.iterrows():
        # print(item_id, stock_id, sells_in_a_day.sum())
        row.append((item_id, stock_id, collect_day, sells_in_a_day.sum()))
    
    return pd.DataFrame(row, columns = ['ITEM_ID', 'STOCK_ID', 'COLLECT_DAY', 'SELL_AMOUNT'])

def make_report(s):
    if s.empty:
        return pd.DataFrame()
    
    items = pd.DataFrame()
    for item_id in s.ITEM_ID.unique() :
        database = RDB_BI_REV
        db_rows = get_item(database, item_id)
        items = items.append(db_rows)

    tmp = s.merge(items[['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'GOODS_NAME', 'ORG_GOODS_NUM', 'ITEM_ID']], how='left', on='ITEM_ID')
    tmp = tmp[['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'GOODS_NAME', 'ORG_GOODS_NUM', 'ITEM_ID', 'COLLECT_DAY', 'SELL_AMOUNT']]
    tmp.to_excel(f'tmp.xlsx')
    report = pd.pivot_table(tmp, index=['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'GOODS_NAME', 'ORG_GOODS_NUM', 'ITEM_ID'],values=["SELL_AMOUNT"], columns=["COLLECT_DAY"],aggfunc=[np.sum])
    return report
# -------------------------------

def main():
    database = RDB_BI_REV # 30분 간격 수집 database
    items = get_recent_items(database, DATE_ITEM_UPT_DT)
    print(items.groupby('COLLECT_SITE').ITEM_ID.count().reset_index(name='NUM_ITEMS').to_string())
    
    # filter items by brand, item_id, ... for test
    items = items[(items.COLLECT_SITE == 'store.musinsa.com') & items.BRAND_NAME.isin(['JILLSTUART SPORT NEWYORK', 'IL CORSO','TNGT','BLUE LOUNGE MAESTRO'])]
    #items = items[(items.COLLECT_SITE == 'store.musinsa.com') & items.BRAND_NAME.isin(['BLUE LOUNGE MAESTRO'])]
    #items = items[(items.COLLECT_SITE == 'store.musinsa.com') & items.BRAND_NAME.isin(['TNGT', 'BLUE LOUNGE MAESTRO'])]
    
    print(items.groupby(['BRAND_NAME', 'TRUST_SELLER']).ITEM_ID.count().reset_index(name='NUM_STYLES'))
    
    collect_day_list = [d.strftime('%Y-%m-%d') for d in pd.date_range('2019-05-03', '2019-05-13')]

    # collect_site x trust_seller x brand_name x collect_day 단위로 diff matrix (m)을 만듦
    split_list = []
    for (collect_site, trust_seller, brand_name), items_seller_brand in items.groupby(['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME']):
        for collect_day in collect_day_list:
            print(trust_seller, brand_name, collect_day)
            split_list.append((database, collect_site, trust_seller, brand_name, collect_day, items_seller_brand))
    
    split_list = split_list[:]
    num_process = min(len(split_list), 30)
    m_list = []
    with Pool(num_process) as pool:
        #pool.map(set_threshold_task, split_list[:])
        for m in tqdm.tqdm(pool.imap_unordered(get_m, split_list), total=len(split_list)):
            m_list.append(m)

    
    # collect_site x trust_seller x brand_name x collect_day 단위로 만들어진 diff matrix (m)를 scm tool 동작을 감지하기 위하여 pass1, pass2를 통과시킴
    split_list = m_list[:]
    num_process = min(len(split_list), 30)
    p_list = []
    with Pool(num_process) as pool:
        #pool.map(set_threshold_task, split_list[:])
        for p in tqdm.tqdm(pool.imap_unordered(pre_process_m, split_list), total=len(split_list)):
            p_list.append(p)
            
            
    # 실제로는 여기가 끝 !
            
    # 여기서부터는  LF에서 제공한 정답과 비교하기 위한 코드들임
    # collect_site x trust_seller x brand_name x collect_day 단위로 sell amount를 계산함 (단순 aggregation sum)
    split_list = p_list[:]
    num_process = min(len(split_list), 30)            
    sell_amt = pd.DataFrame()
    with Pool(num_process) as pool:
        #pool.map(set_threshold_task, split_list[:])
        for s in tqdm.tqdm(pool.imap_unordered(get_sell_amt, split_list), total=len(split_list)):
            sell_amt = sell_amt.append(s)
    
    # 계산된 sell amt 
    file_dump(sell_amt, 'sell_amt')
    # 계산된 sell amt를 pivot : rows = items , columns = collect_day
    file_dump(make_report(sell_amt), 'report')

if __name__ == '__main__':            
    DT = datetime.now()
    r = main()
    print(datetime.now()-DT)