import os
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import pymysql
import inspect
import hashlib
pymysql.install_as_MySQLdb()
from multiprocessing import Pool
import tqdm
import glob
import numpy as np
# -------------------------------------------    
RDB_BI_GFK_11ST = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_11st?charset=utf8mb4' 
RDB_BI_GFK_COUPANG = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_coupang?charset=utf8mb4' 
RDB_BI_GFK_TMON = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_tmon?charset=utf8mb4' 
RDB_BI_GFK_WEMAKEPRICE = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_wemakeprice?charset=utf8mb4' 
RDB_BI_GFK_HMALL = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_hmall?charset=utf8mb4' 
RDB_BI_FASHION1 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider?charset=utf8mb4'
RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4' 
RDB_BI_AP = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_ap?charset=utf8mb4' 
RDB_BI_OY = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_oliveyoung?charset=utf8mb4' 
RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4' 
RDB_BI_SIVILLAGE = 'mysql://wspider:wspider00!q@13.124.149.212:3306/wspider?charset=utf8mb4' 
#RDB_BI_AP_M = 'mysql://wspidermr:wspidermr00!q@133.186.134.175:3306/ap-bigdata-v2?charset=utf8mb4' 
# -------------------------------------------    

class ReadRDB:
	def __init__(self, save_path, db):
		self.save_path = save_path
		self.engine = create_engine(db, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})

	def saveData(self, table, prefix):
	    print('READ DATA FROM : ', self.engine)
	    print(f'>> save {table} as {prefix} ~~ ')

	    files =  glob.glob(rev_path + f'raw/{table}/*')
	    max_id_list = [int(f.split('.parquet.gzip')[0].split('_')[-1]) for f in files]
	    max_id = max(max_id_list)

	    new_min_id = max_id + 1
	    new_max_id = pd.read_sql(f"SELECT ID FROM {table} order by id desc limit 1", self.engine)['ID'].values[0]
	    new_id_list = list(range(new_min_id,new_max_id))
	    print(f'query id between {min(new_id_list)} and {max(new_id_list)}')
	    
	    # should query at most 100000 records. 
	    chunk_size = 100000
	    chunk_count = round(len(new_id_list) / chunk_size) + 1
	    splited_idlist = list(np.array_split(new_id_list, chunk_count))
	    minmax_splited_idlist = [[i.min(), i.max(), len(i)] for i in splited_idlist]
	    

	    
	    for idx, i in enumerate(minmax_splited_idlist):
	        s = datetime.now()
	        print(idx, '/', len(minmax_splited_idlist), i[0], i[1])
	        df = pd.read_sql(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}", self.engine)

	        if df.empty: 
	            pass
	        else: 
	            df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
	            df['COLLECT_DAY_PART'] = df['COLLECT_DAY']

	            min_day = df['COLLECT_DAY'][0]
	            max_day = list(df['COLLECT_DAY'])[-1]
	            print(df.shape)
	            df.to_parquet(self.save_path + f'raw/{table}/{prefix}_{min_day}_{max_day}_{i[0]}_{i[1]}.parquet.gzip', compression='gzip', partition_cols = ['ITEM_ID_PART', 'COLLECT_DAY_PART'])
	            print(datetime.now()-s)
	        print("---------------")

if __name__ == '__main__':
	rev_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_REV/'
	readREV = ReadRDB(rev_path, RDB_BI_REV)
	readREV.saveData('MWS_COLT_ITEM_IVT', 'ivt')
	readREV.saveData('MWS_COLT_ITEM_PRICE_HIS', 'phis')
	readREV.saveData('MWS_COLT_ITEM_DISCOUNT', 'dc')