import pymongo
import pandas as pd
import glob
from datetime import datetime, timedelta
import sys
import fastparquet
import gc
import pprint



# --

import os
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import pymysql
import inspect
import hashlib
pymysql.install_as_MySQLdb()
from multiprocessing import Pool
import tqdm


# Related folder structure ...
# under '/home/ws-hadoop/DataTeam/sellamt/'
# data
# |- WSPIDER_11ST
#   |- raw
#       |- MWS_COLT_ITEM
#       |- MWS_COLT_ITEM_IVT
#       |- MWS_COLT_ITEM_PRICE_HIS
#       |- MWS_COLT_ITEM_DISCOUNT
# src
# |- data
#       |- dataSaver.py
#       |- saveDataMain.py

class dataSaverRDB():
    def __init__(self, path, table, database):
        self.path = path
        self.table = table
        self.database = database
        file_name_prefix = {'MWS_COLT_ITEM': 'item_',
                           'MWS_COLT_ITEM_IVT': 'ivt_',
                           'MWS_COLT_ITEM_PRICE_HIS': 'phis_',
                           'MWS_COLT_ITEM_DISCOUNT': 'dc_'}
        self.prefix = file_name_prefix[self.table]
        print(f"------------------------- GET data from {self.table}")
        self.start_point = datetime.now()
        self.connectRDB()
        self.setOutputPath(path)

    def connectRDB(self):
        try:
            self.engine = create_engine(self.database, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
            print(self.engine)
        except sqlalchemy.exc.OperationalError:
            print('Connection Error occured')
    def setOutputPath(self, path):
        self.outputpath = path + '/' + self.table + '/'
        print("2. Set Output Path")
        print(">> Parquet file will be saved under [[", self.outputpath, "]]")

    def makeCollectDaysList(self, sellamt_cal_day, collect_day_window):
        # make a list of days need for the threshold setting process.
        collect_day_start = datetime.strptime(sellamt_cal_day, '%Y-%m-%d') - timedelta(days=collect_day_window)
        collect_day_list = pd.date_range(collect_day_start, periods=collect_day_window + 1).tolist()
        collect_day_list = [datetime.strftime(i, '%Y-%m-%d') for i in collect_day_list]
        collect_day_list.sort()
        print("3. Make COLLECT_DAY List")
        print(f">> Need {len(collect_day_list)} days data: {str(min(collect_day_list))} ~ {str(max(collect_day_list))}")
        self.collect_day_list = collect_day_list
        return self.collect_day_list

    def checkFiles(self, sellamt_cal_day, collect_day_window):
        # check if already saved the data as a parquet file.
        # 1) make a list of collect day you need. 
        self.collect_day_list = self.makeCollectDaysList(sellamt_cal_day, collect_day_window)
        self.today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        # 2) check files under the output path
        print("4. Check Already Saved Files under ", self.outputpath)
        saved_files = glob.glob(self.outputpath + '*')
        # print(saved_files, len(saved_files))
        if len(saved_files) is 0:
            saved_files = [f'dummy_file_name_{self.prefix}_to detour index out of range error ']
            print("No saved files : ", saved_files)


        # 3) get output(uptdt_range to query & collect_day to query)
        if self.table == 'MWS_COLT_ITEM':
            # ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s
            saved_uptdt_range = [i.split(f'{self.prefix}')[1].split(".")[0] for i in saved_files]
            self.uptdt_from = min(self.collect_day_list)
            self.uptdt_range_toquery  = self.uptdt_from + '_' + self.today
            print('saved_uptdt_range', saved_uptdt_range)
            self.uptdt_range_toquery = list(set([self.uptdt_range_toquery]) - set(saved_uptdt_range))
            print('uptdt_range_toquery', self.uptdt_range_toquery)
            return self.uptdt_range_toquery

        elif (self.table == 'MWS_COLT_ITEM_IVT') | (self.table == 'MWS_COLT_ITEM_DISCOUNT'):
            # IVT table's COLLECT_DAY format : %Y%m%d
            # DISCOUNT table's COLLECT_DAY format : %Y%m%d
            self.collect_day_list = [''.join(i.split('-')) for i in self.collect_day_list]
            saved_days = [i.split(f'{self.prefix}')[1].split('.')[0] for i in saved_files]
            collect_days_toquery = list(set(self.collect_day_list) - set(saved_days))
            return collect_days_toquery
        
        elif self.table == 'MWS_COLT_ITEM_PRICE_HIS':
            # PRICE_HIS table's COLLECT_DAY fromat : %Y-%m-%d
            saved_days = [i.split(f'{self.prefix}')[1].split('.')[0] for i in saved_files]
            print(saved_days)
            collect_days_toquery = list(set(self.collect_day_list) - set(saved_days))
            return collect_days_toquery
        
        else:
            return self.collect_day_list

    # def setColumnFilter(self, columns):
    #     self.columns = columns
    #     columns_filter = {c: True for c in self.columns}
    #     columns_filter.update({'_id': False})

    #     return columns_filter

    def query(self, query):
        print("5. Query MySQL DB")
        df = pd.read_sql_query(query, self.engine)
        self.columns = list(df.columns)
        print(">> Data Shape :", df.shape)
        return df

    def setPartitions(self, partition_cols, part_n):
        print("6. Set partitions.", partition_cols)
        self.partition_cols = partition_cols
        self.pcols = [i + '_PART' for i in self.partition_cols]
        self.columns = self.columns + self.pcols
        self.part_n = part_n

    def saveParquet(self, df):
        print("df is empty:", df.empty)
        if df.empty:
            return print('No data to save. Maybe table error or transfer error occured. ')
        else:
            # 1) make partition column
            col1 = self.partition_cols[0]
            pcol1 = self.pcols[0]

            try:
                df[pcol1] = df[col1].astype('int') % self.part_n
            except ValueError:
                df[pcol1] = df[col1].apply(lambda id: int(''.join([i for i in id if i.isdigit()])))
                df[pcol1] = df[pcol1] % self.part_n

            # df[pcol1] = df[col1].astype('int') % self.part_n

            if len(self.partition_cols) > 1:
                col2 = self.partition_cols[1]
                pcol2 = self.pcols[1]
                try:
                    df[pcol2] = df[col2].astype('int') % self.part_n
                except ValueError:
                    df[pcol2] = df[col2].apply(lambda id: int(''.join([i for i in id if i.isdigit()])))
                    df[pcol2] = df[pcol2] % self.part_n

            # 2) set file names
            if self.table == 'MWS_COLT_ITEM':
                file_name = self.outputpath + f'{self.prefix}{min(self.collect_day_list)}_{self.today}.parquet.gzip'
            elif self.table in ['MWS_COLT_ITEM_IVT', 'MWS_COLT_ITEM_PRICE_HIS', 'MWS_COLT_ITEM_DISCOUNT']:
                cday = df['COLLECT_DAY'].unique()[0]
                file_name = self.outputpath + f'{self.prefix}{cday}.parquet.gzip'
            else:
                print(f"Table is not in the list!!")
                return

            # 0) transform column order
            print('df.columns: ', df.columns)
            print('self.columns :', self.columns)
            print(df.dtypes)
            df = df[self.columns]

            print("7. Save Data: ", file_name)
            index_col = 'ID'
            print(self.columns)
            df.to_parquet(fname=file_name, index=index_col, partition_cols=self.pcols, engine='pyarrow',
                          compression='gzip')
            print("TIME SPENT: ", str(datetime.now() - self.start_point))
    def __del__(self):
        self.engine.dispose()
        print("---------------------------------------------------------------------------------------- ")
