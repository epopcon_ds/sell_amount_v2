import os
import gc
from multiprocessing import Pool
import tqdm
import glob
import sys
path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)

from sqlalchemy import create_engine
import pymysql
pymysql.install_as_MySQLdb()

import pandas as pd
import numpy as np
from datetime import datetime, timedelta

from src.data.dataSaver import dataSaver
from src.data.dataSaverRDB import dataSaverRDB

# -------------------------------------------    
RDB_BI_GFK_11ST = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_11st?charset=utf8mb4' 
RDB_BI_GFK_COUPANG = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_coupang?charset=utf8mb4' 
RDB_BI_GFK_TMON = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_tmon?charset=utf8mb4' 
RDB_BI_GFK_WEMAKEPRICE = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_wemakeprice?charset=utf8mb4' 
RDB_BI_GFK_HMALL = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_hmall?charset=utf8mb4' 
RDB_BI_FASHION1 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider?charset=utf8mb4'
RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4' 
RDB_BI_AP = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_ap?charset=utf8mb4' 
RDB_BI_OY = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_oliveyoung?charset=utf8mb4' 
RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4' 
RDB_BI_SIVILLAGE = 'mysql://wspider:wspider00!q@13.124.149.212:3306/wspider?charset=utf8mb4' 
#RDB_BI_AP_M = 'mysql://wspidermr:wspidermr00!q@133.186.134.175:3306/ap-bigdata-v2?charset=utf8mb4' 
# -------------------------------------------    


# ---------------------------------------------------------------------------------------
class ReadMongo:
	def __init__(self, save_path, db, sellamt_cal_day, collect_day_window):
		self.base_path = save_path
		self.database = db
		self.sellamt_cal_day = sellamt_cal_day
		self.collect_day_window = collect_day_window

	def saveData(self, collection):
		if collection == 'MWS_COLT_ITEM_IVT':
		    columns = ['ID', 'ITEM_ID', 'STOCK_ID', 'COLOR_OPTION', 'SIZE_OPTION', 'STYLE_OPTION',
		               'GIFT_OPTION', 'OPTION', 'STOCK_AMOUNT', 'ADD_PRICE', 'COLLECT_DAY', 'REG_ID', 'REG_DT']
		elif collection == 'MWS_COLT_ITEM_PRICE_HIS':
			columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE', 'REG_DT']
		elif collection == 'MWS_COLT_ITEM_DISCOUNT':
			columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'OPTION', 'DISCOUNT_PRICE','DISCOUNT_RATE', 'REG_DT']
		else : 
			print("The collection name is not in the list. Please check typo.")
			return

		# ---- make Saver instance
		histSaver = dataSaver(database = self.database, collection= collection, path = self.base_path)
		# ---- query DB  & save files
		collect_days_toquery = histSaver.checkFiles(self.sellamt_cal_day, self.collect_day_window)
		if collect_days_toquery:
			print(f">> {len(collect_days_toquery)} days data should be called from DB")
			print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
			for cday in collect_days_toquery:
				rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
				columns_filter = histSaver.setColumnFilter(columns)
				daily_df = histSaver.queryMongo(query = rows_filter, projections=columns_filter)
				histSaver.setPartitions(partition_cols= ['ITEM_ID'], part_n = 19)
				histSaver.saveParquet(daily_df)
				del daily_df
				gc.collect()
		else:
			print(f">> No new {collection} data to query.")
		del histSaver


class ReadRDB:
	def __init__(self, save_path, db):
		self.save_path = save_path
		self.engine = create_engine(db, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})

	def saveData(self, table, prefix):
	    print('READ DATA FROM : ', self.engine)
	    print(f'>> save {table} as {prefix}_minday_maxday_minid_maxid.parquet')

	    files =  glob.glob(rev_path + f'raw/{table}/*')
	    max_id_list = [int(f.split('.parquet.gzip')[0].split('_')[-1]) for f in files]
	    max_id = max(max_id_list)

	    new_min_id = max_id + 1
	    new_max_id = pd.read_sql(f"SELECT ID FROM {table} order by id desc limit 1", self.engine)['ID'].values[0]
	    new_id_list = list(range(new_min_id,new_max_id))
	    print(f'query id between {min(new_id_list)} and {max(new_id_list)}')
	    
	    # should query at most 100000 records. 
	    chunk_size = 100000
	    chunk_count = round(len(new_id_list) / chunk_size) + 1
	    splited_idlist = list(np.array_split(new_id_list, chunk_count))
	    minmax_splited_idlist = [[i.min(), i.max(), len(i)] for i in splited_idlist]
	    

	    
	    for idx, i in enumerate(minmax_splited_idlist):
	        s = datetime.now()
	        print(idx, '/', len(minmax_splited_idlist), i[0], i[1])
	        df = pd.read_sql(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}", self.engine)

	        if df.empty: 
	            pass
	        else: 
	            df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
	            df['COLLECT_DAY_PART'] = df['COLLECT_DAY']

	            min_day = df['COLLECT_DAY'][0]
	            max_day = list(df['COLLECT_DAY'])[-1]
	            print(df.shape)
	            df.to_parquet(self.save_path + f'raw/{table}/{prefix}_{min_day}_{max_day}_{i[0]}_{i[1]}.parquet.gzip', compression='gzip', partition_cols = ['ITEM_ID_PART', 'COLLECT_DAY_PART'])
	            print(datetime.now()-s)
	        print("---------------")



# ---------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    collect_day_window = 15
    
    # ----------------------- LBRAND
    database = 'wspider_lbrand'
    base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND/raw'
    lbrand_readMongo = ReadMongo(save_path = base_path, db = database, sellamt_cal_day = sellamt_cal_day, collect_day_window = collect_day_window)
    print("Start saveDataMain Module")
    lbrand_readMongo.saveData('MWS_COLT_ITEM_IVT')
    lbrand_readMongo.saveData('MWS_COLT_ITEM_PRICE_HIS')
    lbrand_readMongo.saveData('MWS_COLT_ITEM_DISCOUNT')     

    # ----------------------- LBRAND : REV
	rev_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_REV/'
	rev_database = RDB_BI_REV
	readREV = ReadRDB(rev_path, rev_database)
	readREV.saveData('MWS_COLT_ITEM_IVT', 'ivt')
	readREV.saveData('MWS_COLT_ITEM_PRICE_HIS', 'phis')
	readREV.saveData('MWS_COLT_ITEM_DISCOUNT', 'dc')

    # ----------------------- GFK 11ST
    # database = 'wspider_11st'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_11ST/raw'
    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)
    
    # # ---------------------- GFK_COUPANG
    # database = 'wspider_coupang'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_COUPANG/raw'
    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)
    
    # # ----------------------- AP
    # database = 'wspider_ap'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_AP/raw'
    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)





