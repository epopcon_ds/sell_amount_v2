from datetime import datetime, timedelta
import gc
import sys
path = '/home/ws-hadoop/DataTeam/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.data.dataSaver import dataSaver
from src.data.dataSaverRDB import dataSaverRDB



# ---------------------------------------------------------------------------------------
def saveDataMain(database, base_path, sellamt_cal_day, collect_day_window):
    # # ##################
    # # #  mws_colt_item #
    # # ##################
    # # ---- settings
    # collection = 'MWS_COLT_ITEM'
    # columns = ['ID', 'ITEM_NUM', 'COLLECT_SITE', 'GOODS_CATE', 'BRAND_NAME', 'TRUST_SELLER', 'PRICE', 'SITE_PRICE',
    #            'REG_DT', 'UPT_DT']
    # # columns = ['ID', 'GOODS_NUM', 'ORG_GOODS_NUM', 'ITEM_NUM', 'GOODS_NAME', 'GOODS_CATE', 'BRAND_NAME', 'RELEASE_DT',
    # #            'COLLECT_DAY', 'SELL_CLOSE_DT', 'ORG_MAFT_DT', 'MAFT_DT', 'MAFT_ORIGIN', 'COLLECT_SITE', 'PRICE_STD_CODE',
    # #            'PRICE' , 'SITE_PRICE', 'COLOR_OPTION', 'SIZE_OPTION', 'STYLE_OPTION', 'GIFT_OPTION', 'OPTION', 'COLLECT_URL', 'BEST_ITEM_YN']
    # # ---- make itemSaver
    # itemSaver = dataSaver(database=database, collection=collection, path=base_path)
    # # ---------- query DB  & save files
    # uptdt_range_toquery = itemSaver.checkFiles(sellamt_cal_day, collect_day_window)

    # if uptdt_range_toquery:
    #     uptdt_from = [i.split('_')[0] for i in uptdt_range_toquery][0]
    #     uptdt_from = datetime.strptime(uptdt_from, '%Y-%m-%d')
    #     rows_filter = {'UPT_DT': {'$gte': uptdt_from}}
    #     columns_filter = itemSaver.setColumnFilter(columns)
    #     # ----------
    #     # item_df = itemSaver.queryMongo(query = rows_filter, projections= columns_filter, nlimit = 10)
    #     item_df = itemSaver.queryMongo(query = rows_filter, projections= columns_filter)
    #     itemSaver.setPartitions(partition_cols = ['ID'], part_n = 19)
    #     itemSaver.saveParquet(item_df)
    #     del item_df
    #     gc.collect()
    # else:
    #     print(f">> No new {collection} data to query.")
    # del itemSaver


    ######################
    #  mws_colt_item_ivt #
    ######################
    # ---- settings
    collection = 'MWS_COLT_ITEM_IVT'
    columns = ['ID', 'ITEM_ID', 'STOCK_ID', 'COLOR_OPTION', 'SIZE_OPTION', 'STYLE_OPTION',
               'GIFT_OPTION', 'OPTION', 'STOCK_AMOUNT', 'ADD_PRICE', 'COLLECT_DAY', 'REG_ID', 'REG_DT']

    # columns = ['ID', 'ITEM_ID', 'STOCK_ID', 'STOCK_AMOUNT', 'ADD_PRICE', 'COLLECT_DAY', 'REG_ID', 'REG_DT']
    # ---- make ivtSaver
    ivtSaver = dataSaver(database = database, collection= collection, path = base_path)
    # ---- query DB  & save files
    collect_days_toquery = ivtSaver.checkFiles(sellamt_cal_day, collect_day_window)
    if collect_days_toquery:
        print(f">> {len(collect_days_toquery)} days data should be called from DB")
        print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
        for cday in collect_days_toquery:
            rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
            columns_filter = ivtSaver.setColumnFilter(columns)
            ivt_df = ivtSaver.queryMongo(query = rows_filter, projections=columns_filter)
            ivtSaver.setPartitions(partition_cols= ['ITEM_ID', 'STOCK_ID'], part_n = 19)
            ivtSaver.saveParquet(ivt_df)
            del ivt_df
            gc.collect()
    else:
        print(f">> No new {collection} data to query.")
    del ivtSaver

    ############################
    #  mws_colt_item_price_his #
    ############################
    # ---- settings
    collection = 'MWS_COLT_ITEM_PRICE_HIS'
    columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE', 'REG_DT']
    # ---- make phisSaver
    phisSaver = dataSaver(database=database, collection=collection, path = base_path)
    # ---- query DB & save files
    collect_days_toquery = phisSaver.checkFiles(sellamt_cal_day, collect_day_window)
    if collect_days_toquery:
        print(f">> {len(collect_days_toquery)} days data should be called from DB")
        print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
        for cday in collect_days_toquery:
            rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
            columns_filter = phisSaver.setColumnFilter(columns)
            phis_df = phisSaver.queryMongo(query = rows_filter, projections=columns_filter)
            phisSaver.setPartitions(partition_cols = ['ITEM_ID'], part_n = 19 )
            phisSaver.saveParquet(phis_df)
            del phis_df
            gc.collect()
    else:
        print(f">> No new {collection} data to query.")
    del phisSaver


    ############################
    #  mws_colt_item_discount #
    ############################
    # ---- settings
    collection = 'MWS_COLT_ITEM_DISCOUNT'
    columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'OPTION', 'DISCOUNT_PRICE','DISCOUNT_RATE', 'REG_DT']
    # ---- make dcSaver
    dcSaver = dataSaver(database=database, collection=collection, path = base_path)
    # ---- query DB & save files
    collect_days_toquery = dcSaver.checkFiles(sellamt_cal_day, collect_day_window)
    if collect_days_toquery:
        print(f">> {len(collect_days_toquery)} days data should be called from DB")
        print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
        for cday in collect_days_toquery:
            rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
            columns_filter = dcSaver.setColumnFilter(columns)
            dc_df = dcSaver.queryMongo(query = rows_filter, projections=columns_filter)
            dcSaver.setPartitions(partition_cols = ['ITEM_ID'], part_n = 19 )
            dcSaver.saveParquet(dc_df)
            del dc_df
            gc.collect()
    else:
        print(f">> No new {collection} data to query.")
    del dcSaver


    # #####################
    # #  Parameter Tables #
    # #####################
    # # ---- settings
    # collection = 'SELL_AMT_SCM_FILTER'
    # columns = ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'BRAND_PRIORITY', 'SCM_PRIORITY', 'REG_DT']
    # paramSaver(database=database, collection=collection, path = base_path)

# ---------------------------------------------------------------------------------------------------------------
# def saveDataMainRDB(database, base_path, sellamt_cal_day, collect_day_window):
#     # ##################
#     # #  mws_colt_item #
#     # ##################
#     # ---- settings
#     table = 'MWS_COLT_ITEM'
#     # ---- make itemSaver
#     itemSaver = dataSaverRDB(database=database, table = table, path=base_path)
#     # ---------- query DB  & save files
#     uptdt_range_toquery = itemSaver.checkFiles(sellamt_cal_day, collect_day_window)

#     if uptdt_range_toquery:
#         uptdt_from = [i.split('_')[0] for i in uptdt_range_toquery][0]
#         uptdt_from = datetime.strptime(uptdt_from, '%Y-%m-%d')
#         print(uptdt_from)
#         query = f"SELECT ID, COLLECT_SITE, ITEM_NUM, COLLECT_URL, ORG_GOODS_NUM, GOODS_NAME, TRUST_SELLER, BRAND_NAME, PRICE, SITE_PRICE, UPT_DT FROM MWS_COLT_ITEM where UPT_DT >= '{uptdt_from}';"
#         print(query)
#         # ----------
#         item_df = itemSaver.query(query)
#         print(item_df.head())
#         itemSaver.setPartitions(partition_cols = ['ID'], part_n = 19)
#         itemSaver.saveParquet(item_df)
#         del item_df
#         gc.collect()
#     else:
#         print(f">> No new {table} data to query.")
#     del itemSaver


#     ######################
#     #  mws_colt_item_ivt #
#     ######################
#     # ---- settings
#     table = 'MWS_COLT_ITEM_IVT'
#     # ---- make ivtSaver
#     ivtSaver = dataSaverRDB(database = database, table = table, path = base_path)
#     # ---- query DB  & save files
#     collect_days_toquery = ivtSaver.checkFiles(sellamt_cal_day, collect_day_window)
#     if collect_days_toquery:
#         print(f">> {len(collect_days_toquery)} days data should be called from DB")
#         print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
#         for cday in collect_days_toquery:
#             query = f"SELECT * FROM MWS_COLT_ITEM_IVT where ID >= 19192400 and COLLECT_DAY = {int(cday)};"
#             # query = f"SELECT * FROM MWS_COLT_ITEM_IVT limit 10;"
#             print(query)
#             ivt_df = ivtSaver.query(query)
#             ivtSaver.setPartitions(partition_cols= ['ITEM_ID', 'STOCK_ID'], part_n = 19)
#             ivtSaver.saveParquet(ivt_df)
#             del ivt_df
#             gc.collect()
#     else:
#         print(f">> No new {table} data to query.")
#     del ivtSaver

#     ############################
#     #  mws_colt_item_price_his #
#     ############################
#     # ---- settings
#     collection = 'MWS_COLT_ITEM_PRICE_HIS'
#     columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE', 'REG_DT']
#     # ---- make phisSaver
#     phisSaver = dataSaver(database=database, collection=collection, path = base_path)
#     # ---- query DB & save files
#     collect_days_toquery = phisSaver.checkFiles(sellamt_cal_day, collect_day_window)
#     if collect_days_toquery:
#         print(f">> {len(collect_days_toquery)} days data should be called from DB")
#         print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
#         for cday in collect_days_toquery:
#             rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
#             columns_filter = phisSaver.setColumnFilter(columns)
#             phis_df = phisSaver.queryMongo(query = rows_filter, projections=columns_filter)
#             phisSaver.setPartitions(partition_cols = ['ITEM_ID'], part_n = 19 )
#             phisSaver.saveParquet(phis_df)
#             del phis_df
#             gc.collect()
#     else:
#         print(f">> No new {collection} data to query.")
#     del phisSaver


#     ############################
#     #  mws_colt_item_discount #
#     ############################
#     # ---- settings
#     collection = 'MWS_COLT_ITEM_DISCOUNT'
#     columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'OPTION', 'DISCOUNT_PRICE','DISCOUNT_RATE', 'REG_DT']
#     # ---- make dcSaver
#     dcSaver = dataSaver(database=database, collection=collection, path = base_path)
#     # ---- query DB & save files
#     collect_days_toquery = dcSaver.checkFiles(sellamt_cal_day, collect_day_window)
#     if collect_days_toquery:
#         print(f">> {len(collect_days_toquery)} days data should be called from DB")
#         print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
#         for cday in collect_days_toquery:
#             rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
#             columns_filter = dcSaver.setColumnFilter(columns)
#             dc_df = dcSaver.queryMongo(query = rows_filter, projections=columns_filter)
#             dcSaver.setPartitions(partition_cols = ['ITEM_ID'], part_n = 19 )
#             dcSaver.saveParquet(dc_df)
#             del dc_df
#             gc.collect()
#     else:
#         print(f">> No new {collection} data to query.")
#     del dcSaver


# ---------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    # # # ----------------------- GFK 11ST
    # database = 'wspider_11st'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_11ST/raw'
    # sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    # collect_day_window = 15
    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)
    
    # ---------------------- GFK_COUPANG
    database = 'wspider_coupang'
    base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_COUPANG/raw'
    sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    collect_day_window = 15
    print("Start saveDataMain Module")
    saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)
    
    #
    # # ----------------------- AP
    # database = 'wspider_ap'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_AP/raw'
    # sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    # collect_day_window = 15
    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)


    # # ----------------------- LBRAND
    # database = 'wspider_lbrand'
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_LBRAND/raw'
    # sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    # collect_day_window = 15

    # print("Start saveDataMain Module")
    # saveDataMain(database, base_path, sellamt_cal_day, collect_day_window)

    # print("Start saveDataMainRDB Module")
    # database = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4'
    # saveDataMainRDB(database,base_path, sellamt_cal_day, collect_day_window)
    # #
    # # ----------------------- LBRAND_REV [FROM RDB]
    # RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4'
    # database = RDB_BI_REV
    # base_path = '/home/ws-hadoop/DataTeam/sellamt/data/WSPIDER_REV/raw'
    # sellamt_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    # collect_day_window = 15
    # print("Start saveDataMainRDB Module")
    # saveDataMainRDB(database,base_path, sellamt_cal_day, collect_day_window)
