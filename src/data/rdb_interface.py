'''
    database interface 
    version 0 : tkcho.2019.05.24  
'''

import os
import pandas as pd
from datetime import datetime, timedelta
from sqlalchemy import create_engine
import pymysql
import inspect
import hashlib
pymysql.install_as_MySQLdb()
from multiprocessing import Pool
import tqdm

# -------------------------------------------    
RDB_BI_GFK_11ST = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_11st?charset=utf8mb4' 
RDB_BI_GFK_COUPANG = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_coupang?charset=utf8mb4' 
RDB_BI_GFK_TMON = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_tmon?charset=utf8mb4' 
RDB_BI_GFK_WEMAKEPRICE = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_wemakeprice?charset=utf8mb4' 
RDB_BI_GFK_HMALL = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_hmall?charset=utf8mb4' 
RDB_BI_FASHION1 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider?charset=utf8mb4'
RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4' 
RDB_BI_AP = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_ap?charset=utf8mb4' 
RDB_BI_OY = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_oliveyoung?charset=utf8mb4' 
RDB_BI_REV = 'mysql://wspider:wspider00!q@13.125.242.38:3306/wspider_rev?charset=utf8mb4' 
RDB_BI_SIVILLAGE = 'mysql://wspider:wspider00!q@13.124.149.212:3306/wspider?charset=utf8mb4' 
#RDB_BI_AP_M = 'mysql://wspidermr:wspidermr00!q@133.186.134.175:3306/ap-bigdata-v2?charset=utf8mb4' 
# -------------------------------------------    

CACHE_DIR = './db_cache'

# iterable -> tuple for SQL IN query
def tp(item_id_list):
    item_id_list = list(item_id_list)
    return tuple(item_id_list) if len(item_id_list) > 1 else tuple(item_id_list*2)

# db cache flush
def expire_cache(hours=24):
    os.makedirs(CACHE_DIR, exist_ok=True) # make cache dir if not exists
    
    def del_expired_cache(f):
        if (datetime.now()-datetime.fromtimestamp(os.stat(f).st_mtime) > timedelta(hours=hours)):
            print('expire_cache', f)
            os.remove(f)

    [del_expired_cache(os.path.join(CACHE_DIR, f)) for f in os.listdir(CACHE_DIR) if os.path.isfile(os.path.join(CACHE_DIR, f))]
    
def get_item(database, item_id):
    func_name = inspect.stack()[0][3]
    file_path = f'{CACHE_DIR}/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{item_id}.pickle'
    
    if os.path.exists(file_path) and (datetime.now()-datetime.fromtimestamp(os.stat(file_path).st_mtime) <= timedelta(hours=1)):
        #print(f'{func_name} cache hit', file_path)
        db_rows = pd.read_pickle(file_path)        
    else:    
        engine = create_engine(database, encoding = 'utf8' , pool_size=50,pool_recycle=3600,connect_args={'connect_timeout':1000000} )    
        query = f'SELECT ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, COLLECT_URL, ORG_GOODS_NUM, GOODS_NAME, TRUST_SELLER, BRAND_NAME FROM MWS_COLT_ITEM where ID = {item_id};'
        db_rows = pd.read_sql_query(query, engine)
        engine.dispose()
        db_rows.to_pickle(file_path)

    return db_rows

def get_ivt(database, item_id, collect_day=None): 
    func_name = inspect.stack()[0][3]
    
    if collect_day < datetime.now().strftime('%Y%m%d'):
        permanent = True
        os.makedirs(CACHE_DIR, exist_ok=True) # make cache dir if not exists
        os.makedirs(CACHE_DIR+'/permanent/', exist_ok=True) # make cache dir if not exists
    else:
        permanent = False
    
    if collect_day:
        if permanent:
            file_path = f'{CACHE_DIR}/permanent/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{item_id}_{collect_day}.pickle'
        else :   
            file_path = f'{CACHE_DIR}/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{item_id}_{collect_day}.pickle'
    else:
        file_path = f'{CACHE_DIR}/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{item_id}_{hashlib.md5(DATE_IVT_REG_DT.encode()).hexdigest()}.pickle'
        
    if os.path.exists(file_path):
        #print(f'{func_name} cache hit', file_path)
        db_rows = pd.read_pickle(file_path)        
    else:    
        print(f'{func_name} database accessing ... ', file_path)
        engine = create_engine(database, encoding = 'utf8' , pool_size=5,pool_recycle=3600,connect_args={'connect_timeout':1000000} )    
        if collect_day:
            query = f'SELECT * FROM MWS_COLT_ITEM_IVT where ITEM_ID = {item_id} and COLLECT_DAY = "{collect_day}";'
        else:
            query = f'SELECT * FROM MWS_COLT_ITEM_IVT where ITEM_ID = {item_id} and REG_DT >= "{DATE_IVT_REG_DT}";'
        db_rows = pd.read_sql_query(query, engine)
        engine.dispose()
        db_rows.to_pickle(file_path)

    return db_rows

def get_recent_items(database, upt_dt):
    func_name = inspect.stack()[0][3]
    file_path = f'{CACHE_DIR}/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{hashlib.md5(upt_dt.encode()).hexdigest()}.pickle'
    
    if os.path.exists(file_path) and (datetime.now()-datetime.fromtimestamp(os.stat(file_path).st_mtime) <= timedelta(hours=1)):
        print(f'{func_name} cache hit', file_path)
        db_rows = pd.read_pickle(file_path)        
    else:    
        query = f'select ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, COLLECT_URL, TRUST_SELLER, BRAND_NAME, ORG_GOODS_NUM from MWS_COLT_ITEM where UPT_DT > "{upt_dt}";'
        engine = create_engine(database, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
        db_rows = pd.read_sql_query(query, engine) 
        engine.dispose()
        db_rows.to_pickle(file_path)

    return db_rows

'''
def get_num_opt_item(task_param):
    database, item_id = task_param
    
    func_name = inspect.stack()[0][3]
    file_path = f'{CACHE_DIR}/{func_name}_{hashlib.md5(database.encode()).hexdigest()}_{item_id}.pickle'
    
    if os.path.exists(file_path) and (datetime.now()-datetime.fromtimestamp(os.stat(file_path).st_mtime) <= timedelta(hours=1)):
        #print(f'{func_name} cache hit', file_path)
        db_rows = pd.read_pickle(file_path)        
    else:    
        query = f'select ITEM_ID, count(distinct STOCK_ID) AS NUM_OPT from MWS_COLT_ITEM_IVT where ITEM_ID={item_id};'
        engine = create_engine(database, encoding='utf8', pool_size=5, pool_recycle=3600, connect_args={'connect_timeout':1000000})
        db_rows = pd.read_sql_query(query, engine) 
        engine.dispose()
        db_rows.to_pickle(file_path)

    return db_rows

def get_num_opt_items(items):
    database = RDB_BI_REV
    
    split_list = [(database, item_id) for item_id in items.ITEM_ID.unique()]
    split_list = split_list[:]
    
    p_list = []

    with Pool(50) as pool:
        #pool.map(set_threshold_task, split_list[:])
        for p in tqdm.tqdm(pool.imap_unordered(get_num_opt_item, split_list), total=len(split_list)):
            p_list.append(p)
    
    num_opt_items = pd.concat(p_list)
    # num_opt_items.to_excel('num_opt_items.xlsx')
    return num_opt_items
'''

expire_cache(hours=24)