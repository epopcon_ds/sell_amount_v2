class pqReader():
    def __init__(dase_path):
        self.dase_path = base_path

    def makeCollectDaysList(sellamt_cal_day, collect_day_window):
         # make a list of days need for the threshold setting process.
        collect_day_start = datetime.strptime(sellamt_cal_day, '%Y-%m-%d') - timedelta(days=collect_day_window)
        collect_day_list = pd.date_range(collect_day_start, periods=collect_day_window + 1).tolist()
        collect_day_list = [datetime.strftime(i, '%Y-%m-%d') for i in collect_day_list]
        collect_day_list.sort()
        self.collect_day_list = collect_day_list
        return self.collect_day_list

    def readFiles(self, collection, partition_col, id_part, collect_day_list):
        collection_part_path = self.base_path +'/'+ collection + "/*"
        files = glob.glob(path + collection_part_path + f'/{partition_col}_PART={id_part}')
        files_selected = [f for f in files if f.split(".")[0].split("_")[-1] in collect_day_list]

        df_full = pd.DataFrame()
        for i in files_selected:
            table = pq.read_table(i, use_threads=10)
            df_temp = table.to_pandas()
            df_full = df_full.append(df_temp)
            del df_temp
            gc.collect()

        return df_full

# ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s



input_path = '~~'
pqR = pqReader(data_path)
collect_day_list = pqR.makeCollectDaysList(sellamt_cal_day, collect_day_window)
pqR.readFiles(self, collection = 'MWS_COLT_ITEM', partition_col= 'ITEM_ID_PART', )
item = pqR.readItem(partition_col = 'ITEM_ID_PART', part = p)
for p in range(0, 19):
    ivt = pqR.readIvt(partition_col = 'ITEM_ID_PART', part = p)
    phis = pqR.readPhis(partition_col = 'ITEM_ID_PART', part = p)



