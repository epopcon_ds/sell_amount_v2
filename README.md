# SELL_AMOUNT_V2



### sellamt_v2 module structure

- 판매량 계산 모듈 버전 2와 관련된 자료는 아래 디렉토리 구조로 관리한다. 
- 

```
sellamt_v2/
├── figures/            <- Figures saved by notebooks and scripts.
├── notebooks/          <- Jupyter notebooks.
│
├── src/     			<- Python package with source code.
│   └── __init__.py     <-- Make the folder a package.
│   └── data/
│       └── dataSaver.py
│       └── saveDataMain.py
├── tests/              <- Tests for your Python package.
    └── test_process.py <-- Tests for process.py.
└── README.md           <- README with info of the project.
```



### sellamt/data folder structure

- 판매량 계산 사용 데이터는 아래 포멧으로 관리한다.
  - 데이터 저장 형식: `parquet` 
  - 데이터 저장 디렉토리 :  `/home/ws-hadoop/DataTeam/sellamt/data/`
  - 위 디렉토리 하위의 데이터 폴더는 기본적으로 아래 구조를 따른다.  

```
data/
├── WSPIDER_11ST/               
│   └── raw/			<- The original, immutable data dump.						
│       └── MWS_COLT_ITEM/
│       └── MWS_COLT_ITEM_IVT/
│       └── MWS_COLT_ITEM_PRICE_HIS/
│       └── MWS_COLT_ITEM_DISCOUNT/
│   └── processed/
│   └── result/
├── WSPIDER_AP/
├── WSPIDER_COUPANG/
├── WSPIDER_HMALL/
├── WSPIDER_LBRAND
├── WSPIDER_OLIVEYOUNG
├── WSPIDER_TMON
└── WSPIDER_WEMAKEPRICE
```