import gc
import sys
import glob
import os

import pymongo
from sqlalchemy import create_engine
from multiprocessing import Pool, cpu_count
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
from pyarrow import parquet as pq 
import telepot



# source code path
path = '/data/sellamt/sell_amount_v2/' 
sys.path.append(path)
from src.data.pqReader import pqReader




class thrsImpSetter:
    def __init__(self, diff):
        self.diff = diff
        # self.slope_params = slope_params
        self.slope_params = diff[['COLLECT_SITE', 'UNIT_HOUR', 'MIN_THRS_PUH', 'MIN_IMP_PUH','MIN_IMP_PUH_DEAL', 'MIN_THRS_PUH_DEAL', 'REPLACE_ZERO_DIFF', 'Z_SCORE_MAX']]
        self.reg_id = 'ALGORITHM'
        self.reg_dt = datetime.now()
        self.collect_day_window = diff['COLLECT_DAY'].nunique()
        self.base_collect_day = diff['COLLECT_DAY'].max()
        self.item_id_part = (diff['ITEM_ID'] % 19).values[0]

    def applyMultiProc(self, test = False):
        groupedDiff = self.diff.groupby(['ITEM_ID', 'STOCK_ID'])
        with Pool(10) as p:
            # p.map(self.getResult, [sdf for name, sdf in groupedDiff])
            df_list = p.map(self.getResult, [sdf for name, sdf in groupedDiff])
        result = pd.concat(df_list)

        if test:
            result['REG_ID'] = 'TEST'
            # result.to_pickle(f'/data/cnr/SELLAMT_V2/thrs_imp_WSPIDER_11ST_TEST_{self.item_id_part}_v2.pickle')
            # print('FINISEHED:', f'/data/cnr/SELLAMT_V2/thrs_imp_WSPIDER_11ST_TEST_{self.item_id_part}_v2.pickle')
        else:
            # Save result into Monogo
            pass
            
        collection = 'SELL_AMT_THRS_IMP'
        host = '133.186.168.8'
        port = 50000
        client = pymongo.MongoClient(host, port)
        ref_database = self.diff['MONGO_DB'].unique()[0]
        mongodb = client[ref_database.upper()]
        mongodb[collection].insert_many(result.to_dict('records'))
        client.close()


    def groupbyapply(self):
        self.diff.groupby(['ITEM_ID', 'STOCK_ID']).apply(lambda x: self.getResult(x))

    def getResult(self, sdf):
        iid = sdf['ITEM_ID'].unique()[0]
        iip = iid % 19
        sid = sdf['STOCK_ID'].unique()[0]
        csite = sdf['COLLECT_SITE'].unique()[0]
        source = 'ORIGINAL_DB'
        stk_diff_mean = sdf['STK_DIFF'].mean()
        regdt_diff_mean = sdf['REGDT_DIFF'].mean()
        data_points_count = len(sdf)
        min_thrs_puh = sdf['MIN_THRS_PUH'].unique()[0]
        min_imp_puh = sdf['MIN_IMP_PUH'].unique()[0]
        min_thrs_puh_deal = sdf['MIN_THRS_PUH_DEAL'].unique()[0]
        min_imp_puh_deal = sdf['MIN_IMP_PUH_DEAL'].unique()[0]

        unit_hour = sdf['UNIT_HOUR'].unique()[0]
        ref_database = sdf['MONGO_DB'].unique()[0]
        # replace_zero_diff = sdf['REPLACE_ZERO_DIFF'].values[0]

        # filter out STK_DIFF == 0 or minus. 
        sdf = sdf[sdf['STK_DIFF'] >= 0]
        if sdf.empty:
            stock_diff_thrs_puh = min_thrs_puh
            impute_value_puh = min_imp_puh
            
            stock_diff_thrs_puh_deal = min_thrs_puh_deal
            impute_value_puh_deal = min_imp_puh_deal
            data_points_count_deal = 0
            reasonable_slopes_max_psec = None
            reason = '1. No DIFF >= 0'
            reason_deal = '1. No DIFF >= 0'

        else: 
            sdf = sdf.assign(SLOPE =  sdf['STK_DIFF'] / sdf['REGDT_DIFF'])
            general_result, deal_result = self.getRsnbSlpMx(sdf)
            down_slopes, reasonable_slopes_max_psec, reason = general_result
            deal_down_slopes, reasonable_slopes_max_psec_deal, reason_deal = deal_result
            
            # (1) General
            stock_diff_thrs_puh, impute_value_puh = self.setThrsImp(sdf, down_slopes, reasonable_slopes_max_psec)
            
            # (2) Deal
            stock_diff_thrs_puh_deal, impute_value_puh_deal = self.setThrsImp(sdf, deal_down_slopes, reasonable_slopes_max_psec_deal, deal = True)
            data_points_count_deal = len(deal_down_slopes)

        result = pd.DataFrame({'ITEM_ID_PART': [iip], 'ITEM_ID': [iid], 'STOCK_ID': [sid],'COLLECT_SITE': [csite],
                               'RSN_SLOPE_MAX': [reasonable_slopes_max_psec], 
                               'STK_DIFF_MEAN': [stk_diff_mean], 'REGDT_DIFF_MEAN': [regdt_diff_mean], 
                               'REASON': [reason], 'REASON_DEAL': [reason_deal],
                               'SLOPE_THRS_PUH': [stock_diff_thrs_puh], 'IMP_VALUE_PUH': [impute_value_puh], 'N_DATA_POINTS': [data_points_count],
                               'SLOPE_THRS_PUH_DEAL': [stock_diff_thrs_puh_deal], 'IMP_VALUE_PUH_DEAL': [impute_value_puh_deal], 'N_DATA_POINTS_DEAL':  [data_points_count_deal],
                               'UNIT_HOUR': [unit_hour],
                               'SOURCE': [source],
                               'COLLECT_DAY_BASE': [self.base_collect_day], 'COLLECT_DAY_WINDOW': [self.collect_day_window],
                               'REG_ID': [self.reg_id], 'REG_DT': self.reg_dt})
        
        return result


    def getRsnbSlpMx(self, sdf):
        '''
        newer version ! as of 190515
        '''
        # ----------------------------------------------------
        # 1) check if the stock is appropriate for setting threshold
        # cond1) Is data collection period longer than 48 hours?
        # cond2) Is there more than 7 data points?
        collection_period = sdf.REG_DT.max() - sdf.REG_DT.min()
        cond1 = collection_period < timedelta(hours=48)  
        cond2 = len(sdf) < 7 

        # ----------------------------------------------------
        # 2) calculate mod_z applied threshold for the slope
        if (cond1 | cond2):
            # 2-1) if under cond1 or cond2, set reasonalbe_slope_max as None.
            down_slopes = None
            reasonable_slopes_max_psec = None
            reason = '2. Not enough Data Points'

        else:
            # 2-2) if not, set reasonable_slope_max using modified-z score.
            # (1) get down slopes
            down_slopes = self.getSlopes(sdf)
            z_score_max = sdf['Z_SCORE_MAX'].values[0]

            # (2) calculate reasonable max slope **per a second** (could be None)
            if (len(down_slopes) != 0):
                reasonable_slopes_max_psec, detail_reason = self.calSlopeMax(down_slopes, z_score_max)
                reason = '0 - ' + detail_reason 
            else:
                reasonable_slopes_max_psec = None
                reason = '3. No down Slopes'
        
            
        if ((sdf['REGDT_DIFF'] <= 5*60*60).sum() < 5):
            deal_down_slopes = pd.Series()
            reasonable_slopes_max_psec_deal = None
            reason_deal = '3. Not enough deal data Points'
        else:
            deal_down_slopes = self.getSlopes(sdf, deal = True)
            z_score_max = sdf['Z_SCORE_MAX'].values[0]
            
            if (len(deal_down_slopes) != 0):
                reasonable_slopes_max_psec_deal, detail_reason_deal = self.calSlopeMax(deal_down_slopes, z_score_max)
                reason_deal = '0 - ' + detail_reason_deal 
            else: 
                reasonable_slopes_max_psec_deal = None
                reason_deal = '3. Not enough deal data Points'
                
                
                
        return (down_slopes, reasonable_slopes_max_psec, reason), (deal_down_slopes, reasonable_slopes_max_psec_deal, reason_deal)
#         return reasonable_slopes_max_psec, reason
    

#     def setThrsImp(self, sdf, down_slopes, reasonable_slopes_max_psec):
#         # ----------------------------------------------------
#         # 3) set stock diff thrs & impute value (per a unit hour)
#         if reasonable_slopes_max_psec:
#             # 3-1) set threshold and impute values according to the reasonable slope max.
#             # (1) set thrs
#             reasonable_slope_max_puh = round(reasonable_slopes_max_psec * 60 * 60 * sdf['UNIT_HOUR'].values[0])
#             stock_diff_thrs_puh = max(reasonable_slope_max_puh, sdf['MIN_THRS_PUH'].values[0])
# #             detail_reason = '1. RSN_SLOPE_MAX:' + str(reasonable_slope_max_puh)
#             # (2) set impute value
#             normal_slope_condition = down_slopes <= reasonable_slopes_max_psec
#             normal_slope_median_puh = round((down_slopes[normal_slope_condition]).median() * 60 * 60 * sdf['UNIT_HOUR'].values[0])
#             impute_value_puh = max(normal_slope_median_puh, sdf['MIN_IMP_PUH'].values[0])

#         else:
#             # If reasonable_slopes_max_psec is None (on step #1 or #3-2)
#             # Take min_allow_thrs_puh & min_allow_imp_puh
#             stock_diff_thrs_puh = sdf['MIN_THRS_PUH'].values[0]
#             impute_value_puh = sdf['MIN_IMP_PUH'].values[0]

# #         return stock_diff_thrs_puh, impute_value_puh, detail_reason
#         return stock_diff_thrs_puh, impute_value_puh

    def setThrsImp(self, sdf, down_slopes, reasonable_slopes_max_psec, deal = False):
        # ----------------------------------------------------
        # 3) set stock diff thrs & impute value (per a unit hour)
        if deal:
            min_thrs_puh = sdf['MIN_THRS_PUH_DEAL'].values[0]
            min_imp_puh = sdf['MIN_IMP_PUH_DEAL'].values[0]
        else: 
            min_thrs_puh = sdf['MIN_THRS_PUH'].values[0]
            min_imp_puh = sdf['MIN_IMP_PUH'].values[0]
            
        if reasonable_slopes_max_psec:
            # 3-1) set threshold and impute values according to the reasonable slope max.
            # (1) set thrs
            reasonable_slope_max_puh = round(reasonable_slopes_max_psec * 60 * 60 * sdf['UNIT_HOUR'].values[0])
            stock_diff_thrs_puh = max(reasonable_slope_max_puh, min_thrs_puh)
#             detail_reason = '1. RSN_SLOPE_MAX:' + str(reasonable_slope_max_puh)
            # (2) set impute value
            normal_slope_condition = down_slopes <= reasonable_slopes_max_psec
            normal_slope_median_puh = round((down_slopes[normal_slope_condition]).median() * 60 * 60 * sdf['UNIT_HOUR'].values[0])
            impute_value_puh = max(normal_slope_median_puh, min_imp_puh)

        else:
            # If reasonable_slopes_max_psec is None (on step #1 or #3-2)
            # Take min_allow_thrs_puh & min_allow_imp_puh
            stock_diff_thrs_puh = min_thrs_puh
            impute_value_puh = min_imp_puh

#         return stock_diff_thrs_puh, impute_value_puh, detail_reason
        return stock_diff_thrs_puh, impute_value_puh

    def getSlopes(self, sdf, deal = False):
        # (0) filter deal period data
        # sdf['STK_DIFF'] = (sdf['STOCK_AMOUNT'].shift() - sdf['STOCK_AMOUNT'])
        # sdf['REGDT_DIFF'] = (sdf['REG_DT'] - sdf['REG_DT'].shift())
        # ivt_ids_except_deal_period = sdf[(~sdf['REGDT_DIFF'].isna()) & (sdf['REGDT_DIFF'] > '04:00:00')].index

        # ------------------------------------------------------------
        # ------ SCM 데이터에 대해서는 이부분을 꺼두는 걸로 일단 하자.
        # ivt_ids_except_deal_period = sdf[(~sdf['REGDT_DIFF'].isna()) & (sdf['REGDT_DIFF'] > 14400)].index # 4 hours = 14400 seconds
        # sdf = sdf[sdf.index.isin(ivt_ids_except_deal_period)]
        # ------------------------------------------------------------
        # (1) get stock_amount_diff
        stock_amount_diff = sdf['STK_DIFF'].fillna(0)
 
        # (2) get reg_dt_diff
        reg_dt_diff = sdf['REGDT_DIFF']
        
        if not deal:
            # (3) get slope -- for general
            reg_dt_diff_median = reg_dt_diff.median() 
            # if reg_dt_diff is too short(less than reg_dt_diff_median), replace reg_dt_diff with the half of reg_dt_diff_median.
            reg_dt_diff = reg_dt_diff.apply(
                lambda rddiff: reg_dt_diff_median * .5 if rddiff < reg_dt_diff_median else rddiff)

            slop_per_sec = stock_amount_diff / reg_dt_diff
            down_slopes = slop_per_sec[slop_per_sec >= 0]
            down_slopes = down_slopes.replace(0, 1.2/(8*60*60))
            return down_slopes
        
        else: # == if deal
            if any(reg_dt_diff <= 5*60*60):
                reg_dt_diff = reg_dt_diff.apply(lambda rddiff: 600 if rddiff < 400 else rddiff) # 수집 간격이 10분 미만(중복 url로 인함)인 경우 10분 간격의 diff로 봄 
                deal_stock_amount_diff = stock_amount_diff[reg_dt_diff < 5*60*60]
                deal_reg_dt_diff = reg_dt_diff[reg_dt_diff < 5*60*60]
                slop_per_sec = deal_stock_amount_diff / deal_reg_dt_diff
                down_slopes = slop_per_sec[slop_per_sec >= 0]
                down_slopes = down_slopes.replace(0, 1.2/(8*60*60))
            else: 
                down_slopes = pd.Series()
            return down_slopes

    
        # (3) get slope -- for deal period 
#         reg_dt_diff = sdf['REGDT_DIFF']
#         if any(reg_dt_diff <= 5*60*60): # deal period down_slopes
#             deal_stock_amount_diff = stock_amount_diff[reg_dt_diff < 5*60*60]
#             deal_reg_dt_diff = reg_dt_diff[reg_dt_diff < 5*60*60]
#             slop_per_sec = deal_stock_amount_diff / deal_reg_dt_diff
#             deal_down_slopes = slop_per_sec[slop_per_sec >= 0]
#             deal_down_slopes = deal_down_slopes.replace(0, 1.2/(8*60*60))
#             print('type(deal_down_slopes)', type(deal_down_slopes))
#         else: 
#             deal_down_slopes = pd.Series()
            
        return down_slopes, deal_down_slopes


    def calSlopeMax(self, down_slopes, z_score_max):
        # calculate mod_z score for each down_slope value & set stock_diff threshold for outlier dectection
        '''
        Calculate reasonable max value (per second) of slopes according to mod-z score.

        > Conditions :
        To calculate reasonable_slope_max_ph, MAD should not be zero.

        > Params :
        - down_slopes : pd.Series. dropped STOCK AMOUNT.
        - z_score_max : int. baseline for cutting modified-z socre outliers.

        > Return :
        - reasonable_slopes_max_psec : reasonable slope max value per a second

        > References:
        - z_score_max : https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm
        "These authors recommend that modified Z-scores with an absolute value of greater than 3.5
        be labeled as potential outliers."
        '''
        down_slopes_median = np.median(down_slopes)
        MAD = np.median(np.abs(down_slopes - down_slopes_median))
        condition = (MAD != 0)
        if condition:
            
            #         print("Set reasonable_slopes_max_psec as max(normal_down_slopes)")
            modified_z_scores = 0.6745 * ((down_slopes - down_slopes_median) / MAD)
            normal_down_slopes = down_slopes[modified_z_scores < z_score_max]
            reasonable_slopes_max_psec = max(normal_down_slopes)
            detail_reason = '1. (MAD != 0):' + str(MAD)
        else:
            #         print("Set reasonable_slopes_max_psec as None")
            reasonable_slopes_max_psec = None
            detail_reason = '1. (MAD == 0):' + str(MAD)
        
        return reasonable_slopes_max_psec, detail_reason


# class thrsImpSetter_old:
#     def __init__(self, diff):
#         self.diff = diff
#         # self.slope_params = slope_params
#         self.slope_params = diff[['COLLECT_SITE', 'UNIT_HOUR', 'MIN_THRS_PUH', 'MIN_IMP_PUH', 'REPLACE_ZERO_DIFF', 'Z_SCORE_MAX']]
        
#         self.reg_id = 'ALGORITHM'
#         self.reg_dt = datetime.now()
#         self.collect_day_window = diff['COLLECT_DAY'].nunique()
#         self.base_collect_day = diff['COLLECT_DAY'].max()


#     def applyMultiProc(self, test = False):
#         groupedDiff = self.diff.groupby(['ITEM_ID', 'STOCK_ID'])
#         with Pool(10) as p:
#             # p.map(self.getResult, [sdf for name, sdf in groupedDiff])
#             df_list = p.map(self.getResult, [sdf for name, sdf in groupedDiff])
#         result = pd.concat(df_list)

#         if test:
#             result['REG_ID'] = 'TEST'

#         # Save result into Monogo
#         collection = 'SELL_AMT_THRS_IMP'
#         host = '133.186.168.8'
#         port = 50000
#         client = pymongo.MongoClient(host, port)
#         ref_database = self.diff['MONGO_DB'].unique()[0]
#         mongodb = client[ref_database.upper()]
#         mongodb[collection].insert_many(result.to_dict('records'))
#         client.close()


#     def groupbyapply(self):
#         self.diff.groupby(['ITEM_ID', 'STOCK_ID']).apply(lambda x: self.getResult(x))

#     def getResult(self, sdf):
#         iid = sdf['ITEM_ID'].unique()[0]
#         iip = iid % 19
#         sid = sdf['STOCK_ID'].unique()[0]
#         csite = sdf['COLLECT_SITE'].unique()[0]
#         source = 'ORIGINAL_DB'
#         stk_diff_mean = sdf['STK_DIFF'].mean()
#         regdt_diff_mean = sdf['REGDT_DIFF'].mean()
#         data_points_count = len(sdf)
#         min_thrs_puh = sdf['MIN_THRS_PUH'].unique()[0]
#         min_imp_puh = sdf['MIN_IMP_PUH'].unique()[0]
#         unit_hour = sdf['UNIT_HOUR'].unique()[0]
#         ref_database = sdf['MONGO_DB'].unique()[0]
#         # replace_zero_diff = sdf['REPLACE_ZERO_DIFF'].values[0]

#         # filter out STK_DIFF == 0 or minus. 
#         sdf = sdf[sdf['STK_DIFF'] >= 0]
#         if sdf.empty:
#             stock_diff_thrs_puh = min_thrs_puh
#             impute_value_puh = min_imp_puh

#         else: 
#             sdf = sdf.assign(SLOPE =  sdf['STK_DIFF'] / sdf['REGDT_DIFF'])

#             down_slopes, reasonable_slopes_max_psec = self.getRsnbSlpMx(sdf)
#             # print('down_slopes', iid, sid, down_slopes)
#             stock_diff_thrs_puh, impute_value_puh = self.setThrsImp(sdf, down_slopes, reasonable_slopes_max_psec)

#             #         print('RESULT ---- ')
#             #         print({'ITEM_ID': [self.iid], 'STOCK_ID': [self.sid], 'STOCK_DIFF_THRS_PUH': [self.stock_diff_thrs_puh],
#             #                              'IMP_VALUE_PUH': [self.impute_value_puh], 'UNIT_HOUR': [self.unit_hour]})

#         result = pd.DataFrame({'ITEM_ID_PART': [iip], 'ITEM_ID': [iid], 'STOCK_ID': [sid],'COLLECT_SITE': [csite],
#                                'SLOPE_THRS_PUH': [stock_diff_thrs_puh], 'IMP_VALUE_PUH': [impute_value_puh], 'UNIT_HOUR': [unit_hour],
#                                'SOURCE': [source],
#                                'STK_DIFF_MEAN': [stk_diff_mean], 'REGDT_DIFF_MEAN': [regdt_diff_mean], 'N_DATA_POINTS': [data_points_count],
#                                'COLLECT_DAY_BASE': [self.base_collect_day], 'COLLECT_DAY_WINDOW': [self.collect_day_window],
#                                'REG_ID': [self.reg_id], 'REG_DT': self.reg_dt})

#         return result


#     def getRsnbSlpMx(self, sdf):
#         '''
#         newer version ! as of 190515
#         '''
#         # ----------------------------------------------------
#         # 1) check if the stock is appropriate for setting threshold
#         # cond1) Is data collection period longer than 48 hours?
#         # cond2) Is there more than 7 data points?
#         collection_period = sdf.REG_DT.max() - sdf.REG_DT.min()
#         cond1 = collection_period < timedelta(hours=48)
#         cond2 = len(sdf) < 7

#         # ----------------------------------------------------
#         # 2) calculate mod_z applied threshold for the slope
#         if (cond1 | cond2):
#             # 2-1) if under cond1 or cond2, set reasonalbe_slope_max as None.
#             down_slopes = None
#             reasonable_slopes_max_psec = None

#         else:
#             # 2-2) if not, set reasonable_slope_max using modified-z score.
#             # (1) get down slopes
#             down_slopes = self.getSlopes(sdf)
#             z_score_max = sdf['Z_SCORE_MAX'].values[0]

#             # (2) calculate reasonable max slope **per a second** (could be None)
#             if (len(down_slopes) != 0):
#                 reasonable_slopes_max_psec = self.calSlopeMax(down_slopes, z_score_max)
#             else:
#                 reasonable_slopes_max_psec = None

#         return down_slopes, reasonable_slopes_max_psec

#     def setThrsImp(self, sdf, down_slopes, reasonable_slopes_max_psec):
#         # ----------------------------------------------------
#         # 3) set stock diff thrs & impute value (per a unit hour)
#         if reasonable_slopes_max_psec:
#             # 3-1) set threshold and impute values according to the reasonable slope max.
#             # (1) set thrs
#             reasonable_slope_max_puh = round(reasonable_slopes_max_psec * 60 * 60 * sdf['UNIT_HOUR'].values[0])
#             stock_diff_thrs_puh = max(reasonable_slope_max_puh, sdf['MIN_THRS_PUH'].values[0])
#             # (2) set impute value
#             normal_slope_condition = down_slopes <= reasonable_slopes_max_psec
#             normal_slope_median_puh = round((down_slopes[normal_slope_condition]).median() * 60 * 60 * sdf['UNIT_HOUR'].values[0])
#             impute_value_puh = max(normal_slope_median_puh, sdf['MIN_IMP_PUH'].values[0])

#         else:
#             # If reasonable_slopes_max_psec is None (on step #1 or #3-2)
#             # Take min_allow_thrs_puh & min_allow_imp_puh
#             stock_diff_thrs_puh = sdf['MIN_THRS_PUH'].values[0]
#             impute_value_puh = sdf['MIN_IMP_PUH'].values[0]

#         return stock_diff_thrs_puh, impute_value_puh

#     def getSlopes(self, sdf):
#         # (0) filter deal period data
#         # sdf['STK_DIFF'] = (sdf['STOCK_AMOUNT'].shift() - sdf['STOCK_AMOUNT'])
#         # sdf['REGDT_DIFF'] = (sdf['REG_DT'] - sdf['REG_DT'].shift())
#         # ivt_ids_except_deal_period = sdf[(~sdf['REGDT_DIFF'].isna()) & (sdf['REGDT_DIFF'] > '04:00:00')].index

#         # ------------------------------------------------------------
#         # ------ SCM 데이터에 대해서는 이부분을 꺼두는 걸로 일단 하자.
#         # ivt_ids_except_deal_period = sdf[(~sdf['REGDT_DIFF'].isna()) & (sdf['REGDT_DIFF'] > 14400)].index # 4 hours = 14400 seconds
#         # sdf = sdf[sdf.index.isin(ivt_ids_except_deal_period)]
#         # ------------------------------------------------------------

#         # (1) get stock_amount_diff
#         stock_amount_diff = sdf['STK_DIFF'].fillna(0)
#         # stock_amount_diff = stock_amount_diff.replace(0, sdf['REPLACE_ZERO_DIFF'].values[0]) --> 삭제함

#         # (2) get reg_dt_diff
#         reg_dt_diff = sdf['REGDT_DIFF']
#         # reg_dt_diff_median = (reg_dt_diff[(reg_dt_diff > timedelta(hours=1).total_seconds())]).median()---> 삭제함
#         reg_dt_diff_median = reg_dt_diff.median() 
#         # if reg_dt_diff is too short(less than reg_dt_diff_median), replace reg_dt_diff with the half of reg_dt_diff_median.
#         reg_dt_diff = reg_dt_diff.apply(
#             lambda rddiff: reg_dt_diff_median * .5 if rddiff < reg_dt_diff_median else rddiff)

#         # (3) get slope
#         slop_per_sec = stock_amount_diff / reg_dt_diff
#         down_slopes = slop_per_sec[slop_per_sec >= 0]
#         down_slopes = down_slopes.replace(0, 1.2/(8*60*60)) 
        


#         return down_slopes

#     def calSlopeMax(self, down_slopes, z_score_max):
#         # calculate mod_z score for each down_slope value & set stock_diff threshold for outlier dectection
#         '''
#         Calculate reasonable max value (per second) of slopes according to mod-z score.

#         > Conditions :
#         To calculate reasonable_slope_max_ph, MAD should not be zero.

#         > Params :
#         - down_slopes : pd.Series. dropped STOCK AMOUNT.
#         - z_score_max : int. baseline for cutting modified-z socre outliers.

#         > Return :
#         - reasonable_slopes_max_psec : reasonable slope max value per a second

#         > References:
#         - z_score_max : https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm
#         "These authors recommend that modified Z-scores with an absolute value of greater than 3.5
#         be labeled as potential outliers."
#         '''
#         down_slopes_median = np.median(down_slopes)
#         MAD = np.median(np.abs(down_slopes - down_slopes_median))
#         condition = (MAD != 0)
#         if condition:
#             #         print("Set reasonable_slopes_max_psec as max(normal_down_slopes)")
#             modified_z_scores = 0.6745 * ((down_slopes - down_slopes_median) / MAD)
#             normal_down_slopes = down_slopes[modified_z_scores < z_score_max]
#             reasonable_slopes_max_psec = max(normal_down_slopes)
#         else:
#             #         print("Set reasonable_slopes_max_psec as None")
#             reasonable_slopes_max_psec = None

#         return reasonable_slopes_max_psec




## --- 
def setThrsImpMain(ref_mongo_database, collect_site):
    telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
    bot = telepot.Bot(token=telegbot_token)
    chat_id = 592874873
    base_path = f'/data/sellamt/data/{ref_mongo_database}'
    print(base_path)
    # ----------------------
    thrs_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    collect_day_window = 20
    pqR = pqReader(base_path, thrs_cal_day, collect_day_window)
    # col_day_list = pqR.makeCollectDaysList(thrs_cal_day, collect_day_window)
    # ----------------------
    start_time = datetime.now()
    print(f"START Setting Thresholds and Impute Values module for {collect_site}")

    msg = f"""Start Setting Thresholds and Impute Values module for {collect_site}
        Get Thrs & Imp values of following days..
        >> maximum collect_day : [{thrs_cal_day}]
        >>collect day window : [{collect_day_window}] days
        =========================================="""
    bot.sendMessage(chat_id=chat_id, text=msg)

    # >> Read slope params
    print("GET SELL_AMT_MOD_Z_HYP")
    host = '133.186.168.8'
    port = 50000
    client = pymongo.MongoClient(host, port)
    mongodb = client[ref_mongo_database.upper()]
    collection = 'SELL_AMT_THRS_IMP'

    # get recent thrs
    cursor = mongodb[collection].find({'ITEM_ID_PART': 18, 'COLLECT_SITE': collect_site, 'REG_ID': 'ALGORITHM'},
                                      {'REG_DT': True, '_id': False}).limit(1)
    thrs_imp_df_last_1 = pd.DataFrame(list(cursor))
    print('thrs_imp_df_last_1.shape', thrs_imp_df_last_1.shape)
    today = str(datetime.now())[:10]




    if not thrs_imp_df_last_1.empty:
        thrs_imp_last_regdt = str(thrs_imp_df_last_1['REG_DT'][0])[:10]
    else:
        thrs_imp_last_regdt = 'dummy regdt! thrs_imp never been set'

    print('thrs_imp_last_regdt', thrs_imp_last_regdt)
    if thrs_imp_last_regdt == today:
        print('(thrs_imp_df_last_1.empty)', (thrs_imp_df_last_1.empty))
        print('(thrs_imp_last_regdt == today)', (thrs_imp_last_regdt == today))
        msg = f"No Need to set Thrs again. Use Thrs&Imp already set on {thrs_imp_last_regdt}"
        print(msg)
        bot.sendMessage(chat_id=chat_id, text=msg)
        print('TIME SPENT:', str(datetime.now() - start_time))

    else:
        print('SET NEW THRS!')
        collection = 'SELL_AMT_MOD_Z_HYP'
        cursor = mongodb[collection].find({'COLLECT_SITE': collect_site}, {'_id': False},)
        slope_params = pd.DataFrame(list(cursor))
        slope_params = slope_params.sort_values('REG_DT')
        slope_params = slope_params.drop_duplicates('COLLECT_SITE', keep='last')

        ## main
        for iip in range(0, 19):
            print("ITEM_ID_PART: ", iip)
            msg = f"""Start ITEM_ID_PART: {iip}"""
            bot.sendMessage(chat_id=chat_id, text=msg)

            # 1. Read Data
            # >> Read diff table
            diff = pqR.readDiff(partition_1_part = '골프', id_part = iip)
            print(iip,'골프 diff length', len(diff))
            diff['COLLECT_SITE'] = collect_site
            diff = diff.sort_values('REG_DT')
            diff['MONGO_DB'] = base_path.split('/')[-1]
            read_collectday_list = diff['COLLECT_DAY'].unique()
            read_collectday_list.sort()
            print('read_collectday_list', read_collectday_list)

            # >> Merge diff & slopes
            diff_params = pd.merge(diff, slope_params[
                ['COLLECT_SITE', 'UNIT_HOUR', 'MIN_THRS_PUH', 'MIN_IMP_PUH', 'MIN_IMP_PUH_DEAL', 'MIN_THRS_PUH_DEAL','REPLACE_ZERO_DIFF', 'Z_SCORE_MAX']],
                                   on='COLLECT_SITE', how='left')

            # 2. Set THRS & IMP
            # thrs_imp = thrsImpSetter_test(diff_params).applyMultiProc()
            thrsImpSetter(diff_params).applyMultiProc(test = False)
            # thrsImpSetter(diff_params).groupbyapply()

            msg = f"""Complete ITEM_ID_PART: {iip}
            len(input diff): {len(diff)}
            len(merged w/ params): {len(diff_params)}
            TIME SPENT (cum) : {str(datetime.now() - start_time)}"""
            bot.sendMessage(chat_id=chat_id, text=msg)

            del diff_params
            # del thrs_imp
            del diff
            gc.collect()
            print("--------------------------------------------------------")
    client.close()
    msg = f"""Completed Set Thrs module for {collect_site}."""
    bot.sendMessage(chat_id=chat_id, text=msg)
    print('TIME SPENT:', str(datetime.now() - start_time))

########################################################################
########################################################################
########################################################################
########################################################################
########################################################################
########################################################################

def setThrsImpMain_temp(ref_mongo_database, collect_site):
    telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
    bot = telepot.Bot(token=telegbot_token)
    chat_id = 592874873
    base_path = f'/data/sellamt/data/{ref_mongo_database}'
    print(base_path)
    # ----------------------
    # thrs_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y-%m-%d')
    collect_day_window = 20
    thrs_cal_day = datetime.strftime(datetime.now() - timedelta(days=1), '%Y%m%d')
    thrs_start_day = datetime.strftime(datetime.strptime(thrs_cal_day, '%Y%m%d') - timedelta(days=collect_day_window), '%Y%m%d')

    # ----------------------
    start_time = datetime.now()
    print(f"START Setting Thresholds and Impute Values module for {collect_site}")

    msg = f"""Start Setting Thresholds and Impute Values module for {collect_site}
        Get Thrs & Imp values of following days..
        >> maximum collect_day : [{thrs_cal_day}]
        >>collect day window : [{collect_day_window}] days
        =========================================="""
    bot.sendMessage(chat_id=chat_id, text=msg)

    # >> Read slope params
    print("GET SELL_AMT_MOD_Z_HYP")
    host = '133.186.168.8'
    port = 50000
    client = pymongo.MongoClient(host, port)
    mongodb = client[ref_mongo_database.upper()]
    collection = 'SELL_AMT_THRS_IMP'

    # get recent thrs
    cursor = mongodb[collection].find({'ITEM_ID_PART': 18, 'COLLECT_SITE': collect_site, 'REG_ID': 'TEST'},
                                      {'REG_DT': True, '_id': False}).limit(1)
    thrs_imp_df_last_1 = pd.DataFrame(list(cursor))
    print('thrs_imp_df_last_1.shape', thrs_imp_df_last_1.shape)
    today = str(datetime.now())[:10]

    if not thrs_imp_df_last_1.empty:
        thrs_imp_last_regdt = str(thrs_imp_df_last_1['REG_DT'][0])[:10]
    else:
        thrs_imp_last_regdt = 'dummy regdt! thrs_imp has never been set'

    print('thrs_imp_last_regdt', thrs_imp_last_regdt)


    # set thrs or pass.
    if thrs_imp_last_regdt == today:
        print('(thrs_imp_df_last_1.empty)', (thrs_imp_df_last_1.empty))
        print('(thrs_imp_last_regdt == today)', (thrs_imp_last_regdt == today))
        msg = f"No Need to set Thrs again. Use Thrs&Imp already set on {thrs_imp_last_regdt}"
        print(msg)
        bot.sendMessage(chat_id=chat_id, text=msg)
        print('TIME SPENT:', str(datetime.now() - start_time))

    else:
        msg = f"""{collect_site}: SET_TRHS Start"""
        bot.sendMessage(chat_id=chat_id, text=msg)

        print('SET NEW THRS!')
        # 0. Read MOD_Z_HYP
        collection = 'SELL_AMT_MOD_Z_HYP'
        cursor = mongodb[collection].find({'COLLECT_SITE': collect_site}, {'_id': False})
        slope_params = pd.DataFrame(list(cursor))
        slope_params = slope_params.sort_values('REG_DT')
        slope_params = slope_params.drop_duplicates('COLLECT_SITE', keep='last')

        # 1. Read DIFF data
        diff =  pd.DataFrame(client[ref_mongo_database]['SELL_AMT_DIFF'].find({'COLLECT_DAY': {'$gte': thrs_start_day}}))
        diff = diff.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT'], keep = 'last')
        diff = diff[~((diff['STOCK_AMOUNT'] == 0) & (diff['STK_DIFF'] != 1))]
        print('>> diff length', len(diff))
        diff = diff.sort_values('REG_DT')
        diff['COLLECT_SITE'] = collect_site
        diff['MONGO_DB'] = ref_mongo_database

        read_collectday_list = diff['COLLECT_DAY'].unique()
        read_collectday_list.sort()
        print('>> read_collectday_list', read_collectday_list)

        # >> Merge diff & slopes
        diff_params = pd.merge(diff, slope_params[['COLLECT_SITE', 'UNIT_HOUR', 'MIN_THRS_PUH', 'MIN_IMP_PUH', 'MIN_THRS_PUH_DEAL', 'MIN_IMP_PUH_DEAL', 'REPLACE_ZERO_DIFF', 'Z_SCORE_MAX']],
                               on='COLLECT_SITE', how='left')

        # 2. Set THRS & IMP
        thrsImpSetter(diff_params).applyMultiProc(test = True)
        

        msg = f"""Completed Set Thrs module for {collect_site}.
        len(input diff): {len(diff)}
        len(merged w/ params): {len(diff_params)}
        TIME SPENT (cum) : {str(datetime.now() - start_time)}"""
        bot.sendMessage(chat_id=chat_id, text=msg)

        del diff_params
        # del thrs_imp
        del diff
        gc.collect()
    print('TIME SPENT:', str(datetime.now() - start_time))
