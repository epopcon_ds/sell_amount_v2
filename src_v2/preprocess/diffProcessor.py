from multiprocessing import Pool, cpu_count
from datetime import datetime
import sys
import os
import pandas as pd
import numpy as np
import pymongo
import sys
path = '/data/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.data.pqReader import pqReader


# Related folder structure ...
# under '/data/sellamt/'
# data
# |- WSPIDER_11ST
#   |- raw          <- input data source
#   |- processed    <- output data directory
#       |- DIFF
#
#
# sell_amount_v2/src
# |- preprocess
#       |- diffProcessor.py
#            |- diffProcessor
#            |- editData
#            |- ivtBaseFilter
#       |- getDiffMain_template.py

class ivtBaseFilter:
    '''
    Filtering IVT data module.

    Use one of two main action methods:
    (1) ivtBaseFilter.forSCM(ivt_df, collect_site)
        - outBug
        - outNochangedItems

    (2) ivtBaseFilter.forNormal(ivt_df, collect_site)
        - outBug
        - outNochangedItems
        - outMinusStockAmount

    '''

    def __init__(self, ivt_df, collect_site):
        '''
        params:
        - ivt_df : MWS_COLT_ITEM_IVT data
        - collect_site : string. if it is not one, put 'not_one'
        '''
        print('-------- Start Filter Out IVT data --------')
        self.df = ivt_df
        self.original_df_size = len(ivt_df)
        self.collect_site = collect_site.upper()

    def forSCM(self):
        print('>> Filter Out IVT of items using SCM')
        self.outBugs()
        self.outNochangedItems()
        return self.df

    def forNormal(self, mark_out = False):
        print('>> Filter Out All Conditions')
        self.outBugs(mark_out = mark_out)
        self.outNochangedItems()
        self.outMinusStockAmount(mark_out = mark_out)
        return self.df

    def MarkOut(self, out_condition, reason):
        self.df.loc[(self.df['OUT'] == False) & out_condition, 'OUT_REASON'] = reason
        self.df.loc[(self.df['OUT'] == False) & out_condition, 'OUT'] = True


    def outBugs(self, mark_out = False):
        if mark_out : 
            print('MARK OUT & OUT REASON')
            self.df['OUT'] = False
            # [Data collection bug] 
            # (1) filter out 11st bug
            if '11st'.upper() in self.collect_site.split('.'):
                bug11st_cond_1 = (self.df['STOCK_AMOUNT'] < 0)
                bug11st_cond_2 = (self.df['STOCK_ID'] == '-1')
                self.MarkOut((bug11st_cond_1| bug11st_cond_2), 'Collection_Bug_11st')
                print(">>>> outBug - 11st - Result :", self.original_df_size, "-->", self.df['OUT'].sum())

            # (2) filter out collected during mis-collected periods
            if 'coupang'.upper() in self.collect_site.split('.'):
                bugcoupang_cond_1 = (self.df.REG_DT.between('2019-01-02 13:00:00', '2019-01-04 18:30:00')) 
                bugcoupang_cond_2 = (self.df.STOCK_AMOUNT == 0)
                self.MarkOut((bugcoupang_cond_1 & bugcoupang_cond_2), 'Collection_Bug_Coupang')
                print(">>>> outBug - coupang - Result :", self.original_df_size, "-->", self.df['OUT'].sum())

            if 'wemakeprice'.upper() in self.collect_site.split('.'):
                bugwemap_cond_1 = (self.df.REG_DT.lt('2019-08-11 23:59:59')) 
                bugwemap_cond_2 = (self.df.STOCK_AMOUNT == 0)
                self.MarkOut((bugwemap_cond_1 & bugwemap_cond_1), 'Collection_Bug_WeMakePrice')
                print(">>>> outBug - wemakeprice-zero-stock-amount - Result :", self.original_df_size, "-->", self.df['OUT'].sum())

            # [MongoDB transfer bug] 
            # (1) drop duplicates by ['ID']
            duplicated_ID_cond = (self.df.duplicated('ID', keep = 'last'))
            self.MarkOut(duplicated_ID_cond, 'MongoDB_IVT_Transfer_Bug')
            print(">>>> outBug - coupang - Result :", self.original_df_size, "-->", self.df['OUT'].sum())


        else: 
            # [Data collection bug] filter out 11st bug
            if '11st'.upper() in self.collect_site.split('.'):
                self.df = self.df[self.df['STOCK_AMOUNT'] >= 0]
                self.df = self.df[self.df['STOCK_ID'] != '-1']
                print(">>>> outBug - 11st - Result :", self.original_df_size, "-->", len(self.df))

            if self.df.empty:  # no ivt left
                self.df = pd.DataFrame()

            # [Data collection bug] filter out collected during mis-collected periods
            if 'coupang'.upper() in self.collect_site.split('.'):
                self.df = self.df[
                    ~(self.df.REG_DT.between('2019-01-02 13:00:00', '2019-01-04 18:30:00') & (self.df.STOCK_AMOUNT == 0))]
                print(">>>> outBug - coupang - Result :", self.original_df_size, "-->", len(self.df))

            if 'wemakeprice'.upper() in self.collect_site.split('.'):
                self.df = self.df[self.df['STOCK_AMOUNT'] != 0]
                print(">>>> outBug - wemakeprice-zero-stock-amount - Result :", self.original_df_size, "-->", len(self.df))

            if self.df.empty:  # no ivt left
                self.df = pd.DataFrame()

            # [MongoDB transfer bug] drop duplicates by ['ID']
            self.df = self.df.drop_duplicates(['ID'], keep='last')
            if self.df.empty:  # no ivt left
                self.df = pd.DataFrame()
            print(">>>> outBug - mongo transf - Result :", self.original_df_size, "-->", len(self.df))


    def outNochangedItems(self):    
        # [No-changed items] filter items which have no changes.(only left have been changed ivt data)
        stk = self.df.groupby(['ITEM_ID', 'STOCK_ID']).STOCK_AMOUNT.nunique().reset_index()
        is_stock_changed = (stk.STOCK_AMOUNT > 1)
        stk_changed = stk.loc[is_stock_changed, ['ITEM_ID', 'STOCK_ID']]

        if stk_changed.empty:  # Any items' ivt have been changed
            self.df = pd.DataFrame()
        else:
            self.df = pd.merge(stk_changed, self.df, how='left', on=['ITEM_ID', 'STOCK_ID'])
            print(">>>> outNochanged stock - Result :", self.original_df_size, "-->", len(self.df))

    def outMinusStockAmount(self, mark_out):
        # Only left positive integer(PI) stocks.
        # [Dropped to minus ivts] filter items which have dropped to minus stocks(like -1...).
        # [Dropped to 0 ivts] filter items which have dropped to zero
        if mark_out:
            minus_stock_amount_cond = (self.df.STOCK_AMOUNT < 0)
            self.MarkOut(minus_stock_amount_cond, 'Minus_STOCK_AMOUNT')
        else:
            self.df = self.df[self.df.STOCK_AMOUNT >= 0]
            print(">>>> left Positive STOCK_AMOUNT:", self.original_df_size, "-->", len(self.df))

    def __del__(self):
        print('Finished Filtering IVT data ---------------- ')



class diffProcessor:
    '''
    Making Diff values from  IVT raw data module.

    Use one of two main action methods:
    (1) ivtBaseFilter.forSCM(ivt_df)
        - getSTKDiff
        - getDiffMatrix
        - transformMatrix
        - merge on ivt_df
        - getREGDTDiff

    (2) ivtBaseFilter.forNormal(ivt_df)
        - getSTKDiff
        - getREGDTDiff

    '''
    def __init__(self, ivt):
        self.ivt = ivt
        self.ivt = self.ivt.sort_values('REG_DT')
        print("-------- Get STOCK & REG_DT diff values -- groupby [item_id x stock_id]")
        self.s = datetime.now()
        self.original_len = len(self.ivt)

    def forSCM(self):
        print('Start diffProcessor for SCM using items data from REV')
        # 1. set STOCK_AMOUNT diff values.
        self.ivt = self.ivt.assign(REG_DT_SLOT=self.ivt['REG_DT'].apply(
            lambda dt: dt.replace(minute=0 if dt.minute < 30 else 30, second=0, microsecond=0)))
        print('1. Get STK_DIFF values -- groupby [item_id x stock_id]')
        ivt_diff_df = self.ivt.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getSTKDiff)

        print("2. Make diff matrix -- groupby [collect_site x trust_seller x brand_name x collect_day]")
        ctbc_gdfs = ivt_diff_df.groupby(['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'COLLECT_DAY'])
        m_list = self.applyMultiProc([member_df for name, member_df in ctbc_gdfs], self.getDiffMatrix) 
        ivt_style_m_list = self.applyMultiProc(m_list, self.transformMatrix)
        result_df = pd.concat(ivt_style_m_list)
        print(">> # of groups: ", len(ctbc_gdfs),"len(result_df):", len(result_df))
    
        print("3. Merge on original ivt")
        ivt_diff_df = ivt_diff_df.sort_values('REG_DT')
        ivt_diff_df = ivt_diff_df.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT'], keep = 'last')
        merged = pd.merge(ivt_diff_df, result_df, on = ['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT'], how = 'left')
        print(">> len(ivt_drop_duplicated) <--join--> len(result_df):", len(ivt_diff_df), ' & ', len(result_df))
        print(">> len(merged_df):", len(merged))

        print("4. Make final result df -- groupby [item_id x stock_id]")
        merged = merged.rename(columns={'STK_DIFF_y': 'STK_DIFF'})
        del merged['STK_DIFF_x']

        merged['REG_DT'] = merged['REG_DT_SLOT']
        del merged['REG_DT_SLOT']

        print('5. Get REGDT_DIFF values -- groupby [item_id x stock_id]')
        result = merged.groupby(['ITEM_ID', 'STOCK_ID']).apply(self.getREGDTDiff)
        
        return result

    def forNormal(self):
        print('Start diffProcessor for Normal data (Not using SCM)')
        iidsid_gdfs = self.ivt.groupby(['ITEM_ID', 'STOCK_ID'])
        diff_df_list = self.applyMultiProc([member_df for name, member_df in iidsid_gdfs], self.getFullDiff)
        ivt_diff_df = pd.concat(diff_df_list)
        print('len(ivt):', self.original_len, '---> ', len(ivt_diff_df))
       
        return ivt_diff_df

    def mutePrints(self):
        sys.stdout = open(os.devnull, 'w')

    def applyMultiProc(self, input_list, func):
        with Pool(20, initializer = self.mutePrints) as p:
            output_list = p.map(func, input_list)
        return output_list

    def getFullDiff(self, gdf):
        gdf['STK_DIFF'] = (gdf['STOCK_AMOUNT'].shift() - gdf['STOCK_AMOUNT']).fillna(0)
        gdf['REGDT_DIFF'] = gdf['REG_DT'] - gdf['REG_DT'].shift()
        gdf['REGDT_DIFF'] = gdf['REGDT_DIFF'].apply(lambda x: x.total_seconds())
        gdf['ID_PRIOR'] = gdf['ID'].shift()
        gdf['ID_PRIOR'] = gdf['ID_PRIOR'].fillna(0).astype('int')
        return gdf

    def getSTKDiff(self, gdf):
        gdf['STK_DIFF'] = (gdf['STOCK_AMOUNT'].shift() - gdf['STOCK_AMOUNT']).fillna(0)
        return gdf

    def getREGDTDiff(self, gdf):
        gdf['REGDT_DIFF'] = gdf['REG_DT'] - gdf['REG_DT'].shift()
        gdf['REGDT_DIFF'] = gdf['REGDT_DIFF'].apply(lambda x: x.total_seconds())
        return gdf

    def getDiffMatrix(self, ctb_gdf_cday):
        '''
        collect_site x trust_seller x brand_name x collect_day level process.
        ctb_gdf_cday : ivt grouped by ['COLLECT_SITE', 'TRUST_SELLER', 'BRAND_NAME', 'COLLECT_DAY']. 
        '''
        # [step 1. make a pivot table for the item_id and stock id]
        m = pd.pivot_table(ctb_gdf_cday, index=["ITEM_ID","STOCK_ID"],values=["STK_DIFF"], columns=["REG_DT_SLOT"], aggfunc=[np.sum])
        
        # [step2] pass1: multi option item들을 item 단위로 time slot을 drop 하는 모듈
        m, passed_id_list = self.pass1(m)

        # [step3] pass2: single option item들의 time slot을 drop  
        # pass1을 통과시킨 multi option item들을 묶어서 time slot drop
        m = self.pass2(m, passed_id_list)
        return m
    
    def pass1(self, m):
        pass1_processed_item_id_list = []
        m_multi_pass1_processed = pd.DataFrame()

        # 디버깅의 용의성을 위해서 for loop 사용함. 나중에 performance tuning 필요함.
        for item_id, item_m in m.groupby(level=0): # for each item
            num_stock = len(item_m)
            if num_stock == 1: # skip single option items
                # print(item_id)
                continue
            # plot_m_heatmap(collect_site, trust_seller, brand_name, collect_day, item_m, item_id=item_id)
            for col in item_m.columns: # for each time slot
                slot_values = abs(item_m[col])
                val_cnt = slot_values[slot_values > 0].count()
                val_sum = slot_values[slot_values > 0].sum()

                if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
                    print('Drop TS - pass1:', item_id, col, num_stock, val_cnt, val_sum)
                    m.loc[item_m.index, col] = 0 # drop time slot
                    pass1_processed_item_id_list.append(item_id)

            m_multi_pass1_processed = m_multi_pass1_processed.append(m.loc[item_m.index])

        return m, pass1_processed_item_id_list
    
    def pass2(self, m, pass1_processed_item_id_list):
        if m.empty:
            return m

        stock_id_nunique_df = pd.DataFrame(m.reset_index().groupby('ITEM_ID')['STOCK_ID'].nunique()).rename(columns = {'STOCK_ID': 'STOCK_ID_NUNIQUE'})
        multi_stock_item_ids = stock_id_nunique_df[stock_id_nunique_df['STOCK_ID_NUNIQUE']>1].index
        single_stock_item_ids = stock_id_nunique_df[stock_id_nunique_df['STOCK_ID_NUNIQUE']==1].index
        print('multi_stock_item_ids',multi_stock_item_ids)
        print('single_stock_item_ids',single_stock_item_ids)

        m_single = m.loc[single_stock_item_ids]
        for col in m_single.columns: # for each time slot
            sum_, diff, ts = col
            slot_values = abs(m_single[col])
            val_cnt = slot_values[slot_values > 0].count()
            val_sum = slot_values[slot_values > 0].sum()

            if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
                print('Drop TS - pass2 single', ts, val_cnt, val_sum)
                m.loc[m_single.index, col] = 0 # drop time slot
                
        # # Multi option 추후 수정 필요 -- 일단은 임시로 off -------- 
        # m_multi = m.loc[multi_stock_item_ids]
        # for col in m_multi.columns: # for each time slot
        #     sum_, diff, ts = col
        #     slot_values = abs(m_multi[col])
        #     val_cnt = slot_values[slot_values > 0].count()
        #     val_sum = slot_values[slot_values > 0].sum()

        #     if val_cnt >= 2 and val_sum >= 2: # conditions for drop time slots <-- clustering 
        #         print('Drop TS - pass2 multi', ts, val_cnt, val_sum)
        #         m.loc[m_multi.index, col] = 0 # drop time slot

        return m
    
    def transformMatrix(self, m):
        # transform matrix to ivt style
        temp = m.stack(2).reset_index()
        temp.columns = ['ITEM_ID', 'STOCK_ID', 'REG_DT_SLOT', 'STK_DIFF']
        return temp


    def __del__(self):
        print("Finished diffProcessor.")
        print('time spent: ', str(datetime.now() - self.s))





class editData:
    '''Edit data before and after get diff'''
    def __init__(self):
        print('Activate editData module to add related data')


    def markSCMTypes(self, item, collect_site, scm_ref_database='WSPIDER_LBRAND'):
        print("Mark SCM Types")
        # 1. Read scm filter data table
        host = '133.186.168.8'
        port = 50000
        client = pymongo.MongoClient(host, port)
        mongodb = client[scm_ref_database.upper()]

        # 1. Ready data
        # >> get SELL_AMT_SCM_FILTER
        collection = 'SELL_AMT_SCM_FILTER'
        cursor = mongodb[collection].find({'COLLECT_SITE': collect_site}, {'_id': False})
        scm_filter_df = pd.DataFrame(list(cursor))
        key_column = ['COLLECT_SITE', 'MASTER_SELLER', 'MASTER_BRAND']
        scm_filter_df = scm_filter_df.drop_duplicates(key_column, keep = 'last')
        # keep the most recent records.
        # scm_filter_df = scm_filter_df.sort_values('REG_DT').drop_duplicates(key_column, keep='last')
        scm_filter_df = scm_filter_df[key_column + ['SCM_PRIORITY', 'BRAND_PRIORITY']]

        # [임시로직] #########################################
        #  가져오는 칼럼은 추후 MASTER_SELLER로 바꿔야 할수도
        scm_master_seller_list = scm_filter_df.loc[(scm_filter_df['SCM_PRIORITY'] == 0) & (scm_filter_df['SCM_PRIORITY'] == 1), 'MASTER_SELLER'].unique()
        #####################################################

        # >> get SELL_AMT_ITEM_MASTER
        # SELL_AMT_SCM_FILTER 의 column이 MASTER로 바뀌고 나면 ITEM_MASTER에서 불러오는 칼럼 이름 변경해야 함!!
        collection = 'SELL_AMT_ITEM_MASTER'
        cursor = mongodb[collection].find({'COLLECT_SITE': collect_site}, {'_id': False})
        item_num_master = pd.DataFrame(list(cursor))
        item_num_master = item_num_master.sort_values('REG_DT')
        item_num_master = item_num_master.drop_duplicates(['COLLECT_SITE', 'ITEM_NUM'], keep = 'last')

        # >> merge SELL_AMT_SCM_FILTER & SELL_AMT_ITEM_MASTER
        item_num_scm_prior = pd.merge(item_num_master, scm_filter_df, on = key_column)
        item_num_scm_prior = item_num_scm_prior[['ITEM_NUM', 'COLLECT_SITE','MASTER_SELLER', 'MASTER_BRAND', 'SCM_PRIORITY', 'BRAND_PRIORITY']]

        # 2. Merge Item & SCM Priority table
        print('len(item):', len(item))
        print('len(item_num_scm_prior):', len(item_num_scm_prior))
        item_scm = pd.merge(item, item_num_scm_prior, on=['COLLECT_SITE', 'ITEM_NUM'], how='left')

        # 3.Mark SCM_FILTER types according to 'SCM_PRIORITY' & 'BRAND_PRIORITY'
        t1_scm_cond = (item_scm['SCM_PRIORITY'] == 0) | (item_scm['SCM_PRIORITY'] == 1)
        t1_brand_cond = (item_scm['BRAND_PRIORITY'] >= 0) & (item_scm['BRAND_PRIORITY'] <= 6)

        if len(item) == len(item_scm):
            item_scm.loc[(t1_scm_cond & t1_brand_cond), 'SCM_FILTER'] = 'T1_FILTER_SLOT'
            t2_scm_cond = (item_scm['SCM_PRIORITY'] == 0) | (item_scm['SCM_PRIORITY'] == 1)
            t2_brand_cond = (item_scm['BRAND_PRIORITY'] == 999)
            t2_temp_cond_1 = item_scm['MASTER_SELLER'].isin(scm_master_seller_list)
            t2_temp_cond_2 = item_scm['SCM_FILTER'].isna()

            item_scm.loc[((t2_scm_cond & t2_brand_cond) | (t2_temp_cond_1&t2_temp_cond_2)), 'SCM_FILTER'] = 'T2_MAX1'
            item_scm['SCM_FILTER'] = item_scm['SCM_FILTER'].fillna('T3_NORMAL')
            print(">> Result")
            print("-------- SCM_FILTER TYPES : ITEM COUNT (before filter) ---------")
            print(item_scm['SCM_FILTER'].value_counts())
            return item_scm
        else:
            print('duplicated values in SELL_AMT_SCM_FILTER. return original item df. ')
            return item
        
    def filterSCMType(self, ivt, item, for_SCM = True):
        print('Filter inventories according to its SCM filter types. ')
        ivt = ivt.reset_index()
        item_temp = item[['ITEM_ID', 'COLLECT_SITE', 'ITEM_NUM', 'TRUST_SELLER', 'BRAND_NAME', 'SCM_FILTER', 'PRICE', 'SITE_PRICE']]

        if for_SCM:
            print('Keep inventory of All types of items.')
            # item_temp = item_temp.loc[item_temp['SCM_FILTER'] == 'T1_FILTER_SLOT']
        else:
            print('Keep inventory of T2, T3 type items.')
            item_temp = item_temp.loc[(item_temp['SCM_FILTER'] == 'T2_MAX1') | (item_temp['SCM_FILTER'] == 'T3_NORMAL')]

        print("-------- SCM_FILTER TYPES : ITEM COUNT(after filter) ---------")
        print(item_temp['SCM_FILTER'].value_counts())
        ivt_items = pd.merge(ivt, item_temp, on='ITEM_ID', how='inner')
        ivt_items = ivt_items.set_index('ID')
        print(len(ivt), '--->', len(ivt_items))
        return ivt_items



    def markPriceRef(self, phis, dc, ivt):
        print("Mark ADD_PRICE_REF")
        '''output will be unique on ['ITEM_ID', 'COLLECT_DAY'] ( not reg_dt level)'''
        # 1. preprocess collect_day format, drop duplicates, and merge both.
        # ivt = ivt.reset_index()
        ivt_org_index = ivt.index
        original_columns = ivt.columns
        phis = phis.assign(COLLECT_DAY=phis['COLLECT_DAY'].apply(lambda x: ''.join(str(x).split('-'))))
        phis = phis.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
        dc = dc.drop_duplicates(['ITEM_ID', 'COLLECT_DAY'], keep='last')
        price = pd.merge(phis[['ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE']],
                         dc[['ITEM_ID', 'COLLECT_DAY', 'DISCOUNT_PRICE']], how='outer', on=['ITEM_ID', 'COLLECT_DAY'])

        # 2. fill 'ADD_PRICE_REF' with DISCOUNT_PRICE >> SITE_PRICE >> PRICE
        price.at[~price.PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.PRICE.isnull()].PRICE
        price.at[~price.SITE_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.SITE_PRICE.isnull()].SITE_PRICE
        price.at[~price.DISCOUNT_PRICE.isnull(), 'ADD_PRICE_REF'] = price[~price.DISCOUNT_PRICE.isnull()].DISCOUNT_PRICE

        # 3. merge price with ivt
        ivt = ivt.merge(price[['ITEM_ID', 'COLLECT_DAY', 'ADD_PRICE_REF']], how='left', on=['ITEM_ID', 'COLLECT_DAY'])
        ivt['ADD_PRICE_REF'].fillna(method='ffill', inplace=True)  # for interpolated day
        ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('str')
        ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].apply(lambda price: ''.join([c for c in price if c.isdigit()]))
        # ivt['ADD_PRICE_REF'] = ivt['ADD_PRICE_REF'].astype('int')
        ivt.index = ivt_org_index
        # ivt = ivt.set_index('ID')
        print("Marked Price Refence. New columns added:")
        print(set(ivt.columns) - set(original_columns))
        return ivt


    def markMasterCate(self, mongo_db, ivt): 
        # Master Cate & Price Range & Diff Max
        client = pymongo.MongoClient('133.186.168.8', 50000)
        mongodb = client[mongo_db.upper()]

        # (1) Mark Master Cate 
        item_id_list = ivt['ITEM_ID'].unique()
        item_id_list = [int(i) for i in item_id_list]
        cursor = mongodb['MASTER_CATE'].find({'ITEM_ID': {'$in': item_id_list}}, {'ITEM_ID': True, 'MASTER_CATE': True, '_id': False})
        master_cate = pd.DataFrame(list(cursor))
        master_cate = master_cate.drop_duplicates(keep = 'last')

        # (2) get ref data from mongo
        cursor = mongodb['SELL_AMT_PRICE_RANGE'].find({})
        master_cate_diff_max = pd.DataFrame(list(cursor))
        master_cate_diff_max = master_cate_diff_max.drop_duplicates(keep = 'last')
        mc_max_price = pd.DataFrame(master_cate_diff_max.groupby('MASTER_CATE')['MAX_PRICE'].apply(lambda x : x.to_list()))
        mc_diff_max = pd.DataFrame(master_cate_diff_max.groupby('MASTER_CATE')['DIFF_MAX'].apply(lambda x : x.to_list()))
        master_cate_diff_max = pd.merge(mc_max_price, mc_diff_max, right_index = True, left_index = True).reset_index()
        master_cate_items = pd.merge(master_cate, master_cate_diff_max, on = 'MASTER_CATE', how = 'left')

        # (3) Set ref 기타 category values
        etc_price = master_cate_diff_max.loc[master_cate_diff_max['MASTER_CATE'] == '기타'].MAX_PRICE.values[0]
        etc_diffmax = master_cate_diff_max.loc[master_cate_diff_max['MASTER_CATE'] == '기타'].DIFF_MAX.values[0]

        # (4) merge with IVT data & fill na with '기타' values
        ivt_cate_diff_max = pd.merge(ivt, master_cate_items, how = 'left', on = 'ITEM_ID')
        ivt_cate_diff_max['MASTER_CATE'] = ivt_cate_diff_max['MASTER_CATE'].fillna('기타')
        ivt_cate_diff_max['MAX_PRICE'] = ivt_cate_diff_max['MAX_PRICE'].apply(lambda x: x if isinstance(x, list) else etc_price)
        ivt_cate_diff_max['DIFF_MAX'] = ivt_cate_diff_max['DIFF_MAX'].apply(lambda x: x if isinstance(x, list) else etc_diffmax)

        return ivt_cate_diff_max


    def changeITEM_ID(self, original_rdb_database, df): 
        print(f'Get Original ITEM_ID of the items FROM {original_rdb_database}')
        # find original ITEM_IDs
        t1_csite_itemnums = df[['COLLECT_SITE', 'ITEM_NUM']].drop_duplicates()
        pqR = pqReader('no_basepath_needed')
        df_full = pd.DataFrame()

        for csite in t1_csite_itemnums['COLLECT_SITE'].unique():
            csite_itemnums = t1_csite_itemnums.loc[
                t1_csite_itemnums['COLLECT_SITE'] == f'{csite}', 'ITEM_NUM'].to_list()
            print("CHANGE ITEM_ID of COLLECT SITE : ", csite, 'LEN(ITEM_NUMS):', len(csite_itemnums))

            rev_item_from_NORMAL = pqR.readItemByNum(original_rdb_database, csite, csite_itemnums)
            rev_item_from_NORMAL = rev_item_from_NORMAL[['COLLECT_SITE', 'ITEM_NUM', 'ITEM_ID']]
            df_orig_item_id = pd.merge(df, rev_item_from_NORMAL, on=['COLLECT_SITE', 'ITEM_NUM'], how='inner')
            new_item_id_count = len(df_orig_item_id['ITEM_ID_y'].unique())
            if len(csite_itemnums) > new_item_id_count:
                print('More than 1 item has no original ITEM_ID')
            else:
                print("All items are in the original DB")

            print(len(csite_itemnums), ' --> ', new_item_id_count)

            del df_orig_item_id['ITEM_ID_x']  # delete REV_ITEM_ID
            df_orig_item_id = df_orig_item_id.rename(columns={'ITEM_ID_y': 'ITEM_ID'})  # change ITEM_ID
            df_full = df_full.append(df_orig_item_id)
            del df_orig_item_id
        return df_full

    def leftDiffToSave(self, diff_df, collect_day_list, min_collect_day_window):
        # ivt를 읽은 대상 기간의 날짜를 가져와서 정렬한다.
        collect_day_list = [i for i in collect_day_list if i.find('-') == -1]
        collect_day_list.sort()

        # # 0) STK_DIFF  == 0 인 애들은 지운다.
        diff_df = diff_df[(diff_df['STK_DIFF'] != 0) & (diff_df['STK_DIFF'] > -1)]
 
        # 1) 읽어온 데이터 중 앞에서부터 min_collect_day_window에 해당하는 날짜의 데이터를 지운다
        # --> 대상 collect_day 의 첫날 diff를 계산하기 위해 min_collect_day_window 데이터까지 가져왔던 것.
        output_collect_day_list = collect_day_list[min_collect_day_window:]       
        result = diff_df[diff_df['COLLECT_DAY'].isin(output_collect_day_list)]
        len_before = len(result)
        print('DIFF is in output_collect_day_list : ', len(diff_df), '-->', len_before)


        # 2) REGDT_DIFF가 NaT인 데이터를 지운다
        # --> 대상 기간 중 수집이 시작된 첫 번째 슬롯은 STK_DIFF == 0이고, 시간 DIFF의 대상도 없기 때문에 지운다.
        result = result[~result['REGDT_DIFF'].isna()]
        result['REGDT_DIFF'] = result['REGDT_DIFF'].astype('int')
        print('DIFF is not na : ', len_before, '-->', len(result))
        len_before = len(result)

        # # 3) REGDT_DIFF가 48시간을 넘어가면 지운다. 
        result = result[result['REGDT_DIFF'] <= min_collect_day_window*24*60*60]
        print(f'REG_DIFF over {min_collect_day_window*24} : ', len_before, '-->', len(result), round(1- (len(result)/len_before),2))
        return result

    def saveMongo(self, mongo_db, diff_df):
        client = pymongo.MongoClient('133.186.168.8', 50000)
        mongodb = client[mongo_db.upper()]
        diff_collect_days = diff_df['COLLECT_DAY'].unique()
        item_id_part = diff_df['ITEM_ID_PART'].unique()[0]
        mongodb['SELL_AMT_DIFF'].delete_many({'ITEM_ID_PART': int(item_id_part), 'COLLECT_DAY': {'$in': list(diff_collect_days)}})
        mongodb['SELL_AMT_DIFF'].insert_many(diff_df.to_dict('records'))
        print('COMPLETED SAVE DIFF UNDER MONGO ::: ',len(diff_df), 'ITEM_ID_PART', int(item_id_part), 'COLLECT_DAY', diff_collect_days)


    def transformToSave(self, diff_df): # will depreciated!!!! 
        # 1) 읽어온 데이터의 첫 번째 날의 데이터를 지운다
        # --> 대상 collect_day 의 첫날 diff를 계산하기 위해 하루 전 데이터까지 가져왔던 것.
        result_collect_day_list = diff_df['COLLECT_DAY'].unique()
        result_collect_day_list.sort()
        output_collect_day_list = result_collect_day_list[1:]
        result = diff_df[diff_df['COLLECT_DAY'].isin(output_collect_day_list)]

        # 2) REGDT_DIFF가 NaT인 데이터를 지운다
        # --> 대상 기간 중 수집이 시작된 첫 번째 슬롯은 STK_DIFF == 0이고, 시간 DIFF의 대상도 없기 때문에 지운다.
        result = result[~result['REGDT_DIFF'].isna()]
        result['REGDT_DIFF'] = result['REGDT_DIFF'].astype('int')

        return result

    def saveParquet(self, source, base_path, df): # will depreciated!!!! 
        output_path = base_path + '/processed/'
        print(f"Save diff data from {source} into {output_path}")
        if df.empty:
            print("df is empty:", df.empty)
            return print('No data to save. Maybe collection error or transfer error occured. ')
        else:
            # 1) make partition column
            df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
            df['COLLECT_SITE_PART'] = df['COLLECT_SITE'].copy()
            output_collect_day_list = df['COLLECT_DAY'].unique()
            print(df['ITEM_ID_PART'].value_counts())
            print(df.groupby(['SCM_FILTER'])['ITEM_ID'].nunique())
            print(df.columns)
            print(df['COLLECT_SITE_PART'].value_counts())
            print('output_collect_day_list : ', output_collect_day_list)
            # 2) set file names
            for ocday in output_collect_day_list:
                print('ocday : ', ocday)
                file_name = output_path + f'DIFF/{source}_diff_{ocday}.parquet.gzip'
                # 3) save
                df = df[final_columns]
                df[df['COLLECT_DAY'] == ocday].to_parquet(fname=file_name, partition_cols=['COLLECT_SITE_PART', 'ITEM_ID_PART'],
                                                          engine='pyarrow', compression='gzip')
    def saveParquet(self, source, base_path, df):  # will depreciated!!!! 
#         final_columns = ['ITEM_ID', 'STOCK_ID',  'COLLECT_SITE', 'ITEM_NUM', 'STK_DIFF', 'REGDT_DIFF', 'TRUST_SELLER',
#                          'BRAND_NAME', 'SCM_FILTER', 'PRICE', 'SITE_PRICE', 'ADD_PRICE', 'ADD_PRICE_REF',
#                          'COLOR_OPTION', 'SIZE_OPTION', 'STYLE_OPTION', 'GIFT_OPTION', 'OPTION', 'STOCK_AMOUNT',
#                          'COLLECT_DAY',  'REG_ID', 'REG_DT',  'COLLECT_SITE_PART', 'ITEM_ID_PART']

        output_path = base_path + '/processed/'
        print(f"Save diff data from {source} into {output_path}")
        if df.empty:
            print("df is empty:", df.empty)
            return print('No data to save. Maybe collection error or transfer error occured. ')
        else:
            # 1) make partition column
            df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
            df['COLLECT_SITE_PART'] = df['COLLECT_SITE'].copy()
            output_collect_day_list = df['COLLECT_DAY'].unique()
            print(df['ITEM_ID_PART'].value_counts())
            print(df.groupby(['SCM_FILTER'])['ITEM_ID'].nunique())
            print(df.columns)
            print(df['COLLECT_SITE_PART'].value_counts())
            print('output_collect_day_list : ', output_collect_day_list)
            # 2) set file names
            
