
import sys
import glob
import os
import gc

import pandas as pd
import numpy as np
from datetime import datetime, timedelta

from sqlalchemy import create_engine
# import pymysql
# pymysql.install_as_MySQLdb()
import pymongo

from pyarrow import parquet as pq 
from multiprocessing import Pool
# import tqdm

path = '/data/sellamt/sell_amount_v2/' # source code path
sys.path.append(path)
from src.data.pqReader import pqReader
from src.preprocess.diffProcessor import diffProcessor, editData
import telepot


def stock_intpr(sdf):
	# 1. ready sdf
	sdf = sdf.sort_values('REG_DT')
	sdf['INTPR'] = False

	# 2. make a dummy sdf having interpolated collect day
	min_collect_day = sdf['COLLECT_DAY'].apply(lambda x: datetime.strptime(x, '%Y%m%d')).min()
	max_collect_day = sdf['COLLECT_DAY'].apply(lambda x: datetime.strptime(x, '%Y%m%d')).max()
	collect_day_intr = pd.DataFrame({'COLLECT_DAY': [datetime.strftime(dt, '%Y%m%d') for dt in list(pd.date_range(min_collect_day, max_collect_day))]})

	# 3. check if the sdf need interpolation
	if len(set(collect_day_intr['COLLECT_DAY'].unique()) - set(sdf['COLLECT_DAY'].unique())) == 0:
		return sdf
	else:
		sdf_dummy = pd.merge(sdf[['ITEM_ID', 'STOCK_ID', 'COLLECT_SITE', 'COLLECT_DAY']].drop_duplicates(), collect_day_intr, how = 'outer').ffill(axis = 0).sort_values('COLLECT_DAY')
		sdf = pd.merge(sdf_dummy, sdf, how = 'left')
		sdf['INTPR'] = sdf['INTPR'].fillna(True)
		del sdf_dummy
		gc.collect()

		# 4. interpolating STK_DIFF & REGDT_DIFF
		non_intpr_index = list(sdf[(sdf['STK_DIFF'].isna() == False)].index)
		shift_non_intpr_index = non_intpr_index[1:] + [0]
		result = sdf[:1]
		for f, t in zip(non_intpr_index, shift_non_intpr_index):
			temp = sdf.loc[f+1:t]
			if temp.empty:
				pass
			else:
				temp_len = len(temp)
				base_stk_diff = temp['STK_DIFF'].values[-1]
				intpr_stk_diff_list = [(base_stk_diff // temp_len)] * temp_len
				intpr_stk_diff_list[-1] = intpr_stk_diff_list[-1] + (base_stk_diff - sum(intpr_stk_diff_list))

				base_regdt_diff = temp['REGDT_DIFF'].values[-1]
				intpr_regdt_diff_list = [(base_regdt_diff // temp_len)] * temp_len
				intpr_regdt_diff_list[-1] = intpr_regdt_diff_list[-1] + (base_regdt_diff - sum(intpr_regdt_diff_list))

				temp = temp.assign(STK_DIFF = intpr_stk_diff_list)
				temp = temp.assign(REGDT_DIFF = intpr_regdt_diff_list)
				#                 temp = temp.assign(REG_DT = intpr_regdt)
				result = result.append(temp)
				del temp
				gc.collect()
		result = result.fillna(method = 'ffill')
		return result

# def cal_sellamt_main(mongo_ref_database, collect_site, sellamt_cal_day, collect_day_window, partition_1_part):
# 	'''
# 	- mongo_ref_database : SELL_AMT와 SELL_AMT_LOG를 저장할 Mongo DB 이름
# 	- collect_site : 판매량 계산의 대상이 되는 데이터 수집 사이트
# 	- sellamt_cal_day : 
# 	- collect_day_window: sellamt_cal_day 외에 판매량을 계산할 날짜 수 (예: 0이면 sellamt_cal_day 당일만 계산, 1이면 sellamt_cal_day 하루 전 판매량부터 계산)  
# 	'''
# 	telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
# 	bot = telepot.Bot(token=telegbot_token)
# 	chat_id = 592874873
# 	msg = f"""[{collect_site}] Start Calculating SELL AMT Module...
# 			>> maximum collect_day : [{sellamt_cal_day}]
# 			>>collect day window : [{collect_day_window}] days
# 			=========================================="""
# 	bot.sendMessage(chat_id=chat_id, text=msg)

# 	start_time = datetime.now()
# 	# --- 
# 	base_path = f'/data/sellamt/data/{mongo_ref_database}'
# 	pqR = pqReader(base_path, sellamt_cal_day, collect_day_window)
# 	# collect_day_list = pqR.makeCollectDaysList()

	
# 	s = datetime.now()
# 	print("START Calculating SELL_AMT", str(s))
# 	# >> Read slope params
# 	print("GET SELL_AMT_THRS_IMP")
# 	host = '133.186.168.8'
# 	port = 50000
# 	client = pymongo.MongoClient(host, port)
# 	mongodb = client[mongo_ref_database.upper()]
# 	collection = 'SELL_AMT_THRS_IMP'
# 	cursor = mongodb[collection].find({'COLLECT_SITE': collect_site}, {'_id': False})
# 	thrs_imp_df_full = pd.DataFrame(list(cursor))
# 	thrs_imp_df_full = thrs_imp_df_full.sort_values('REG_DT')
# 	thrs_imp_df_full = thrs_imp_df_full.drop_duplicates(['COLLECT_SITE', 'ITEM_ID', 'STOCK_ID'], keep = 'last')


# 	## main
# 	for iip in range(0, 19):
# 		print("ITEM_ID_PART: ", iip)
# 		s = datetime.now()
# 		# 1. Read Data
# 		# >> Read diff table
# 		diff = pqR.readDiff(partition_1_part = partition_1_part, id_part = iip, test = True)
# 		if diff.empty:
# 			continue
# 		msg = str(diff.columns)
# 		bot.sendMessage(chat_id=chat_id, text=msg)

# 		diff = diff[diff['STK_DIFF'] >= -1] # except minus STK_DIFF

# 		read_collectday_list = diff['COLLECT_DAY'].unique()
# 		read_collectday_list.sort()

# 		# >> Read thrs_imp values
# 		thrs_imp_df = thrs_imp_df_full[thrs_imp_df_full['ITEM_ID']%19 == iip]

# 		# 2. Stock intorpolatoin
# 		diff['COLLECT_SITE'] = collect_site
# 		gdf_list = [gdf for info, gdf in diff.groupby(['ITEM_ID', 'STOCK_ID'])]
# 		print(f'len(thrs_imp_df) of iip {iip}: ', len(thrs_imp_df))


# 		diff_intpr = diff
# 		# with Pool(10) as p:
# 		# 	diff_intpr_list = p.map(stock_intpr, gdf_list)
# 		# diff_intpr = pd.concat(diff_intpr_list, sort = False)

# 		# 3. merge with thrs_imp
# 		diff_intpr_ti = pd.merge(diff_intpr, thrs_imp_df[['ITEM_ID', 'STOCK_ID', 'COLLECT_SITE',
# 														  'IMP_VALUE_PUH', 'SLOPE_THRS_PUH', 'UNIT_HOUR']],
# 								 on = ['COLLECT_SITE', 'ITEM_ID', 'STOCK_ID'], how = 'inner')
# 		diff_intpr_ti = diff_intpr_ti.assign(SLOPE = diff_intpr_ti['STK_DIFF'] / diff_intpr_ti['REGDT_DIFF'])
# 		diff_intpr_ti = diff_intpr_ti.assign(SLOPE_THRS_PS = diff_intpr_ti['SLOPE_THRS_PUH'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
# 		diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE_PS = diff_intpr_ti['IMP_VALUE_PUH'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
# 		diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE = round(diff_intpr_ti['REGDT_DIFF'] * diff_intpr_ti['IMP_VALUE_PS']))
# 		diff_intpr_ti['INOUT'] = 'IN'
# 		diff_intpr_ti.loc[diff_intpr_ti['SLOPE'] > diff_intpr_ti['SLOPE_THRS_PS'], 'INOUT'] = 'OUT'
# 		# ---
# 		# PRICE
# 		# diff_intpr_ti['PRICE_RANGE_THRS'] = np.nan
# 		# host = '133.186.168.8'
# 		# port = 50000
# 		# client = pymongo.MongoClient(host, port)
# 		# mongodb = client[ref_database.upper()]
# 		# price_range = pd.DataFrame(list(mongodb['SELL_AMT_PRICE_RANGE'].find()))
# 		# for pf in price_range.T.to_dict().items():
# 		# 	min_p = pf[1]['MIN_PRICE']
# 		# 	max_p = pf[1]['MAX_PRICE']
# 		# 	diff_thrs = pf[1]['STK_DIFF_THRS']
# 		# 	idx = diff_intpr_ti[diff_intpr_ti.ADD_PRICE.between(min_p,max_p) & diff_intpr_ti.PRICE_RANGE_THRS.isnull()].index
#   #       ivt.at[idx, 'THRESHOLD'] = pf['threshold']
# 		# 	print(min_p, max_p, diff_thrs)

# 		# # 3. Mark new SCM_FILTER_TYPES
# 		diff_intpr_ti['SCM_FILTER'] = 'T3_NORMAL'
# 		# editD = editData()
# 		# item_num_csite = diff[['COLLECT_SITE', 'ITEM_NUM']].drop_duplicates()
# 		# item_num_csite_scm = editD.markSCMTypes(item=item_num_csite, collect_site=collect_site,
# 		# 										scm_ref_database=mongo_ref_database)
# 		# item_num_csite_scm = item_num_csite_scm[['COLLECT_SITE', 'ITEM_NUM', 'SCM_FILTER']]
# 		# del diff_intpr_ti['SCM_FILTER'] # delete old scm filter.
# 		# diff_intpr_ti = pd.merge(diff_intpr_ti, item_num_csite_scm, on=['COLLECT_SITE', 'ITEM_NUM'], how='left')


# 		# 4. Save into MongoDB : SELL_AMT_LOG
# 		diff_intpr_ti['SOURCE'] = 'ORIGINAL_DB'
			


# 		host = '133.186.168.8'	
# 		port = 50000
# 		client = pymongo.MongoClient(host, port)
# 		mongodb = client[mongo_ref_database.upper()]

# 		collection = 'SELL_AMT_LOG'
# 		# columns = ['ITEM_ID', 'STOCK_ID', 'COLLECT_SITE', 'SOURCE', 'SCM_FILTER', 'STOCK_AMOUNT', 'REG_DT',
# 		# 		   'COLLECT_DAY', 'STK_DIFF', 'REGDT_DIFF', 'SLOPE', 'SLOPE_THRS_PUH', 'IMP_VALUE_PUH', 'SLOPE_THRS_PS',
# 		# 		   'IMP_VALUE_PS', 'INTPR', 'INOUT', 'IMP_VALUE', 'LOG_REG_DT', 'LOG_REG_ID']
# 		columns = ['ITEM_ID', 'STOCK_ID', 'COLLECT_SITE', 'ADD_PRICE', 'ADD_PRICE_REF','SOURCE', 'SCM_FILTER', 'STOCK_AMOUNT', 'REG_DT',
# 				   'COLLECT_DAY', 'STK_DIFF', 'REGDT_DIFF', 'SLOPE', 'SLOPE_THRS_PUH', 'IMP_VALUE_PUH', 'SLOPE_THRS_PS',
# 				   'IMP_VALUE_PS', 'INTPR', 'INOUT', 'IMP_VALUE', 'LOG_REG_DT', 'LOG_REG_ID']
		
# 		diff_intpr_ti['LOG_REG_DT'] = datetime.now()
# 		diff_intpr_ti['LOG_REG_ID'] = 'SELL_AMT_V2'
# 		# diff_intpr_ti[columns].to_pickle(f'sellamt_log_{mongo_ref_database}_{sellamt_cal_day}_{collect_day_window}_{iip}.pickle')
# 		mongodb[collection].insert_many(diff_intpr_ti[columns].to_dict('records'))


# 		# 4. Save into MongoDB : SELL_AMT
# 		collection = 'SELL_AMT'

# 		# if scm_filter type is t2, edit STK_DIFF at most Max 1
# 		# t2_cond = diff_intpr_ti['SCM_FILTER'] == 'T2_MAX1'
# 		# diff_intpr_ti.loc[t2_cond, 'STK_DIFF'] = diff_intpr_ti.loc[t2_cond, 'STK_DIFF'].apply(lambda x: min(x, 1))

# 		# groupby sum!
# 		diff_intpr_ti = diff_intpr_ti.loc[diff_intpr_ti['INOUT'] != 'OUT']
# 		# diff_intpr_ti.loc[diff_intpr_ti['INOUT'] == 'OUT', 'STK_DIFF'] = diff_intpr_ti.loc[diff_intpr_ti['INOUT'] == 'OUT', 'IMP_VALUE']
# 		sellamt = diff_intpr_ti.groupby(['ITEM_ID', 'STOCK_ID','COLLECT_DAY'])['STK_DIFF'].sum().reset_index()
# 		sellamt = sellamt[sellamt['STK_DIFF'] != 0]
# 		if sellamt.empty:
# 			continue
# 		sellamt['REG_DT'] = datetime.now()
# 		sellamt['REG_ID'] = 'SELL_AMT_V2'

# 		print('STK_DIFF TOP 10 stocks')
# 		print(sellamt[['ITEM_ID','STOCK_ID', 'COLLECT_DAY', 'STK_DIFF']].sort_values('STK_DIFF', ascending = False)[:10])
# 		sellamt.to_pickle(f'/data/sellamt/data/sellamt_{mongo_ref_database}_{sellamt_cal_day}_{collect_day_window}_{iip}.pickle')
# 		mongodb[collection].insert_many(sellamt.to_dict('records'))
# 		print("="*50)

# 		msg = f"""[{collect_site}] CAL SELLAMT
# 		COMPLETED ITEM_ID_PART == {iip}
# 		TIME SPENT : {str(datetime.now() - s)}
# 		=========================================="""
# 		bot.sendMessage(chat_id=chat_id, text=msg)
# 		print(iip, "TIME SPENT", str(datetime.now() - s))

# 	msg = f"""[[{collect_site}]] CAL SELLAMT
# 	Calculating SELL AMT Module Finished!!!!! 
# 	TIME SPENT : {str(datetime.now() - s)}
# 	=========================================="""
# 	bot.sendMessage(chat_id=chat_id, text=msg)

# def markMasterCate(mongo_db, ivt): 
#     # Master Cate & Price Range & Diff Max
#     client = pymongo.MongoClient('133.186.168.8', 50000)
#     mongodb = client[mongo_db.upper()]

#     # (1) Mark Master Cate 
#     item_id_list = ivt['ITEM_ID'].unique()
#     item_id_list = [int(i) for i in item_id_list]
#     cursor = mongodb['MASTER_CATE'].find({'ITEM_ID': {'$in': item_id_list}}, {'ITEM_ID': True, 'MASTER_CATE': True, '_id': False})
#     master_cate = pd.DataFrame(list(cursor))
#     master_cate = master_cate.drop_duplicates(keep = 'last')

#     # (2) get ref data from mongo
#     cursor = mongodb['SELL_AMT_PRICE_RANGE'].find({})
#     master_cate_diff_max = pd.DataFrame(list(cursor))
#     master_cate_diff_max = master_cate_diff_max.drop_duplicates(keep = 'last')
#     mc_max_price = pd.DataFrame(master_cate_diff_max.groupby('MASTER_CATE')['MAX_PRICE'].apply(lambda x : x.to_list()))
#     mc_diff_max = pd.DataFrame(master_cate_diff_max.groupby('MASTER_CATE')['DIFF_MAX'].apply(lambda x : x.to_list()))
#     master_cate_diff_max = pd.merge(mc_max_price, mc_diff_max, right_index = True, left_index = True).reset_index()
#     master_cate_items = pd.merge(master_cate, master_cate_diff_max, on = 'MASTER_CATE', how = 'left')

#     # (3) Set ref 기타 category values
#     etc_price = master_cate_diff_max.loc[master_cate_diff_max['MASTER_CATE'] == '기타'].MAX_PRICE.values[0]
#     etc_diffmax = master_cate_diff_max.loc[master_cate_diff_max['MASTER_CATE'] == '기타'].DIFF_MAX.values[0]

#     # (4) merge with IVT data & fill na with '기타' values
#     ivt_cate_diff_max = pd.merge(ivt, master_cate_items, how = 'left', on = 'ITEM_ID')
#     ivt_cate_diff_max['MASTER_CATE'] = ivt_cate_diff_max['MASTER_CATE'].fillna('기타')
#     ivt_cate_diff_max['MAX_PRICE'] = ivt_cate_diff_max['MAX_PRICE'].apply(lambda x: x if isinstance(x, list) else etc_price)
#     ivt_cate_diff_max['DIFF_MAX'] = ivt_cate_diff_max['DIFF_MAX'].apply(lambda x: x if isinstance(x, list) else etc_diffmax)

#     return ivt_cate_diff_max

def check_inout(record):
    deal = record['DEAL_PERIOD']
    if deal:
        return record['SLOPE'] > record['SLOPE_THRS_PS_DEAL']

    else: 
        return record['SLOPE'] > record['SLOPE_THRS_PS']

def cal_sellamt_main_test(mongo_ref_database, collect_site, sellamt_cal_day, collect_day_window, partition_1_part, test = False):
	'''
	- mongo_ref_database : SELL_AMT와 SELL_AMT_LOG를 저장할 Mongo DB 이름
	- collect_site : 판매량 계산의 대상이 되는 데이터 수집 사이트
	- sellamt_cal_day : 
	- collect_day_window: sellamt_cal_day 외에 판매량을 계산할 날짜 수 (예: 0이면 sellamt_cal_day 당일만 계산, 1이면 sellamt_cal_day 하루 전 판매량부터 계산)  
	'''
	telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
	bot = telepot.Bot(token=telegbot_token)
	chat_id = 592874873
	msg = f"""[{collect_site}] Start Calculating SELL AMT Module...
			>> maximum collect_day : [{sellamt_cal_day}]
			>>collect day window : [{collect_day_window}] days
			=========================================="""
	bot.sendMessage(chat_id=chat_id, text=msg)

	start_time = datetime.now()
	# --- 
	base_path = f'/data/sellamt/data/{mongo_ref_database}'
	# pqR = pqReader(base_path, sellamt_cal_day, collect_day_window)

	s = datetime.now()
	print("START Calculating SELL_AMT", str(s))
	# >> Read slope params
	print("GET SELL_AMT_THRS_IMP")
	host = '133.186.168.8'
	port = 50000
	client = pymongo.MongoClient(host, port)
	mongodb = client[mongo_ref_database.upper()]


	# ## main
	# for iip in range(0, 19):
	# print("ITEM_ID_PART: ", iip)
	s = datetime.now()
	# 1. Read Data
	# >> Read diff table
	sellamt_cal_start_day = datetime.strftime(datetime.strptime(sellamt_cal_day, '%Y%m%d') - timedelta(days=collect_day_window), '%Y%m%d')
	diff = pd.DataFrame(client[mongo_ref_database]['SELL_AMT_DIFF'].find({'COLLECT_DAY': {'$gte': sellamt_cal_start_day}}))
	del diff['_id']
	diff = diff.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT'], keep = 'last')
	# diff = pqR.readDiff(partition_1_part = partition_1_part, id_part = iip, test = True)

	if diff.empty:
		print('No Data Read from Mongo DB ')
		return 

	# 0. Filter Out diff values
	diff = diff[diff['STK_DIFF'] >= -1] # except minus STK_DIFF
	if diff.empty:
		print('No diff[STK_DIFF] >= -1]')
		return

	# 2. Stock intorpolatoin
	diff['COLLECT_SITE'] = collect_site
	diff_intpr = diff
	# ======== FIX STK_INTPR ========================================================================
	# gdf_list = [gdf for info, gdf in diff.groupby(['ITEM_ID', 'STOCK_ID'])]

	# with Pool(10) as p:
	# 	diff_intpr_list = p.map(stock_intpr, gdf_list)
	# diff_intpr = pd.concat(diff_intpr_list, sort = False)
	# ======== FIX STK_INTPR ========================================================================

	# 3. merge with thrs_imp
	# >> Read thrs_imp values
	item_id_list = diff_intpr['ITEM_ID'].unique()
	item_id_list = [int(i) for i in item_id_list]
	collection = 'SELL_AMT_THRS_IMP'
	cursor = mongodb[collection].find({'COLLECT_SITE': collect_site, 'REG_ID' : 'TEST', 'ITEM_ID': {'$in': item_id_list}}, {'_id': False})
	thrs_imp_df = pd.DataFrame(list(cursor))
	thrs_imp_df = thrs_imp_df.sort_values('REG_DT')
	thrs_imp_df = thrs_imp_df.drop_duplicates(['COLLECT_SITE', 'ITEM_ID', 'STOCK_ID'], keep = 'last')
	thrs_imp_df['SLOPE_THRS_PUH'] = thrs_imp_df['SLOPE_THRS_PUH'].fillna(40)
	thrs_imp_df['SLOPE_THRS_PUH_DEAL'] = thrs_imp_df['SLOPE_THRS_PUH_DEAL'].fillna(500)
	
	# 과거에 min_imp_thrs 잘못 되어있던 애들 수정
	thrs_imp_df.loc[thrs_imp_df['SLOPE_THRS_PUH'] == 12, 'SLOPE_THRS_PUH'] = 40
	thrs_imp_df.loc[thrs_imp_df['SLOPE_THRS_PUH'] == 60, 'SLOPE_THRS_PUH'] = 40
	thrs_imp_df.loc[thrs_imp_df['SLOPE_THRS_PUH_DEAL'] == 1200, 'SLOPE_THRS_PUH_DEAL'] = 500

	# merge with thrs_imp
	diff_intpr_ti = pd.merge(diff_intpr, thrs_imp_df[['ITEM_ID', 'STOCK_ID', 'COLLECT_SITE',
													  'IMP_VALUE_PUH', 'SLOPE_THRS_PUH', 'IMP_VALUE_PUH_DEAL', 'SLOPE_THRS_PUH_DEAL', 'UNIT_HOUR']],
							 on = ['COLLECT_SITE', 'ITEM_ID', 'STOCK_ID'], how = 'inner')

	# >> Read ITEM MASTER CATE
	diff_intpr_ti = markMasterCate(mongo_ref_database, diff_intpr_ti)


	# 4. OUTLIER DETECTION
	diff_intpr_ti = diff_intpr_ti.assign(SLOPE = diff_intpr_ti['STK_DIFF'] / diff_intpr_ti['REGDT_DIFF'])
	diff_intpr_ti = diff_intpr_ti.assign(SLOPE_THRS_PS = diff_intpr_ti['SLOPE_THRS_PUH'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
	diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE_PS = diff_intpr_ti['IMP_VALUE_PUH'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
	diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE = round(diff_intpr_ti['REGDT_DIFF'] * diff_intpr_ti['IMP_VALUE_PS']))

	diff_intpr_ti = diff_intpr_ti.assign(SLOPE_THRS_PS_DEAL = diff_intpr_ti['SLOPE_THRS_PUH_DEAL'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
	diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE_PS_DEAL = diff_intpr_ti['SLOPE_THRS_PUH_DEAL'] / (diff_intpr_ti['UNIT_HOUR']*60*60))
	diff_intpr_ti = diff_intpr_ti.assign(IMP_VALUE_DEAL = round(diff_intpr_ti['REGDT_DIFF'] * diff_intpr_ti['IMP_VALUE_PS']))


	diff_intpr_ti['DEAL_PERIOD'] = diff_intpr_ti['REGDT_DIFF'] < 4*60*60
	diff_intpr_ti['OUT'] = diff_intpr_ti.apply(check_inout, axis = 1)

	diff_intpr_ti['INOUT'] = 'IN'
	diff_intpr_ti['OUT_REASON'] = 'IN'
	diff_intpr_ti.loc[(diff_intpr_ti['OUT']), 'INOUT'] = 'OUT'
	diff_intpr_ti.loc[(diff_intpr_ti['DEAL_PERIOD']) & (diff_intpr_ti['OUT']), 'OUT_REASON'] = 'SLOPE_DEAL'
	diff_intpr_ti.loc[~(diff_intpr_ti['DEAL_PERIOD']) & (diff_intpr_ti['OUT']), 'OUT_REASON'] = 'SLOPE'

	# ---
	# PRICE
	diff_intpr_ti['ADD_PRICE_REF'] = diff_intpr_ti['ADD_PRICE_REF'].astype('int')
	diff_intpr_ti['ADD_PRICE_MIN'] = diff_intpr_ti.apply(lambda x: min(x['ADD_PRICE'], x['ADD_PRICE_REF']), axis = 1)
	diff_intpr_ti['DIFF_MAX_INDEX'] = diff_intpr_ti.apply(lambda x: np.searchsorted(x['MAX_PRICE'], x['ADD_PRICE_MIN'], 'left'), axis = 1)
	diff_intpr_ti['DIFF_MAX_INDEX']  = diff_intpr_ti.apply(lambda x: x['DIFF_MAX_INDEX'] if x['DIFF_MAX_INDEX'] < len(x['MAX_PRICE']) else len(x['MAX_PRICE'])-1, axis = 1)
	diff_intpr_ti['PRICE_DIFF_MAX'] = diff_intpr_ti.apply(lambda x: x['DIFF_MAX'][x['DIFF_MAX_INDEX']], axis = 1)
	diff_intpr_ti = diff_intpr_ti.assign(OUT_REASON = diff_intpr_ti.apply(lambda x: 'PRICE' if (x['STK_DIFF'] > x['PRICE_DIFF_MAX']) & (x['INOUT'] == 'IN') & (x['DEAL_PERIOD'] == False) else x['OUT_REASON'], axis = 1))
	diff_intpr_ti = diff_intpr_ti.assign(INOUT = diff_intpr_ti.apply(lambda x: 'OUT' if (x['STK_DIFF'] > x['PRICE_DIFF_MAX']) & (x['INOUT'] == 'IN') & (x['DEAL_PERIOD'] == False)  else x['INOUT'], axis = 1))
	# diff_intpr_ti = diff_intpr_ti.assign(OUT_REASON = diff_intpr_ti.apply(lambda x: 'PRICE' if (x['STK_DIFF'] > x['PRICE_DIFF_MAX']) & (x['INOUT'] == 'IN') else x['OUT_REASON'], axis = 1))
	# diff_intpr_ti = diff_intpr_ti.assign(INOUT = diff_intpr_ti.apply(lambda x: 'IN' if (x['STK_DIFF'] <= x['PRICE_DIFF_MAX']) & (x['INOUT'] == 'IN') else 'OUT', axis = 1))


	# # 3. Mark new SCM_FILTER_TYPES
	diff_intpr_ti['SCM_FILTER'] = 'T3_NORMAL'

	# 4. Save into MongoDB : SELL_AMT_LOG
	diff_intpr_ti['SOURCE'] = 'ORIGINAL_DB'
	host = '133.186.168.8'	
	port = 50000
	client = pymongo.MongoClient(host, port)
	mongodb = client[mongo_ref_database.upper()]

	collection = 'SELL_AMT_LOG'

	diff_intpr_ti['LOG_REG_DT'] = datetime.now()
	if test: 
		diff_intpr_ti['LOG_REG_ID'] = 'TEST'
	else: 
		diff_intpr_ti['LOG_REG_ID'] = 'SELL_AMT_V2'

	mongodb[collection].insert_many(diff_intpr_ti.to_dict('records'))


	if not test:
		# 4. Save into MongoDB : SELL_AMT (if test, do not insert into SELL_AMT collection. )
		collection = 'SELL_AMT'

		# groupby sum!
		diff_intpr_ti = diff_intpr_ti.loc[diff_intpr_ti['INOUT'] != 'OUT']
		sellamt = diff_intpr_ti.groupby(['ITEM_ID', 'STOCK_ID','COLLECT_DAY'])['STK_DIFF'].sum().reset_index()
		sellamt = sellamt[sellamt['STK_DIFF'] != 0] 
		if sellamt.empty:
			print('NO STK_DIFF != 0')
			return 
		sellamt['REG_DT'] = datetime.now()
		sellamt['REG_ID'] = 'SELL_AMT_V2'

		# sellamt.to_pickle(f'/data/sellamt/data/sellamt_{mongo_ref_database}_{sellamt_cal_day}_{collect_day_window}.pickle')
			

		print('STK_DIFF TOP 10 stocks')
		print(sellamt[['ITEM_ID','STOCK_ID', 'COLLECT_DAY', 'STK_DIFF']].sort_values('STK_DIFF', ascending = False)[:10])

		print("DELETE SELL_AMT records from MONGO DB")
		sellamt_collect_days = sellamt['COLLECT_DAY'].unique()
		mongodb[collection].delete_many({'COLLECT_DAY': {'$in': list(sellamt_collect_days)}})
		print("INSERT NEW SELL_AMT records into MONGO DB")

		mongodb[collection].insert_many(sellamt.to_dict('records'))
		print("="*50)


		msg = f"""[[{collect_site}]] CAL SELLAMT
		Calculating SELL AMT Module Finished!!!!! 
		TIME SPENT : {str(datetime.now() - s)}
		=========================================="""
		bot.sendMessage(chat_id=chat_id, text=msg)



def insert_sellamt_table(params):
	output_rdb, sellamt_output_table, sell_amt_mongo_part = params
	if sell_amt_mongo_part.empty:
		print('no data to insert')
		return
	# engine = create_engine(output_rdb, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
	engine = create_engine(output_rdb)
	item_id_tuple = tuple([int(i) for i in sell_amt_mongo_part['ITEM_ID'].unique()])
	first_day = sell_amt_mongo_part['COLLECT_DAY'].min()
	query = f"DELETE FROM {sellamt_output_table} WHERE (ITEM_ID in {item_id_tuple}) and (COLLECT_DAY ='{first_day}');"
	engine.execute(query)

	query = f"INSERT INTO {sellamt_output_table} (ITEM_ID, STOCK_ID, COLLECT_DAY, SELL_AMOUNT, REG_ID, REG_DT, UPT_ID, UPT_DT) VALUES %s ON DUPLICATE KEY UPDATE SELL_AMOUNT=%s, UPT_ID=%s, UPT_DT=%s;"
	values = [(tuple(x for x in tuple(t)), t[3], t[4], t[5]) for t in sell_amt_mongo_part.values]
	engine.execute(query, values)
	retry_count = 100
	fail = True
	print(f'Inserted a part of sellamt into {sellamt_output_table}, {len(sell_amt_mongo_part)}')
	engine.execute(query, values)
	while(retry_count > 0):
		try:
			print(f'Inserted a part of sellamt into {sellamt_output_table}, {len(sell_amt_mongo_part)}')
			engine.execute(query, values)
			retry_count = 0
			fail = False
		except:
			retry_count -= 1
	if fail:
		print(f' error inserting exec plan after 5 times retrying ... ')
		print('ERROR occured during inserting sell amt of there ITEM_ID')
		print(sell_amt_mongo_part['ITEM_ID'].unique())
	engine.dispose()
	

def write_rdb_main(input_mongo_db, collect_site, sellamt_collect_day, output_rdb, sellamt_output_table, test = False):
	"""
	database insert of computed sell amount
	"""
	s = datetime.now()
	telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
	bot = telepot.Bot(token=telegbot_token)
	chat_id = 592874873
	bot.sendMessage(chat_id = chat_id, text=f"""[{collect_site}] Start Write RDB module...{sellamt_collect_day}""")
	# ------
	# Read SELLAMT from Mongo
	host = '133.186.168.8'
	port = 50000
	client = pymongo.MongoClient(host, port)
	mongodb = client[input_mongo_db.upper()]

	if test: 
		cursor = mongodb['SELL_AMT'].find({'COLLECT_DAY': sellamt_collect_day, 'REG_ID': 'TEST'}, {'_id': False})
	else:
		cursor = mongodb['SELL_AMT'].find({'COLLECT_DAY': sellamt_collect_day, 'REG_ID':'SELL_AMT_V2'}, {'_id': False})
	sell_amt_mongo = pd.DataFrame(list(cursor))
	print(sell_amt_mongo)

	# sell_amt_mongo = pd.DataFrame()
	# for iip in range(0,19):
	# 	# '/data/sellamt/data/{mongo_ref_database}/sellamt_{mongo_ref_database}_{sellamt_cal_day}_{collect_day_window}_{iip}.pickle'
	# 	sell_amt_mongo_temp = pd.read_pickle(f'/data/sellamt/data/{input_mongo_db}/sellamt_{input_mongo_db}_2019-08-11_9_{iip}.pickle')
	# 	print('len(sell_amt_mongo_temp)', len(sell_amt_mongo_temp))
	# 	sell_amt_mongo_temp = sell_amt_mongo_temp[sell_amt_mongo_temp['COLLECT_DAY'] == sellamt_collect_day]
	# 	print(sellamt_collect_day, 'filtered. len(sell_amt_mongo_temp)', len(sell_amt_mongo_temp))
	# 	sell_amt_mongo = sell_amt_mongo.append(sell_amt_mongo_temp)
	# 	del sell_amt_mongo_temp
	# 	gc.collect()

	print(sell_amt_mongo['COLLECT_DAY'].value_counts())


	sell_amt_mongo = sell_amt_mongo.sort_values('REG_DT')
	sell_amt_mongo = sell_amt_mongo.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'COLLECT_DAY'], keep = 'last')
	sell_amt_mongo = sell_amt_mongo[sell_amt_mongo['REG_DT'] >= str(sell_amt_mongo['REG_DT'].max())[:10]]
	print(sellamt_collect_day, len(sell_amt_mongo))
	if sell_amt_mongo.empty:
		msg = f"""[{collect_site}] Write SELLAMT to RDB
		>> COLLECT_DAY : {sellamt_collect_day}
		No data to insert RDB. Check MongoDB SELL_AMT collection"""
		bot.sendMessage(chat_id = chat_id, text=msg)
		return

	# if sellamt data is not empty, insert into RDB
	msg = f"""[{collect_site}] Write SELLAMT to RDB
	>> COLLECT_DAY : {sellamt_collect_day}
	>> SELLAMT SHAPE = {sell_amt_mongo.shape}
	>> SELLAMT SUM = {sell_amt_mongo['STK_DIFF'].sum()}"""
	print(msg)
	bot.sendMessage(chat_id = chat_id, text=msg)

	# transfrom to save
	sell_amt_mongo = sell_amt_mongo.rename(columns = {'STK_DIFF': 'SELL_AMOUNT'})
	sell_amt_mongo['REG_ID'] = 'SELL_AMT_V2.0'
	sell_amt_mongo['REG_DT'] = sell_amt_mongo['REG_DT'].apply(lambda x: str(x))
	sell_amt_mongo['UPT_ID'] = sell_amt_mongo['REG_ID'].copy()
	sell_amt_mongo['UPT_DT'] = sell_amt_mongo['REG_DT'].copy()
	# sell_amt_mongo['COLLECT_DAY'] = sell_amt_mongo['COLLECT_DAY'].apply(lambda x : str(datetime.strptime(x, '%Y%m%d'))[:10])
	print('sell_amt_mongo.columns', sell_amt_mongo.columns)
	sell_amt_mongo = sell_amt_mongo[['ITEM_ID', 'STOCK_ID', 'COLLECT_DAY', 'SELL_AMOUNT', 'REG_ID', 'REG_DT', 'UPT_ID', 'UPT_DT']]
	print('sell_amt_mongo', sell_amt_mongo.iloc[0])
	# split data & insert into db
	len_sell_amt = len(sell_amt_mongo)
	chunk_size = 100
	split_list = [(output_rdb, sellamt_output_table, sell_amt_mongo[j*chunk_size:j*chunk_size+chunk_size]) for j in range((len_sell_amt//chunk_size)+1)]
	insert_sellamt_table((output_rdb, sellamt_output_table, sell_amt_mongo))
	# with Pool(20) as p:
	# 	p.map(insert_sellamt_table, split_list)

	bot.sendMessage(chat_id = chat_id, text=f"""[{collect_site}] Finished writing SELLAMT to RDB
		TIME SPENT : {str(datetime.now() - s)} """)