# DB related
from sqlalchemy import create_engine

# Data Wrangling related
from datetime import datetime, timedelta
import pandas as pd
import numpy as np

from pyarrow import parquet as pq
import gc
import glob

class pqReader():
    def __init__(self, base_path, sellamt_cal_day, collect_day_window):
        self.base_path = base_path
        self.sellamt_cal_day = sellamt_cal_day
        self.collect_day_window = collect_day_window
        self.setCollectDaysList() # set 'self.collect_day_list'
    #     setToday() # set 'self.today' 

    # def setToday(self):
    #   self.today = datetime.strftime(datetime.now(), '%Y-%m-%d')

    def setCollectDaysList(self):
        # set 'self.collect_day_list' & 'self.today' 
        self.collect_day_start = datetime.strptime(self.sellamt_cal_day, '%Y-%m-%d') - timedelta(days=self.collect_day_window)
        self.collect_day_list_t1 = pd.date_range(self.collect_day_start, periods=self.collect_day_window + 1).tolist()
        self.collect_day_list_t1 = [datetime.strftime(i, '%Y-%m-%d') for i in self.collect_day_list_t1]
        collect_day_list_two_types = self.collect_day_list_t1 + [i.replace('-', '') for i in self.collect_day_list_t1]
        collect_day_list_two_types.sort()
        self.collect_day_list = collect_day_list_two_types
        print(self.collect_day_list)
        
    def readRaw(self, rdb_table, partition_1_part, id_part, selected_collect_day_list=None):
        # Read history-type data from RDB (saved by ID column)
        if selected_collect_day_list:
            self.collect_day_list = selected_collect_day_list

        rdb_table_part_path = self.base_path + f'/raw/{rdb_table}/*'
        files = glob.glob(rdb_table_part_path + f'/*={partition_1_part}/ITEM_ID_PART={id_part}/*')
        files_selected = [f for f in files if f.split(".parquet.gzip")[0].split("_")[-1] in self.collect_day_list]
        df_full = pd.DataFrame()
        for i in files_selected:
            df_temp = pd.read_parquet(i)
            df_full = df_full.append(df_temp, sort=False)
            del df_temp
            gc.collect()
        df_full = df_full.drop_duplicates()
        if df_full.empty:
            print('No data to read.. check saved files')
        else:
            read_collectday_list = df_full['COLLECT_DAY'].unique()
            print(f'Read {rdb_table} & First partition = {partition_1_part} & ITEM_ID part = {id_part}')
            print('collect day in ', read_collectday_list)
        return df_full


    def readDiff(self, partition_1_part, id_part, selected_collect_day_list=None, test = False):
        # Read history-type data from RDB (saved by ID column)
        if selected_collect_day_list:
            self.collect_day_list = selected_collect_day_list
            
        if not test:
            rdb_table_part_path = self.base_path + f'/processed/DIFF/*/'
        else: 
            rdb_table_part_path = self.base_path + f'/processed_test/DIFF/*/'

        files = glob.glob(rdb_table_part_path + f'/*={partition_1_part}/ITEM_ID_PART={id_part}/*')
        files_selected = [f for f in files if f.split(".parquet.gzip")[0].split("_")[-1] in self.collect_day_list]
        df_full = pd.DataFrame()
        for i in files_selected:
            df_temp = pd.read_parquet(i)
            df_full = df_full.append(df_temp, sort=False)
            del df_temp
            gc.collect()
        df_full = df_full.drop_duplicates(['ITEM_ID', 'STOCK_ID', 'REG_DT'], keep = "last")
        if df_full.empty:
            print('No data to read.. check saved files')
        else:
            read_collectday_list = df_full['COLLECT_DAY'].unique()
            print(f'Read from {self.base_path}: First partition = {partition_1_part} & ITEM_ID part = {id_part}')
            print('collect day in ', read_collectday_list, len(df_full))
        return df_full





if __name__ == '__main__':
	import pymysql
	pymysql.install_as_MySQLdb()