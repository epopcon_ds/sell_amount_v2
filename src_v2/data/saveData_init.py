from multiprocessing import Pool
import glob
import sys
import gc
import pprint
import os

# DB related
import pymongo
from sqlalchemy import create_engine
# import pymysql
# pymysql.install_as_MySQLdb()

# Data handling related
from datetime import datetime, timedelta
import pandas as pd
import numpy as np

import telepot


def saveFirstHistData(db, table, save_path, prefix, new_min_id):
    engine = create_engine(db, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
    new_max_id = new_min_id + 100000
    new_id_list = list(range(new_min_id,new_max_id))
    print(f'query id between {min(new_id_list)} and {max(new_id_list)}')
    
    # should query at most 100000 records. 
    chunk_size = 100001
    chunk_count = round(len(new_id_list) / chunk_size) + 1
    splited_idlist = list(np.array_split(new_id_list, chunk_count))
    minmax_splited_idlist = [[i.min(), i.max(), len(i)] for i in splited_idlist]
    
    
    for idx, i in enumerate(minmax_splited_idlist):
        s = datetime.now()
        print(idx, '/', len(minmax_splited_idlist), i[0], i[1])
        df = pd.read_sql(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}", engine)
        # df = df[df['REG_DT'] <= self.sellamt_cal_day]

        if df.empty: 
            # print(f'empty df. Maybe REG_DT over the sellamt_cal_day: {self.sellamt_cal_day}')
            print(f'Empty df. Finished Reading [{table}] Data From RDB ')
            break

        else: 
            df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
           
            item_id_list = df['ITEM_ID'].unique()
            item = pd.read_sql(f"SELECT ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, COLLECT_URL, TRUST_SELLER, BRAND_NAME, ' \
                f'ORG_GOODS_NUM, PRICE, SITE_PRICE, REG_DT, UPT_DT FROM MWS_COLT_ITEM WHERE ID IN {tuple(item_id_list)}", engine)
            item['ITEM_ID_PART'] = item['ITEM_ID'] % 19
            item['COLLECT_SITE_PART'] = item['COLLECT_SITE'].copy()
            today = str(datetime.now())[:10]
            item.to_parquet(save_path + f'/MWS_COLT_ITEM/item_{today}.parquet.gzip', compression='gzip', partition_cols = ['COLLECT_SITE_PART', 'ITEM_ID_PART'])
            # item.to_pickle(base_path + f'/raw/MWS_COLT_ITEM/item_{uptdt_from}_{today}.pickle')
            

            df = pd.merge(df, item[['ITEM_ID', 'COLLECT_SITE']], on = 'ITEM_ID', how = 'left')
            df = df.rename(columns = {'COLLECT_SITE': 'COLLECT_SITE_PART'})
            # df['COLLECT_SITE_PART'] = df['COLLECT_SITE'].copy()

            collect_days = df['COLLECT_DAY'].unique()
            for cday in collect_days:
                df[df['COLLECT_DAY'] == cday].to_parquet(save_path + f'/{table}/{prefix}_{cday}.parquet.gzip', compression='gzip', partition_cols = ['COLLECT_SITE_PART', 'ITEM_ID_PART'])
            print(datetime.now()-s)



def saveFirstHistDataGFK(db, table, save_path, prefix, new_min_id):
    new_max_id = new_min_id + 5000
    new_id_list = list(range(new_min_id,new_max_id))
    print(f'query id between {min(new_id_list)} and {max(new_id_list)}')
    
    # should query at most 100000 records. 
    chunk_size = 5001
    chunk_count = round(len(new_id_list) / chunk_size) + 1
    splited_idlist = list(np.array_split(new_id_list, chunk_count))
    minmax_splited_idlist = [[i.min(), i.max(), len(i)] for i in splited_idlist]
    
    
    for idx, i in enumerate(minmax_splited_idlist):
        s = datetime.now()
        engine = create_engine(db, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
        # engine = create_engine(db, pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
        print(idx, '/', len(minmax_splited_idlist), i[0], i[1])
        print(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}")
        df = pd.read_sql_query(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}", engine)

        if df.empty: 
            # print(f'empty df. Maybe REG_DT over the sellamt_cal_day: {self.sellamt_cal_day}')
            print(f'Empty df. Finished Reading [{table}] Data From RDB ')
            break

        else: 
            item_id_list = df['ITEM_ID'].unique()
            item = pd.read_sql_query(f"SELECT ID AS ITEM_ID, COLLECT_SITE FROM MWS_COLT_ITEM WHERE ID IN {tuple(item_id_list)}", engine)
            item['ITEM_ID_PART'] = item['ITEM_ID'] % 19
            item['COLLECT_SITE_PART'] = item['COLLECT_SITE']
            # item['GOODS_CATE_PART'] = item['GOODS_CATE'].apply(lambda x :'non_golf' if x.find('골프')==-1 else 'golf')
            
            df = pd.merge(df, item[['ITEM_ID', 'ITEM_ID_PART', 'COLLECT_SITE_PART']], on = 'ITEM_ID')
            
            collect_days = df['COLLECT_DAY'].unique()
            for cday in collect_days:
                df[df['COLLECT_DAY'] == cday].to_parquet(save_path + f'/{table}/{prefix}_{cday}.parquet.gzip', compression='gzip', partition_cols = ['COLLECT_SITE_PART', 'ITEM_ID_PART'])
            print(datetime.now()-s)



def saveFirstHistDataTop50(db, table, save_path, prefix, new_min_id):
    new_max_id = new_min_id + 5000
    s = datetime.now()
    engine = create_engine(db, encoding='utf8', pool_size=50, pool_recycle=3600, connect_args={'connect_timeout':1000000})
    print(f"SELECT * FROM {table} WHERE ID ID <= {new_max_id}")
    df = pd.read_sql_query(f"SELECT * FROM {table} WHERE ID <= {new_max_id}", engine)

    if df.empty: 
        # print(f'empty df. Maybe REG_DT over the sellamt_cal_day: {self.sellamt_cal_day}')
        print(f'Empty df. Finished Reading [{table}] Data From RDB ')

    else:
        df['COLLECT_DAY_PART'] = df['COLLECT_DAY'].copy() 
        df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
        collect_days = df['COLLECT_DAY'].unique()
        for cday in collect_days:
            df[df['COLLECT_DAY'] == cday].to_parquet(save_path + f'/{table}/{prefix}_{cday}.parquet.gzip', compression='gzip', partition_cols = ['COLLECT_DAY_PART', 'ITEM_ID_PART'])
        print(datetime.now()-s)

if __name__ == '__main__':
    import pymysql
    import glob
    pymysql.install_as_MySQLdb()
    # make_pq_top50('WSPIDER_COUPANG')
    
    # RDB_BI_FASHION1 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider?charset=utf8mb4'
    # RDB_BI_FASHION2 = 'mysql://wspider:wspider00!q@133.186.143.65:3306/wspider_lbrand?charset=utf8mb4' 
    # RDB_BI_REV = 'mysql://wspider:wspider00!q@133.186.210.125:3306/wspider_rev?charset=utf8mb4'

    # save_path = '/data/sellamt/data/WSPIDER_LBRAND/raw_test'
    # saveFirstHistData(RDB_BI_FASHION2, 'MWS_COLT_ITEM_IVT', save_path, 'ivt',6618179341)
    # saveFirstHistData(RDB_BI_FASHION2, 'MWS_COLT_ITEM_PRICE_HIS', save_path, 'phis',879106281)
    # saveFirstHistData(RDB_BI_FASHION2, 'MWS_COLT_ITEM_DISCOUNT', save_path, 'dc',544387557)

    # save_path = '/data/sellamt/data/WSPIDER_REV/raw_rest'
    # print("save under: ", save_path)
    # saveFirstHistData(RDB_BI_REV, 'MWS_COLT_ITEM_IVT', save_path, 'ivt',0)
    # saveFirstHistData(RDB_BI_REV, 'MWS_COLT_ITEM_PRICE_HIS', save_path, 'phis',0)
    # saveFirstHistData(RDB_BI_REV, 'MWS_COLT_ITEM_DISCOUNT', save_path, 'dc',0)

    # save_path = '/data/sellamt/data/WSPIDER/raw_rest'
    # saveFirstHistData(RDB_BI_FASHION1, 'MWS_COLT_ITEM_IVT', save_path, 'ivt',9540337312)
    # saveFirstHistData(RDB_BI_FASHION1, 'MWS_COLT_ITEM_PRICE_HIS', save_path, 'phis',1331477965)
    # saveFirstHistData(RDB_BI_FASHION1, 'MWS_COLT_ITEM_DISCOUNT', save_path, 'dc',772659055)


    # rdb = = 'mysql://wspider:wspider00!q@133.186.146.202:13306/wspider_ap?charset=utf8mb4'
    # save_path = '/data/sellamt/data/WSPIDER_AP/raw'
    # saveFirstHistDataGFK(rdb, 'MWS_COLT_ITEM_IVT', save_path, 'ivt',10000)
    # saveFirstHistDataGFK(rdb, 'MWS_COLT_ITEM_PRICE_HIS', save_path, 'phis', 10000)
    # saveFirstHistDataGFK(rdb, 'MWS_COLT_ITEM_DISCOUNT', save_path, 'dc', 10000)




    RDB_BI_TOP50_11ST = 'mysql://wspider:wspider00!q@133.186.150.168:3306/wspider_top_elevenst?charset=utf8mb4' 
    RDB_BI_TOP50_COUPANG = 'mysql://wspider:wspider00!q@133.186.150.168:3306/wspider_top_coupang?charset=utf8mb4' 
    RDB_BI_TOP50_GMARKET = 'mysql://wspider:wspider00!q@133.186.150.168:3306/wspider_top_gmarket?charset=utf8mb4' 
    RDB_BI_TOP50_WEMAKEPRICE = 'mysql://wspider:wspider00!q@133.186.150.168:3306/wspider_top_wemakeprice?charset=utf8mb4'
    RDB_BI_TOP50_NAVER = 'mysql://wspider:wspider00!q@133.186.150.168:3306/wspider_top_naverstore?charset=utf8mb4'

    top50_rdb_dict = {'wspider_top_elevenst': RDB_BI_TOP50_11ST,
                    'wspider_top_coupang': RDB_BI_TOP50_COUPANG,
                    'wspider_top_gmarket': RDB_BI_TOP50_GMARKET,
                    'wspider_top_wemakeprice': RDB_BI_TOP50_WEMAKEPRICE,
                    'wspider_top_naverstore': RDB_BI_TOP50_NAVER
                    }

    # mongo_database = db
    for db, rdb in top50_rdb_dict.items():
        save_path = f'/data/sellamt/data/{db}/raw'
        saveFirstHistDataTop50(rdb, 'MWS_COLT_ITEM_IVT', save_path, 'ivt',0)
        saveFirstHistDataTop50(rdb, 'MWS_COLT_ITEM_PRICE_HIS', save_path, 'phis', 0)
        saveFirstHistDataTop50(rdb, 'MWS_COLT_ITEM_DISCOUNT', save_path, 'dc', 0)

