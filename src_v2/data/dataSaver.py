from multiprocessing import Pool
# import tqdm
import glob
import sys
import gc
import pprint
import os

# DB related
import pymongo
from sqlalchemy import create_engine
from pyarrow import parquet as pq
# import pymysql
# pymysql.install_as_MySQLdb()

# Data handling related
from datetime import datetime, timedelta
import pandas as pd
import numpy as np

import telepot
# Related folder structure ...
# under '/data/sellamt/'
# data
# |- WSPIDER_11ST
#   |- raw
#       |- MWS_COLT_ITEM
#       |- MWS_COLT_ITEM_IVT
#       |- MWS_COLT_ITEM_PRICE_HIS
#       |- MWS_COLT_ITEM_DISCOUNT

# sell_amount_v2/src
# |- data
#       |- dataSaver.py
#            |- mongoDataSaver
#            |- ReadMongo
#            |- ReadRDB
#       |- saveDataMain_template.py

#
# class RawSaver:
#     def getPartition(self):
#         if self.mongodb:
#             print(f"6. Query MongoDB ITEM table to get {self.partition_1} : {len(id_list)}")
#             client = pymongo.MongoClient('133.186.168.8', 50000)
#             mongodb = client[self.mongodb.upper()]
#             cursor = mongodb['MWS_COLT_ITEM'].find({'ID': {'$in': id_list}},
#                                                    {self.partition_1: True, 'ID': True, '_id': False})
#             item_collect_site = pd.DataFrame(cursor)
#             item_collect_site = item_collect_site.rename(columns={'ID': 'ITEM_ID'})
#             return item_collect_site
#         else:
#             retry_count = 10
#             fail = True
#             while (retry_count > 0):
#                 try:
#                     fail = False
#                     print(f"6. Query MongoDB ITEM table to get {self.partition_1} : {len(id_list)}")
#                     if len(id_list) == 1:
#                         id_list_toquery = f'= {id_list[0]}'
#                     else:
#                         id_list_toquery = f'in {tuple(id_list)}'
#
#                     query = f"select ID AS ITEM_ID, {self.partition_1} from MWS_COLT_ITEM where ID {id_list_toquery};"
#                     engine = create_engine(self.rdb, encoding='utf8', pool_size=50, pool_recycle=500,
#                                            connect_args={'connect_timeout': 1000000})
#                     item_partition = pd.read_sql_query(query, engine)
#                     engine.dispose()
#                     return item_partition
#                 except:
#                     retry_count -= 1
#                 if fail:
#                     print(f'ERROR occured during reading item_id and {self.partition_1}')
#
#     def getMaxID(self):
#         pass
#     def setPartition(self):
#         pass
#     def saveParquet(self):
#         pass
#
# class ReadRDB(RawSaver):
#     def __init__(self):
#         pass
#     def getHist(self):
#         pass
#     def action(self):
#         pass






class ReadRDB:
    # class attributes
    telegbot_token = '873268353:AAFRJZ5QYM4dw1hTtR_7185GSB3eGjbEuBc'
    bot = telepot.Bot(token=telegbot_token)
    chat_id = 592874873

    def __init__(self, save_path, rdb, sellamt_cal_day, partition_column, mongodb=None):
        self.save_path = save_path
        self.rdb = rdb
        self.sellamt_cal_day = sellamt_cal_day
        self.mongodb = mongodb
        self.db_nickname = save_path.split('/')[-2]
        self.partition_1 = partition_column

    def getMaxId(self, saved_files):
        # (1) get saved collect_day list from file list
        saved_collect_day_list = [f.split('.parquet.gzip')[0].split('_')[-1] for f in saved_files]


        # (2) get most recent
        saved_collect_day_list.sort()
        max_collect_day = saved_collect_day_list[-1]

        max_collect_day_file = [f for f in saved_files if f.split('.parquet.gzip')[0].split('_')[-1] == max_collect_day][0]
        saved_ids_df = pd.read_parquet(max_collect_day_file, columns=['ID'])
        saved_ids_list = saved_ids_df['ID'].unique()
        max_id = max(saved_ids_list)
        print('saved max_id : ', max_id, '(from : ', max_collect_day_file, ')')
        return max_id

    def getPartition(self, id_list):
        if self.mongodb:
            # print(f"6. Query MongoDB ITEM table to get {self.partition_1} : {len(id_list)}")
            client = pymongo.MongoClient('133.186.168.8', 50000)
            mongodb = client[self.mongodb.upper()]
            cursor = mongodb['MWS_COLT_ITEM'].find({'ID': {'$in': id_list}},
                                                   {self.partition_1: True, 'ID': True, '_id': False})
            item_partition = pd.DataFrame(cursor)
            item_partition = item_partition.rename(columns={'ID': 'ITEM_ID'})
            return item_partition
        else:
            if len(id_list) == 1:
                id_list_toquery = f'= {id_list[0]}'
            else:
                id_list_toquery = f'in {tuple(id_list)}'

            query = f"select ID AS ITEM_ID, {self.partition_1} from MWS_COLT_ITEM where ID {id_list_toquery};"
            engine = create_engine(self.rdb, encoding='utf8')
            item_partition = pd.read_sql_query(query, engine)
            engine.dispose()
            return item_partition

    def setPartition(self, partition_column_series):
        if self.partition_1 == 'COLLECT_SITE':
            return partition_column_series
        elif self.partition_1 == 'GOODS_CATE':
            return partition_column_series.apply(lambda x: '전자' if x.find('골프') == -1 else '골프')
        else:
            return partition_column_series

    def getNewItems(self):
        # Get newly added ITEMS.
        # ITEM table's REG_DT format : %Y-%m-%d %h:%m:%s
        # item_path = self.base_path + '/raw/MWS_COLT_ITEM/'
        # Especially for HSY seller mastering.
        reg_dt = self.sellamt_cal_day
        # reg_dt_max = datetime.strftime(datetime.strptime(reg_dt, '%Y-%m-%d') + timedelta(days=1), '%Y-%m-%d')

        files = glob.glob(self.save_path + f'/MWS_COLT_ITEM/new_item_*')
        regdt_list = [f.split('.parquet.gzip')[0].split('_')[-2] for f in files]
        max_id_list = [int(f.split('.parquet.gzip')[0].split('_')[-1]) for f in files]
        max_id = max(max_id_list)
        if reg_dt in regdt_list:
            print(
                f"No need to read from RDB. Already saved as [[{self.save_path}/MWS_COLT_ITEM/new_item_{reg_dt}_{max_id}.parquet.gzip]]")
            return pd.DataFrame()
        else:
            new_min_id = max_id + 1
            print("READ new item from RDB MWS_COLT_ITEM table *DIRECTLY* (by ID)")
            query = f'select ID, ITEM_NUM, GOODS_NAME, GOODS_CATE, BRAND_NAME, COLLECT_DAY, COLLECT_SITE, ' \
                    f'ADD_INFO, COLLECT_URL, TRUST_SELLER, REG_DT, UPT_DT from MWS_COLT_ITEM where ID >= {new_min_id};'
            engine = create_engine(self.rdb, encoding='utf8')
            db_rows = pd.read_sql_query(query, engine)
            db_rows = db_rows[db_rows['REG_DT'] <= reg_dt + ' 23:59:59']
            print(">> target regdt : ", reg_dt)
            engine.dispose()
            return db_rows

    # def readItemByNum(self, collect_site, item_num_list):
    #     # Read update-type data
    #     # ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s
    #     # item_path = self.base_path + '/raw/MWS_COLT_ITEM/'
    #     query = f"""select ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, GOODS_CATE, 
    #     COLLECT_URL, TRUST_SELLER, BRAND_NAME,ORG_GOODS_NUM, PRICE, SITE_PRICE, REG_DT, UPT_DT
    #     from MWS_COLT_ITEM where COLLECT_SITE = '{collect_site}' and ITEM_NUM in {tuple(item_num_list)};"""
    #     engine = create_engine(self.rdb, encoding='utf8', pool_size=50, pool_recycle=3600,
    #                            connect_args={'connect_timeout': 1000000})
    #     db_rows = pd.read_sql_query(query, engine)
    #     engine.dispose()
    #     # print("READ MWS_COLT_ITEM table from RDB *DIRECTLY*")
    #     # print(">> collect_site : ", collect_site)
    #     # print(">> item_num list length: ", len(item_num_list))
    #     return db_rows

    def readItemByID(self, item_id_list):
        '''
        Read update-type data
        ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s
        item_path = self.base_path + '/raw/MWS_COLT_ITEM/'
        '''
        query = f"""select ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, GOODS_CATE, 
            COLLECT_URL, TRUST_SELLER, BRAND_NAME, ORG_GOODS_NUM, PRICE, SITE_PRICE, REG_DT, UPT_DT 
            from MWS_COLT_ITEM where ID in {tuple(item_id_list)};"""
        engine = create_engine(self.rdb, encoding='utf8')
        db_rows = pd.read_sql_query(query, engine)
        engine.dispose()
        return db_rows

        # print("READ MWS_COLT_ITEM table from RDB *DIRECTLY*")
        # ## SPLIT PARAMS!!!
        # chunk_count = len(item_id_list) // 100
        # split_id_list = np.array_split(item_id_list, chunk_count)
        # item_df = pd.DataFrame()
        # for idx, sil in enumerate(split_id_list):
        #     query = f"""select ID AS ITEM_ID, COLLECT_SITE, ITEM_NUM, GOODS_NAME, GOODS_CATE, 
        #     COLLECT_URL, TRUST_SELLER, BRAND_NAME, ORG_GOODS_NUM, PRICE, SITE_PRICE, REG_DT, UPT_DT 
        #     from MWS_COLT_ITEM where ID in {tuple(sil)};"""
        #     engine = create_engine(self.rdb, encoding='utf8', pool_size=50, pool_recycle=3600,
        #                            connect_args={'connect_timeout': 1000000})
        #     db_rows = pd.read_sql_query(query, engine)
        #     item_df = item_df.append(db_rows)
        #     engine.dispose()
        #     print('>> item_df reading from RDB proces.... ', idx + 1, '/', len(split_id_list))
        # print(">> item_id list length: ", len(item_id_list), '-->', len(item_df))
        # return item_df

    def getHist(self, params_list):
        # print(params_list)
        min_id, max_id, table = params_list
        engine = create_engine(self.rdb, encoding='utf8')
        df = pd.read_sql(f"SELECT * FROM {table} WHERE ID BETWEEN {min_id} and {max_id}", engine)
        engine.dispose()
        return df

    def action(self, table, prefix):
        msg = f"""
        [{self.db_nickname}] Make Pq files...
        Start saveDataMain Module for [{table}]
        Save new_item or history type data as parquets files.
        >> maximum collect_day : [{self.sellamt_cal_day}]"""
        self.bot.sendMessage(chat_id=self.chat_id, text=msg)
        print(msg)
        print('READ DATA FROM : ', self.db_nickname)

        if (table == 'MWS_COLT_ITEM') & (prefix == 'new_item'):
            print(f'>> save {table} as [[{prefix}_reg_dt_day.parquet]]')
            new_item = self.getNewItems()
            if not new_item.empty:
                new_item['COLLECT_SITE_PART'] = new_item['COLLECT_SITE'].copy()
                max_id = max(new_item['ID'])
                new_item.to_parquet(self.save_path + f'/{table}/{prefix}_{self.sellamt_cal_day}_{max_id}.parquet.gzip',
                                    engine = 'pyarrow', compression='gzip', partition_cols=['COLLECT_SITE_PART'])

        elif (table == 'MWS_COLT_ITEM') & (prefix == 'item'):
            pass

        else:
            print(f'>> save {table} as [[{prefix}_collectday.parquet.gzip/COLLECT_SITE_PART/ITEM_ID_PART]]')
            engine = create_engine(self.rdb, encoding='utf8')
            # (1) Check saved files and get max ID value.
            files = glob.glob(self.save_path + f'/{table}/*')
            if files:
                max_id = self.getMaxId(files)
            else:
                # Finish the process
                print(f"There is no file under {self.save_path}/{table}/")
                print("Get Min item_id of the first day of the collect_day and run [saveData_init] module first..")
                return

            # (2) Get the most recent ID value
            new_min_id = max_id + 1
            new_max_id = pd.read_sql(f"SELECT ID FROM {table} order by id desc limit 1", engine)['ID'].values[0]
            engine.dispose()
            print('new_min_id, new_max_id', new_min_id, new_max_id)
            new_id_list = list(range(new_min_id, new_max_id))
            if len(new_id_list) == 0: 
                return
            print(f'query id between {min(new_id_list)} and {max(new_id_list)}')

            # (3) Make splited ID list
            chunk_size = 20000  # should query at most 100000 records.
            chunk_count = round(len(new_id_list) / chunk_size)
            chunk_count = chunk_count + 1 if chunk_count == 0 else chunk_count
            new_id_list.sort()
            splited_idlist = list(np.array_split(new_id_list, chunk_count))
            minmax_splited_idlist = [[i.min(), i.max(), table] for i in splited_idlist]
            minmax_splited_idlist_chunk = np.array_split(minmax_splited_idlist, 20)

            # (4) Get data from new_min_id ~ new_max_id
            s = datetime.now()

            for idx, idlist_chunk in enumerate(minmax_splited_idlist_chunk):
                # print(f'GET DATA : ID BETWEEN {idlist_chunk[0][0]} ~ {idlist_chunk[-1][1]}')

                # 1. read data between min ID and max ID
                print("# 1. read data between min ID and max ID")
                with Pool(20) as p:
                    df_list = p.map(self.getHist, idlist_chunk)

                if len(df_list) == 0:
                    print(f'Empty df_list. Maybe no data between the ids.')
                    continue
                df = pd.concat(df_list)

                # df = pd.read_sql(f"SELECT * FROM {table} WHERE ID BETWEEN {i[0]} and {i[1]}", engine)
                if df.empty:
                    print(f'Empty df. Maybe no data between the ids.')
                    continue

                # 2. Filter out data over sellamt_cal_day
                print("# 2. Filter out data over sellamt_cal_day")
                df = df[df['REG_DT'] <= self.sellamt_cal_day + ' 23:59:59']
                if df.empty:
                    print(f'Empty df. Maybe REG_DT over the sellamt_cal_day: {self.sellamt_cal_day}')
                    print(f'Finish Reading [{table}] Data From RDB ')
                    break

                # 3. Make item id list(iid_list)
                print("# 3. Make item id list(iid_list)")
                df['ITEM_ID_PART'] = df['ITEM_ID'] % 19
                iid_list = df['ITEM_ID'].unique()
                iid_list = [int(i) for i in iid_list]

                # # 4. query mongoDB or RDB to get partition column info.
                # print('# 4. query mongoDB or RDB to get partition column info')
                # chunk_size = 1000  # should query at most 100000 records.
                # chunk_count = round(len(iid_list) / chunk_size)
                # chunk_count = chunk_count + 1 if chunk_count == 0 else chunk_count
                # splited_idlist = list(np.array_split(iid_list, chunk_count))

                # with Pool(20) as p2:
                #     item_partition_df_list = p2.map(self.getPartition, splited_idlist)
                # item_partition_df = pd.concat(item_partition_df_list)
                # if item_partition_df.empty:
                #     print(">> Cannot get item data from mongoDB or RDB")
                #     continue
                # print(">> item_id list length: ", len(iid_list), '-->', len(item_partition_df))
                # # 5. merge partitioning column info with history-type data
                # print('# 5. merge partitioning column info with history-type data')
                # df = pd.merge(df, item_partition_df, how='left', on='ITEM_ID')
                # df = df.fillna(np.nan)
                # df[self.partition_1] = df[self.partition_1].fillna('NO_ITEM_IN_DB')
                df[self.partition_1 + '_PART'] = self.setPartition(df[self.partition_1])
                # print(df[self.partition_1].value_counts())
                # print('>> [NO_ITEM_IN_DB] count should be zero')
                # df = df.rename(columns={self.partition_1: })

                if table == 'MWS_COLT_ITEM_PRICE_HIS':
                    df['COLLECT_DAY'] = df['COLLECT_DAY'].apply(lambda x: x.strftime('%Y-%m-%d') )
                elif table == 'MWS_COLT_ITEM_IVT':
                    df['GIFT_OPTION'] = df['GIFT_OPTION'].astype('str')
                    df['STYLE_OPTION'] = df['STYLE_OPTION'].astype('str')
                    df['SIZE_OPTION'] = df['SIZE_OPTION'].astype('str')
                    df['COLOR_OPTION'] = df['COLOR_OPTION'].astype('str')
                unique_collect_days = df['COLLECT_DAY'].unique()

                for cday in unique_collect_days:
                    file_name = self.save_path + f'/{table}/{prefix}_{str(cday)}.parquet.gzip'
                    df[df['COLLECT_DAY'] == cday].to_parquet(file_name, compression='gzip',
                                                             partition_cols=[self.partition_1 + '_PART', 'ITEM_ID_PART'])
                del df
                # del item_partition_df
                gc.collect()

                print('----------------------------------------------------------------------------------------')

            print(datetime.now() - s)
            msg = f"""[{self.db_nickname}] Finish Reading [{table}] Data From RDB . ({datetime.now() - s})"""
            self.bot.sendMessage(chat_id=self.chat_id, text=msg)
            print(msg)


class mongoDataSaver:
    def __init__(self, path, collection, database, host = '133.186.168.8', port = 50000):
        self.host = host
        self.database = database
        self.port = port
        self.collection = collection
        file_name_prefix = {'MWS_COLT_ITEM': 'item',
                           'MWS_COLT_ITEM_IVT': 'ivt',
                           'MWS_COLT_ITEM_PRICE_HIS': 'phis',
                           'MWS_COLT_ITEM_DISCOUNT': 'dc'}
        self.prefix = file_name_prefix[self.collection]
        print(f"------------------------- GET data from {self.collection}")
        self.start_point = datetime.now()
        self.connectMongo()
        self.setOutputPath(path)

    def connectMongo(self):
        try :
            self.client = pymongo.MongoClient(self.host, self.port)
            self.mongodb = self.client[self.database.upper()]
            print(f"1. Successfully Connected to MongoDB {self.database}")
        except pymongo.errors.ServerSelectionTimeoutError as err:
            print(f"1. Connection Failed : {self.database}")
            print(err)

    def setOutputPath(self, path):
        self.outputpath = path + '/' + self.collection + '/'
        print("2. Set Output Path")
        print(">> Parquet file will be saved under [[", self.outputpath, "]]")


    def makeCollectDaysList(self, sellamt_cal_day, collect_day_window):
        # make a list of days need for the threshold setting process.
        collect_day_start = datetime.strptime(sellamt_cal_day, '%Y-%m-%d') - timedelta(days=collect_day_window)
        collect_day_list = pd.date_range(collect_day_start, periods=collect_day_window + 1).tolist()
        collect_day_list = [datetime.strftime(i, '%Y-%m-%d') for i in collect_day_list]
        collect_day_list.sort()
        print("3. Make COLLECT_DAY List")
        print(f">> Need {len(collect_day_list)} days data: {str(min(collect_day_list))} ~ {str(max(collect_day_list))}")
        self.collect_day_list = collect_day_list
        return self.collect_day_list

    def checkFiles(self, sellamt_cal_day, collect_day_window):
        # check if already saved the data as a parquet file.
        # 1) make a list of collect day you need.
        self.collect_day_list = self.makeCollectDaysList(sellamt_cal_day, collect_day_window)
        self.today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        # 2) check files under the output path
        print("4. Check Already Saved Files under ", self.outputpath)
        saved_files = glob.glob(self.outputpath + '*')
        # print(saved_files, len(saved_files))
        if len(saved_files) is 0:
            saved_files = [f'dummy_file_name_{self.prefix}_to detour index out of range error ']
            print("No saved files : ", saved_files)


        # 3) get output(uptdt_range to query & collect_day to query)
        if self.collection == 'MWS_COLT_ITEM':
            # ITEM table's UPT_DT format : %Y-%m-%d %h:%m:%s
            saved_uptdt_range = [i.split(f'{self.prefix}_')[1].split(".")[0] for i in saved_files]
            self.uptdt_from = min(self.collect_day_list)
            self.uptdt_range_toquery  = self.uptdt_from + '_' + self.today
            print('saved_uptdt_range', saved_uptdt_range)
            self.uptdt_range_toquery = list(set([self.uptdt_range_toquery]) - set(saved_uptdt_range))
            print('uptdt_range_toquery', self.uptdt_range_toquery)
            return self.uptdt_range_toquery

        elif (self.collection == 'MWS_COLT_ITEM_IVT') | (self.collection == 'MWS_COLT_ITEM_DISCOUNT'):
            # IVT table's COLLECT_DAY format : %Y%m%d
            # DISCOUNT table's COLLECT_DAY format : %Y%m%d
            self.collect_day_list = [''.join(i.split('-')) for i in self.collect_day_list]
            saved_days = [i.split(f'{self.prefix}_')[1].split('.')[0] for i in saved_files]
            collect_days_toquery = list(set(self.collect_day_list) - set(saved_days))
            return collect_days_toquery

        elif self.collection == 'MWS_COLT_ITEM_PRICE_HIS':
            # PRICE_HIS table's COLLECT_DAY fromat : %Y-%m-%d
            saved_days = [i.split(f'{self.prefix}_')[1].split('.')[0] for i in saved_files]
            print(saved_days)
            collect_days_toquery = list(set(self.collect_day_list) - set(saved_days))
            return collect_days_toquery

        else:
            return self.collect_day_list

    def setColumnFilter(self, columns):
        self.columns = columns
        columns_filter = {c: True for c in self.columns}
        columns_filter.update({'_id': False})

        return columns_filter


    def queryMongo(self, query = {}, projections = None, nlimit = None):
        print("5. Query Mongo DB")
        if projections:
            cursor = self.mongodb[self.collection].find(query, projections)
            print(">> rows filter query :",  query)
            print(">> columns filter query :", projections)
        else:
            cursor = self.mongodb[self.collection].find(query, {'_id': False})
            print(">> rows filter query :", query)

        if nlimit:
            cursor = cursor.limit(nlimit)
            print(">> rows limit :", nlimit)
        else :
            cursor = cursor

        df = pd.DataFrame(list(cursor))
        print(">> Data Shape :", df.shape)
        return df


    def saveParquet(self, df, pcols):
        print("df is empty:", df.empty)
        if df.empty:
            return print('No data to save. Maybe collection error or transfer error occured. ')
        else:
            # 1) set file names
            if self.collection == 'MWS_COLT_ITEM':
                file_name = self.outputpath + f'{self.prefix}_{min(self.collect_day_list)}_{self.today}.parquet.gzip'
            elif self.collection in ['MWS_COLT_ITEM_IVT', 'MWS_COLT_ITEM_PRICE_HIS', 'MWS_COLT_ITEM_DISCOUNT']:
                cday = df['COLLECT_DAY'].unique()[0]
                file_name = self.outputpath + f'{self.prefix}_{cday}.parquet.gzip'
            else:
                print(f"collection is not in the list!!")
                return

            # 2) transform column order
            print('df.columns: ', df.columns)
            print('self.columns :', self.columns)
            print(df.dtypes)
            df = df[self.columns + pcols]

            print("7. Save Data: ", file_name)
            index_col = 'ID'
            print(self.columns)
            df.to_parquet(fname=file_name, index=index_col, partition_cols=pcols, engine='pyarrow',
                          compression='gzip')
            print("TIME SPENT: ", str(datetime.now() - self.start_point))
    def __del__(self):
        self.client.close()
        print("---------------------------------------------------------------------------------------- ")



class ReadMongo:
    def __init__(self, save_path, db, sellamt_cal_day, collect_day_window, partition_1, rdb):
        self.base_path = save_path
        self.database = db
        self.sellamt_cal_day = sellamt_cal_day
        self.collect_day_window = collect_day_window
        self.partition_1 = partition_1
        self.rdb = rdb

    def getCsite(self, id_list):
        print("6. Query MongoDB ITEM table go get COLLECT_SITE: ", len(id_list))
        client = pymongo.MongoClient('133.186.168.8',50000)
        mongodb = client[self.database.upper()]
        cursor = mongodb['MWS_COLT_ITEM'].find({'ID': {'$in':id_list}}, {'COLLECT_SITE': True, 'ID': True, '_id': False})
        item_collect_site = pd.DataFrame(cursor)
        item_collect_site = item_collect_site.rename(columns = {'ID': 'ITEM_ID'})
        # item_collect_site['COLLECT_SITE_PART'] = item_collect_site['COLLECT_SITE'].copy()
        print("6. Query MongoDB ITEM table go get COLLECT_SITE -- Completed")
        return item_collect_site

    def getPartition(self, id_list):
        # 1. Query MongoDB First. 
        client = pymongo.MongoClient('133.186.168.8', 50000)
        mongodb = client[self.database.upper()]
        cursor = mongodb['MWS_COLT_ITEM'].find({'ID': {'$in': id_list}},
                                               {self.partition_1: True, 'ID': True, '_id': False})
        item_partition = pd.DataFrame(cursor)
        item_partition = item_partition.rename(columns={'ID': 'ITEM_ID',  self.partition_1 :  self.partition_1+'_PART' })
        item_partition = item_partition.drop_duplicates('ITEM_ID', keep = 'last')
        saved_ids = item_partition['ITEM_ID'].unique()
        need_to_query_ids = list(set(id_list) - set(saved_ids))

        # 2. If not all items are in Mongo, query RDB.
        if not need_to_query_ids:
            print('ALL ITEMS are in Mongodb. Do not need to access RDB.')
        else:
            print('NOT ALL ITEMS are in MONGO DB.', len(need_to_query_ids))            
            con = create_engine(self.rdb)
            chunk_size = 1000
            chunk_count = round(len(need_to_query_ids) / chunk_size)
            chunk_count = chunk_count + 1 if chunk_count == 0 else chunk_count
            new_item_partition_splited = np.array_split(need_to_query_ids, chunk_count)
            print(new_item_partition_splited)
            for need_to_query_ids_part in new_item_partition_splited:
                if len(need_to_query_ids_part) == 1:
                    id_list_toquery = f'= {need_to_query_ids_part[0]}'
                else:
                    id_list_toquery = f'in {tuple(need_to_query_ids_part)}'
                
                query = f'SELECT ID, {self.partition_1} FROM MWS_COLT_ITEM where ID {id_list_toquery};'
                print(query)
                new_item_partition = pd.read_sql(query, con)
                new_item_partition = new_item_partition.rename(columns={'ID': 'ITEM_ID',  self.partition_1 :  self.partition_1+'_PART' })
                item_partition = item_partition.append(new_item_partition)
            print('--> read rest ITEMS FROM RDB', len(item_partition))
        
        return item_partition[['ITEM_ID',  self.partition_1+'_PART']]
        
    def setPartition(self, partition_column_series):
        if self.partition_1 == 'COLLECT_SITE':
            return partition_column_series
        elif self.partition_1 == 'GOODS_CATE':
            return partition_column_series.apply(lambda x: '전자' if x.find('골프') == -1 else '골프')
    def action(self, collection):
        if collection == 'MWS_COLT_ITEM_IVT':
            columns = ['ID', 'ITEM_ID', 'STOCK_ID', 'COLOR_OPTION', 'SIZE_OPTION', 'STYLE_OPTION',
                       'GIFT_OPTION', 'OPTION', 'STOCK_AMOUNT', 'ADD_PRICE', 'COLLECT_DAY', 'REG_ID', 'REG_DT']
        elif collection == 'MWS_COLT_ITEM_PRICE_HIS':
            columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'PRICE', 'SITE_PRICE', 'REG_DT']
        elif collection == 'MWS_COLT_ITEM_DISCOUNT':
            columns = ['ID', 'ITEM_ID', 'COLLECT_DAY', 'OPTION', 'DISCOUNT_PRICE','DISCOUNT_RATE', 'REG_DT']
        else :
            print("The collection name is not in the list. Please check typo.")
            return

        # ---- make Saver instance
        histSaver = mongoDataSaver(database = self.database, collection= collection, path = self.base_path)
        # ---- query DB  & save files
        collect_days_toquery = histSaver.checkFiles(self.sellamt_cal_day, self.collect_day_window)
        collect_days_toquery.sort()
        if collect_days_toquery:
            print(f">> {len(collect_days_toquery)} days data should be called from DB")
            print(">> COLLECT DAYS TO QUERY", collect_days_toquery)
            for cday in collect_days_toquery:
                # 0. Get history style data of the cday from MongoDB
                rows_filter = {'COLLECT_DAY': {'$in': [cday]}}
                columns_filter = histSaver.setColumnFilter(columns)
                daily_df = histSaver.queryMongo(query = rows_filter, projections=columns_filter)
                if daily_df.empty:
                    print('No Data in ', cday)
                    continue
                daily_df['ITEM_ID_PART'] = daily_df['ITEM_ID'] % 19

                # 1. get item_ids of the part
                iip_id_lists = [daily_df.loc[daily_df['ITEM_ID_PART'] == iip, 'ITEM_ID'].unique() for iip in range(0,19)]
                iip_id_lists = [[int(iid) for iid in idlist] for idlist in iip_id_lists]

                # 2. query mongo to get collect sites
                with Pool(10) as p:
                    item_csite_list = p.map(self.getPartition, iip_id_lists)
                item_csite_df = pd.concat(item_csite_list)
                item_csite_df[self.partition_1 + '_PART'] = self.setPartition(item_csite_df[self.partition_1+'_PART'])

                # 2. merge item collect site & item num with the ivt_df
                daily_df = pd.merge(daily_df, item_csite_df, how = 'left', on = 'ITEM_ID')
                daily_df[self.partition_1 + '_PART'] = daily_df[self.partition_1 + '_PART'].fillna('기타')
                print(daily_df[self.partition_1 + '_PART'].value_counts())
                histSaver.saveParquet(daily_df, [self.partition_1 + '_PART', 'ITEM_ID_PART'])
                del daily_df
                del item_csite_df
                gc.collect()
        else:
            print(f">> No new {collection} data to query.")
        del histSaver
